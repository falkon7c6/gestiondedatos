package gestion.gestionFinal.persistencia;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

import com.mysql.jdbc.PreparedStatement;

public class UsuarioDAO {
	databaseConnection conexion;

	private CallableStatement llamada;
	private PreparedStatement stmt;

	public UsuarioDAO() {
		conexion = new databaseConnection();

	}

	public void insertar(String usuario, String pass) throws SQLException {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Usuario(?,?)}");
			llamada.setString(1, usuario);
			llamada.setString(2, pass);
			llamada.execute();
			System.out.println("usuario creado");
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void borrar(String usuario) throws SQLException {
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Baja_Usuario(?)}");
			llamada.setString(1, usuario);
			llamada.execute();
			System.out.println("Usuario borrado");
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}



	public void otorgarPrivilegios(String usuario, String privilegio)
			throws SQLException {
		this.revocarPrivilegios(usuario);
		conexion.conectar();
		try {
			switch (privilegio) {
			case "administrador":
				llamada = conexion.connection
						.prepareCall("{call Conceder_privilegios_administracion(?)}");
				llamada.setString(1, usuario);
				llamada.execute();
				break;
			case "compraventa":
				llamada = conexion.connection
						.prepareCall("{call Conceder_privilegios_compraventa(?)}");
				llamada.setString(1, usuario);
				llamada.execute();
				break;
			case "liquidacion":
				llamada = conexion.connection
						.prepareCall("{call Conceder_privilegios_liquidacion(?)}");
				llamada.setString(1, usuario);
				llamada.execute();
				break;
			default:
				llamada = conexion.connection
						.prepareCall("{call Quitar_Todos_Privilegios(?)}");
				llamada.setString(1, usuario);
				llamada.execute();
				break;
			}
			System.out.println("privilegios actualizados");
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String obtenerRol(String usuario) {
		String rol = "";
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call obtenerRol(?,?)}");
			llamada.setString(1, usuario);
			llamada.registerOutParameter(2, Types.VARCHAR);
			llamada.execute();
			rol = llamada.getString(2);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return rol;
	}

	public void bajaLogicaUsuario(String legajo) throws SQLException {
		revocarPrivilegios(legajo);
	}

	public void revocarPrivilegios(String legajo) throws SQLException {
		conexion.conectar();
		llamada = conexion.connection
				.prepareCall("{call Quitar_Todos_Privilegios(?)}");
		llamada.setString(1, legajo);
		realizarLlamada();
	}

	public void concederPrivilegiosAdministracion(String legajo)
			throws SQLException {
		conexion.conectar();
		llamada = conexion.connection
				.prepareCall("{call Conceder_Privilegios_Administracion(?)}");
		llamada.setString(1, legajo);
		realizarLlamada();
	}

	public void concederPrivilegiosCompraVenta(String legajo)
			throws SQLException {
		conexion.conectar();
		llamada = conexion.connection
				.prepareCall("{call Conceder_Privilegios_CompraVenta(?)}");
		llamada.setString(1, legajo);
		realizarLlamada();
	}

	public void concederPrivilegiosLiquidacion(String legajo)
			throws SQLException {
		conexion.conectar();
		llamada = conexion.connection
				.prepareCall("{call Conceder_Privilegios_Liquidacion(?)}");
		llamada.setString(1, legajo);
		realizarLlamada();
	}

	private void realizarLlamada() throws SQLException {
		try {
			llamada.execute();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
					System.out.println("Connection Closed");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void cambiarClave(String legajo, String clave) throws SQLException {
		conexion.conectar();
		llamada = conexion.connection.prepareCall("{call cambiar_clave(?,?)}");
		llamada.setString(1, legajo);
		llamada.setString(2, clave);
		realizarLlamada();
	}

}
