package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Categoriaproducto;
import gestion.gestionFinal.modelo.Marcaproducto;
import gestion.gestionFinal.modelo.Producto;
import gestion.gestionFinal.modelo.Proveedor;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ProductoDAO{
	databaseConnection conexion;

	private CallableStatement llamada;

	public ProductoDAO() {
		conexion = new databaseConnection();
	}
	
	public int insertar(Producto prod) {
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Producto(?,?,?,?,?,?,?,?)}");
			llamada.registerOutParameter(8, Types.INTEGER);
			llamada.setInt(1, prod.getCategoriaproducto().getIdCategoriaProducto());
			llamada.setInt(2, prod.getMarcaproducto().getIdMarcaProducto());
			llamada.setString(3, prod.getNombre());
			llamada.setString(4, prod.getDescripcion());
			llamada.setFloat(5, prod.getPrecioVenta());
			llamada.setString(6, prod.getDeshabilitado());
			llamada.setInt(7, prod.getStock());
			llamada.execute();
			id = llamada.getInt(8);
			System.out.println("objeto insertado con id = " + id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return id;
	}

	public void borrar(int id) throws SQLException{
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Producto(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");		
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	
	public void editar(Producto prod) {
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Producto(?,?,?,?,?,?,?,?)}");
			llamada.setInt(1, prod.getCategoriaproducto().getIdCategoriaProducto());
			llamada.setInt(2, prod.getMarcaproducto().getIdMarcaProducto());
			llamada.setString(3, prod.getNombre());
			llamada.setString(4, prod.getDescripcion());
			llamada.setFloat(5, prod.getPrecioVenta());
			llamada.setString(6, prod.getDeshabilitado());
			llamada.setInt(7, prod.getStock());
			llamada.setInt(8, prod.getIdProducto());
			llamada.execute();
			System.out.println("Objeto editado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
	public ObservableList<Producto> listar() {
		ObservableList<Producto> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_Producto()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				
				Producto prod = new Producto(
						RS.getInt("idProducto"),
						RS.getString("descripcion"),
						RS.getString("deshabilitado"),
						RS.getString("nom_producto"),
						RS.getInt("Stock"),
						RS.getFloat("precioVenta"),
						new Categoriaproducto(),
						new Marcaproducto());
						prod.getCategoriaproducto().setIdCategoriaProducto(RS.getInt("CategoriaProducto_idCategoriaProducto"));
						prod.getCategoriaproducto().setNombre(RS.getString("categoria_nombre"));
						prod.getMarcaproducto().setIdMarcaProducto(RS.getInt("Marca_idMarca"));
						prod.getMarcaproducto().setNombre(RS.getString("marca_nombre"));
				lista.add(prod);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
	
	public ObservableList<Producto> listarHabilitados() {
		ObservableList<Producto> lista = FXCollections.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_Producto_Habilitado()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {

				Producto prod = new Producto(RS.getInt("idProducto"),
						RS.getString("descripcion"),
						RS.getString("deshabilitado"),
						RS.getString("nom_producto"), RS.getInt("Stock"),
						RS.getFloat("precioVenta"), new Categoriaproducto(),
						new Marcaproducto());
				prod.getCategoriaproducto().setIdCategoriaProducto(
						RS.getInt("CategoriaProducto_idCategoriaProducto"));
				prod.getCategoriaproducto().setNombre(
						RS.getString("categoria_nombre"));
				prod.getMarcaproducto().setIdMarcaProducto(
						RS.getInt("Marca_idMarca"));
				prod.getMarcaproducto().setNombre(RS.getString("marca_nombre"));
				lista.add(prod);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}

	public ObservableList<Producto> listar(Proveedor prov) {
		ObservableList<Producto> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_ProductoCompras(?)}");
			llamada.setInt(1, prov.getIdProveedor());
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				
				Producto prod = new Producto(
						RS.getInt("idProducto"),
						RS.getString("descripcion"),
						RS.getString("deshabilitado"),
						RS.getString("nombre"),
						RS.getInt("Stock"),
						RS.getFloat("precio"),
						new Categoriaproducto(),
						new Marcaproducto());
						prod.getCategoriaproducto().setIdCategoriaProducto(RS.getInt("CategoriaProducto_idCategoriaProducto"));
						prod.getCategoriaproducto().setNombre(RS.getString("categoria_nombre"));
						prod.getMarcaproducto().setIdMarcaProducto(RS.getInt("Marca_idMarca"));
						prod.getMarcaproducto().setNombre(RS.getString("marca_nombre"));
				lista.add(prod);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}

	public void actualizarStock(int id, int stock) {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Editar_Stock(?,?)}");
			llamada.setInt(1, id);
			llamada.setInt(2, stock);
			llamada.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public int obtenerStock(Producto prod) {
		int id = 0;
		conexion.conectar();
		try {
			System.out.println("id = " + prod.getIdProducto());
			llamada = conexion.connection
					.prepareCall("{call obtener_Stock(?)}");
			llamada.setInt(1, prod.getIdProducto());
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				id = RS.getInt("stock");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("stock: " + id);
		return id;
	}

	public void deshabilitar(int id) throws SQLException {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Deshabilitar_Producto(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Producto deshabilitado");
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}
}
