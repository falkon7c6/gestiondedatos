package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Categoriaproducto;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CategoriaProductoDAO {
	
	databaseConnection conexion;

	private CallableStatement llamada;
	
	public CategoriaProductoDAO(){
		conexion = new databaseConnection();

	}
	
	public int insertar(Categoriaproducto cat) throws SQLException{
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Categoria_Producto(?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);						
			llamada.setString(2, cat.getNombre());			
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return id;
	}
	
	public void borrar(int id) throws SQLException {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Categoria_Producto(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public void editar(Categoriaproducto cat){
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Marca_Producto(?,?)}");
			llamada.setInt(1, cat.getIdCategoriaProducto());
			llamada.setString(2, cat.getNombre());
			llamada.execute();
			System.out.println("Objeto editado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public ObservableList<Categoriaproducto> listar() {
		ObservableList<Categoriaproducto> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_Categoria_Producto()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Categoriaproducto cat = new Categoriaproducto(RS.getInt("idCategoriaProducto"), RS.getString("nombre"));
				lista.add(cat);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
	
}
