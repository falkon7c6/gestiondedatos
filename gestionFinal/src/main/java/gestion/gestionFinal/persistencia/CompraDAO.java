package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Compra;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CompraDAO {
	databaseConnection conexion;

	private CallableStatement llamada;

	public CompraDAO() {
		conexion = new databaseConnection();

	}

	public int insertar(Compra com) {
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Compra(?,?,?,?,?,?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setInt(2, com.getProveedor().getIdProveedor());
			llamada.setDate(3, java.sql.Date.valueOf(com.getFecha()));
			llamada.setFloat(4, com.getTotal());
			llamada.setInt(5, com.getEmpleado().getLegajo());
			llamada.setInt(6, com.getPeriodo().getIdPeriodo());
			llamada.setString(7, com.getTipoFactura());
			llamada.setString(8, com.getNumeroComprobante());
			llamada.setString(9, com.getObservaciones());
			llamada.setString(10, com.getCondicionIVA());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return id;
	}

	public void borrar(int id) {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Compra(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void editar(Compra com) {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Modificacion_Compra(?,?)}");
			//llamada.setInt(1, cat.getIdCategoriaProducto());
			//llamada.setString(2, cat.getNombre());
			llamada.execute();
			System.out.println("Objeto editado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ObservableList<Compra> listar() {
		ObservableList<Compra> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_Categoria_Producto()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Compra cat = new Compra();
				lista.add(cat);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
}
