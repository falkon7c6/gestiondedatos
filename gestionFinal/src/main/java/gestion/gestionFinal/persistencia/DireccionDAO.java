package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Direccion;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

public class DireccionDAO {
	
	databaseConnection conexion;

	private CallableStatement llamada;
	
	public DireccionDAO(){
		conexion = new databaseConnection();
	}
	
	public int insertar(Direccion dir) throws SQLException{
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Direccion(?,?,?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);						
			llamada.setString(2, dir.getCalle());
			llamada.setString(3, dir.getNumero());
			llamada.setString(4, dir.getPiso());
			llamada.setString(5, dir.getDepartamento());
			llamada.setString(6, dir.getLocalidad());
			llamada.setString(7, dir.getProvincia());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return id;
	}
	
	public void editar(Direccion dir){
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Modificacion_Direccion(?,?,?,?,?,?,?)}");
			llamada.setInt(1, dir.getIdDireccion());						
			llamada.setString(2, dir.getCalle());
			llamada.setString(3, dir.getNumero());
			llamada.setString(4, dir.getPiso());
			llamada.setString(5, dir.getDepartamento());
			llamada.setString(6, dir.getLocalidad());
			llamada.setString(7, dir.getProvincia());
			llamada.execute();
			System.out.println("Objeto editado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
