package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Direccion;
import gestion.gestionFinal.modelo.Persona;
import gestion.gestionFinal.modelo.Proveedor;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ProveedorDAO {
	databaseConnection conexion;

	private CallableStatement llamada;
	
	public ProveedorDAO(){
		conexion = new databaseConnection();

	}
	
	public int insertar(Proveedor prov){
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Proveedor(?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);						
			llamada.setString(2, prov.getPersona().getCuilcuit());			
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return id;
	}
	
	public void borrar(int id)
 throws SQLException {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Proveedor(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public void editar(Proveedor prov){
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Proveedor(?,?)}");
			llamada.setInt(1, prov.getIdProveedor());
			llamada.setString(2, prov.getPersona().getCuilcuit());
			llamada.execute();
			System.out.println("Objeto editado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public ObservableList<Proveedor> listar() {
		ObservableList<Proveedor> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_Proveedor}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Direccion dir = new Direccion();
				dir.setCalle(RS.getString("calle"));
				dir.setDepartamento(RS.getString("departamento"));
				dir.setIdDireccion(RS.getInt("idDireccion"));
				dir.setLocalidad(RS.getString("localidad"));
				dir.setNumero(RS.getString("numero"));
				dir.setPiso(RS.getString("piso"));
				dir.setProvincia(RS.getString("provincia"));
				Persona per = new Persona();
				per.setCuilcuit(RS.getString(3));
				per.setDireccion(dir);
				per.setDireccionTexto(RS.getString(5));
				per.setTipo(RS.getString(6));
				per.setNombre(RS.getString(7));
				per.setTelefono(RS.getString(8));
				Proveedor prov = new Proveedor();
				prov.setPersona(per);
				prov.setIdProveedor(RS.getInt(1));
				lista.add(prov);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
}
