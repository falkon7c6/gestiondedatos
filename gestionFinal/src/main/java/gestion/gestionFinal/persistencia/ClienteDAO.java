package gestion.gestionFinal.persistencia;


import gestion.gestionFinal.modelo.Cliente;
import gestion.gestionFinal.modelo.Direccion;
import gestion.gestionFinal.modelo.Persona;
import gestion.gestionFinal.util.DialogsUtil;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ClienteDAO {
	
	databaseConnection conexion;

	private CallableStatement llamada;

	public ClienteDAO() {
		conexion = new databaseConnection();
	}

	public int insertar(Cliente cli) {
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Cliente(?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setString(2, cli.getPersona().getCuilcuit());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return id;
	}

	public void borrar(int id) throws SQLException {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Cliente(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("cliente borrado");
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	
	public void editar(Cliente cli) {
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Cliente(? , ?)}");
			llamada.setInt(1, cli.getIdCliente());
			llamada.setString(2, cli.getPersona().getCuilcuit());
			llamada.execute();
			System.out.println("Cliente editado");
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		
		
	}
	
	
	
	public ObservableList<Cliente> listar() {
		ObservableList<Cliente> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {

			llamada = conexion.connection
					.prepareCall("{call Listar_Cliente()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Cliente cli = new Cliente();
				Direccion dir = new Direccion();
				dir.setCalle(RS.getString("calle"));
				dir.setDepartamento(RS.getString("departamento"));
				dir.setIdDireccion(RS.getInt("idDireccion"));
				dir.setLocalidad(RS.getString("localidad"));
				dir.setNumero(RS.getString("numero"));
				dir.setPiso(RS.getString("piso"));
				dir.setProvincia(RS.getString("provincia"));
				Persona per = new Persona();
				per.setCuilcuit(RS.getString("CUILCUIT"));
				per.setDireccion(dir);
				per.setDireccionTexto(RS.getString("direccionTexto"));
				per.setNombre(RS.getString("nombre"));
				per.setTelefono(RS.getString("telefono"));
				per.setTipo(RS.getString("tipo"));
				cli.setPersona(per);
				cli.setIdCliente(RS.getInt("idCliente"));
				lista.add(cli);
			}

		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
}
