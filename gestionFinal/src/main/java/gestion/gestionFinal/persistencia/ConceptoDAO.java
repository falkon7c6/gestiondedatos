package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Conceptoliquidacion;
import gestion.gestionFinal.util.DialogsUtil;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ConceptoDAO {

	databaseConnection conexion;

	private CallableStatement llamada;

	public ConceptoDAO() {
		conexion = new databaseConnection();
	}

	public int insertar(Conceptoliquidacion CL) {
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_ConceptoLiquidacion(?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setString(2, CL.getNombre());
			llamada.setFloat(3, CL.getMonto());
			llamada.setString(4, CL.getDescripcion());
			llamada.setString(5, CL.getTipo());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return id;
	}
	
	public ObservableList<Conceptoliquidacion> listar() {
		ObservableList<Conceptoliquidacion> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {

			llamada = conexion.connection
					.prepareCall("{call Listar_Conceptoliquidacion()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Conceptoliquidacion con = new Conceptoliquidacion();
				con.setIdConceptoLiquidacion(RS.getInt(1));
				con.setNombre(RS.getString(2));
				con.setMonto(RS.getFloat(3));
				con.setDescripcion(RS.getString(4));
				con.setTipo(RS.getString(5));
				lista.add(con);
			}

		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
	
	public ObservableList<Conceptoliquidacion> listarConceptosObligatorios() {
		ObservableList<Conceptoliquidacion> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {

			llamada = conexion.connection
					.prepareCall("{call Listar_ConceptosObligatorios}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Conceptoliquidacion con = new Conceptoliquidacion();
				con.setIdConceptoLiquidacion(RS.getInt(1));
				con.setNombre(RS.getString(2));
				//con.setMonto(RS.getFloat(3));
				con.setDescripcion(RS.getString(4));
				con.setTipo(RS.getString(5));
				lista.add(con);
			}

		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}

	public void borrar(int idConceptoLiquidacion) throws SQLException{
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_ConceptoLiquidacion(?)}");
			llamada.setInt(1, idConceptoLiquidacion);
			llamada.execute();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}		
	}

	public void editar(Conceptoliquidacion con) throws SQLException{
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Modificacion_ConceptoLiquidacion(?,?,?,?,?)}");
			llamada.setString(2, con.getNombre());
			llamada.setFloat(4, con.getMonto());
			llamada.setString(3, con.getDescripcion());
			llamada.setString(5, con.getTipo());
			llamada.setInt(1, con.getIdConceptoLiquidacion());
			llamada.execute();
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}	
	}
	
}
