package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Catalogo;
import gestion.gestionFinal.modelo.Lineacompra;
import gestion.gestionFinal.modelo.Producto;
import gestion.gestionFinal.util.DialogsUtil;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class LineaCompraDAO {

	databaseConnection conexion;

	private CallableStatement llamada;

	public LineaCompraDAO() {
		conexion = new databaseConnection();
	}

	public int insertar(Lineacompra LC) {
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_LineaCompra(?,?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setInt(2, LC.getCompra().getIdCompra());
			llamada.setInt(3, LC.getProducto().getIdProducto());
			llamada.setInt(4, LC.getCantidad());
			llamada.setFloat(5, LC.getPrecio());
			llamada.setFloat(6, LC.getSubtotal());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return id;
	}

	public void borrar(int id) {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_LineaCompra(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	
	public ObservableList<Lineacompra> listar() {
		ObservableList<Lineacompra> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {

			llamada = conexion.connection
					.prepareCall("{call Listar_LineaCompra()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {

				Lineacompra lc = new Lineacompra();
				lc.setIdLineaCompra(RS.getInt(1));
				//lc.setCompra(RS.getInt(2));
				Producto prod = new Producto();
				Catalogo cat = new Catalogo();
				cat.setPrecio(RS.getFloat(2));
				prod.setNombre(RS.getString(3));
				prod.setIdProducto(RS.getInt(4));
				prod.getListaCatalogo().add(cat);
				lc.setProducto(prod);
				lc.setCantidad(RS.getInt(5));
				lc.setPrecio(RS.getFloat(6));
				lc.setSubtotal(RS.getFloat(7));

				lista.add(lc);
			}

		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
}
