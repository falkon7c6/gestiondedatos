package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Venta;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

public class VentaDAO {

	databaseConnection conexion;

	private CallableStatement llamada;

	public VentaDAO() {
		conexion = new databaseConnection();

	}

	public int insertar(Venta ven) {
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Venta(?,?,?,?,?,?,?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setInt(5, ven.getCliente().getIdCliente());
			llamada.setDate(3, java.sql.Date.valueOf(ven.getFecha()));
			llamada.setFloat(4, ven.getTotal());
			llamada.setInt(2, ven.getEmpleado().getLegajo());
			llamada.setInt(6, ven.getPeriodo().getIdPeriodo());
			llamada.setString(7, ven.getTipoFactura());
			llamada.setInt(8, ven.getNumeroComprobante());
			llamada.setFloat(9, ven.getBonificacion());
			llamada.setString(10, ven.getObservaciones());
			llamada.setString(11, ven.getCondicionIVA());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return id;
	}

	public void borrar(int id) {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Venta(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void editar(Venta ven) {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Modificacion_Venta(?,?)}");
			//llamada.setInt(1, cat.getIdCategoriaProducto());
			//llamada.setString(2, cat.getNombre());
			llamada.execute();
			System.out.println("Objeto editado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


}
