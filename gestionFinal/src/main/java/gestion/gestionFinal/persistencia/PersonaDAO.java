package gestion.gestionFinal.persistencia;



import gestion.gestionFinal.modelo.Persona;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

public class PersonaDAO {

	databaseConnection conexion;

	private CallableStatement llamada;
	
	public PersonaDAO(){
		conexion = new databaseConnection();

	}
	
	public String insertar(Persona per) throws SQLException{
		String CuilCuit = "";
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Persona(?,?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.VARCHAR);	
			llamada.setString(1, per.getCuilcuit());
			llamada.setInt(2, per.getDireccion().getIdDireccion());
			llamada.setString(3, per.getDireccionTexto());	
			llamada.setString(4, per.getTipo());
			llamada.setString(5, per.getNombre());
			llamada.setString(6, per.getTelefono());
			llamada.execute();
			CuilCuit = llamada.getString(1);
			System.out.println("objeto insertado con id = " + CuilCuit);
		
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return CuilCuit;
	}
	
	public void editar(Persona per, String CuilViejo){
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Persona(?,?,?,?,?,?,?)}");
			llamada.setString(1, CuilViejo);
			llamada.setString(2, per.getCuilcuit());
			llamada.setInt(3, per.getDireccion().getIdDireccion());
			llamada.setString(4, per.getDireccionTexto());
			llamada.setString(5, per.getTipo());
			llamada.setString(6, per.getNombre());
			llamada.setString(7, per.getTelefono());
			llamada.execute();
			System.out.println("Objeto editado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
