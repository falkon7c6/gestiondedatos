package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Liquidacion;
import gestion.gestionFinal.util.DialogsUtil;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

public class LiquidacionDAO {
	databaseConnection conexion;

	private CallableStatement llamada;

	public LiquidacionDAO() {
		conexion = new databaseConnection();
	}

	public int insertar(Liquidacion L) {
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Liquidacion(?,?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setInt(2, L.getEmpleado().getLegajo());
			llamada.setInt(3, L.getPeriodo().getIdPeriodo());
			llamada.setDate(4, java.sql.Date.valueOf(L.getFechaPago()));
			llamada.setFloat(5, L.getTotal());
			llamada.setDate(6, java.sql.Date.valueOf(L.getFechaLiquidacion()));
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return id;
	}
}
