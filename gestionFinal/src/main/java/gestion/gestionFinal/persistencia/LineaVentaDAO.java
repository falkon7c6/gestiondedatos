package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Catalogo;
import gestion.gestionFinal.modelo.Lineaventa;
import gestion.gestionFinal.modelo.Producto;
import gestion.gestionFinal.util.DialogsUtil;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class LineaVentaDAO{

	databaseConnection conexion;

	private CallableStatement llamada;

	public LineaVentaDAO() {
		conexion = new databaseConnection();
	}

	public int insertar(Lineaventa LC) {
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_LineaVenta(?,?,?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setInt(2, LC.getVenta().getIdVenta());
			llamada.setInt(3, LC.getProducto().getIdProducto());
			llamada.setInt(4, LC.getCantidad());
			llamada.setFloat(5, LC.getPrecioUnitario());
			llamada.setFloat(6, LC.getSubtotal());
			llamada.setFloat(7, LC.getDescuento());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return id;
	}

	public void borrar(int id) {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_LineaVenta(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	
	
	
	

	ObservableList<Lineaventa> listar() {
		ObservableList<Lineaventa> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {

			llamada = conexion.connection
					.prepareCall("{call Listar_LineaVenta()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {

				Lineaventa lc = new Lineaventa();
				lc.setIdLineaVenta(RS.getInt(1));
				//lc.setCompra(RS.getInt(2));
				Producto prod = new Producto();
				Catalogo cat = new Catalogo();
				cat.setPrecio(RS.getFloat(2));
				prod.setNombre(RS.getString(3));
				prod.setIdProducto(RS.getInt(4));
				prod.getListaCatalogo().add(cat);
				lc.setProducto(prod);
				lc.setCantidad(RS.getInt(5));
				lc.setPrecioUnitario(RS.getFloat(6));
				lc.setSubtotal(RS.getFloat(7));
				lc.setSubtotal(RS.getFloat(8));

				lista.add(lc);
			}

		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}

}
