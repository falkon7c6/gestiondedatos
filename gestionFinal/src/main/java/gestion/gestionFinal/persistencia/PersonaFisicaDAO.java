package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Direccion;
import gestion.gestionFinal.modelo.Persona;
import gestion.gestionFinal.modelo.Personafisica;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class PersonaFisicaDAO {

	databaseConnection conexion;

	private CallableStatement llamada;

	public PersonaFisicaDAO() {
		conexion = new databaseConnection();

	}

	public int insertar(Personafisica per) throws SQLException{
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Persona_Fisica(?,?,?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setString(2, per.getPersona().getCuilcuit());
			llamada.setString(3, per.getApellido());
			llamada.setString(4, per.getNombrePila());
			llamada.setString(5, per.getSexo());
			if (per.getPadre() != null) {
				llamada.setInt(6, per.getPadre().getIdPersonaFisica());
			}else llamada.setNull(6, java.sql.Types.INTEGER);
			if(per.getConyuge() != null){
			llamada.setInt(7, per.getConyuge().getIdPersonaFisica());
			}else llamada.setNull(7, java.sql.Types.INTEGER);
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return id;
	}

	public void editar(Personafisica per) throws MySQLIntegrityConstraintViolationException{
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Modificacion_Persona_Fisica(?,?,?,?,?,?,?)}");
			llamada.setInt(1, per.getIdPersonaFisica());
			llamada.setString(2, per.getPersona().getCuilcuit());
			llamada.setString(3, per.getApellido());
			llamada.setString(4, per.getNombrePila());
			llamada.setString(5, per.getSexo());
			if (per.getPadre() != null) {
				llamada.setInt(6, per.getPadre().getIdPersonaFisica());
			}else llamada.setNull(6, java.sql.Types.INTEGER);
			if(per.getConyuge() != null){
			llamada.setInt(7, per.getConyuge().getIdPersonaFisica());
			}else llamada.setNull(7, java.sql.Types.INTEGER);
			llamada.execute();
			System.out.println("Objeto editado");
		} catch (SQLException e) {
			throw new MySQLIntegrityConstraintViolationException();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void quitarPadre(int idPersonaFisica) {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Quitar_Padre(?)}");
			llamada.setInt(1, idPersonaFisica);
			llamada.execute();
			System.out.println("Objeto editado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	public ObservableList<Personafisica> listar() {
		ObservableList<Personafisica> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_Persona_Fisica()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Direccion dir = new Direccion();
				dir.setCalle(RS.getString("calle"));
				dir.setDepartamento(RS.getString("departamento"));
				dir.setIdDireccion(RS.getInt("idDireccion"));
				dir.setLocalidad(RS.getString("localidad"));
				dir.setNumero(RS.getString("numero"));
				dir.setPiso(RS.getString("piso"));
				dir.setProvincia(RS.getString("provincia"));
				Persona per = new Persona();
				per.setCuilcuit(RS.getString("CUIL_p"));
				per.setDireccion(dir);
				per.setDireccionTexto(RS.getString("direccionTexto"));
				per.setNombre(RS.getString("nombre_persona"));
				per.setTelefono(RS.getString("telefono"));
				per.setTipo(RS.getString("tipo"));
				Personafisica perF = new Personafisica();
				perF.setApellido(RS.getString("apellido"));
				//perF.setConyuge(conyuge);
				perF.setIdPersonaFisica(RS.getInt("idPersonaFisica"));
				perF.setNombrePila(RS.getString("nombrePila"));
				//perF.setPadre(padre);
				perF.setPersona(per);
				perF.setSexo(RS.getString("sexo"));
				lista.add(perF);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}

	public ObservableList<Personafisica> listarSinFamilia() {
		ObservableList<Personafisica> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_Persona_Sin_Flia()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Direccion dir = new Direccion();
				dir.setCalle(RS.getString("calle"));
				dir.setDepartamento(RS.getString("departamento"));
				dir.setIdDireccion(RS.getInt("idDireccion"));
				dir.setLocalidad(RS.getString("localidad"));
				dir.setNumero(RS.getString("numero"));
				dir.setPiso(RS.getString("piso"));
				dir.setProvincia(RS.getString("provincia"));
				Persona per = new Persona();
				per.setCuilcuit(RS.getString("CUIL_p"));
				per.setDireccion(dir);
				per.setDireccionTexto(RS.getString("direccionTexto"));
				per.setNombre(RS.getString("nombre_persona"));
				per.setTelefono(RS.getString("telefono"));
				per.setTipo(RS.getString("tipo"));
				Personafisica perF = new Personafisica();
				perF.setApellido(RS.getString("apellido"));
				//perF.setConyuge(conyuge);
				perF.setIdPersonaFisica(RS.getInt("idPersonaFisica"));
				perF.setNombrePila(RS.getString("nombrePila"));
				//perF.setPadre(padre);
				perF.setPersona(per);
				perF.setSexo(RS.getString("sexo"));
				lista.add(perF);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
}
