package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Calificacionprofesional;
import gestion.gestionFinal.modelo.Categoriaempleado;
import gestion.gestionFinal.modelo.Direccion;
import gestion.gestionFinal.modelo.Empleado;
import gestion.gestionFinal.modelo.Obrasocial;
import gestion.gestionFinal.modelo.Persona;
import gestion.gestionFinal.modelo.Personafisica;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class EmpleadoDAO {
	
	databaseConnection conexion;

	private CallableStatement llamada;
	
	public EmpleadoDAO(){
		conexion = new databaseConnection();

	}
	
	public int insertar(Empleado emp) throws SQLException{
		int legajo = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Empleado(?,?,?,?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setInt(2, emp.getCategoriaempleado().getIdCategoriaEmpleado());
			llamada.setInt(3, emp.getCalificacionprofesional().getIdCalificacionProfesional());
			llamada.setInt(4, emp.getActivo());
			llamada.setInt(5, emp.getObrasocial().getIdObraSocial());
			llamada.setInt(6, emp.getPersonafisica().getIdPersonaFisica());
			llamada.setDate(7, java.sql.Date.valueOf(emp.getFechaIngreso()));
			llamada.setInt(8, emp.getHorasSemanales());
			
			llamada.execute();
			legajo = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + legajo);
		
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return legajo;
	}
	
	public void borrar(int id){
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Empleado(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void borrarLogico(int id) {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Logica_Empleado(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public void editar(Empleado emp){
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Empleado(?,?,?,?,?,?,?)}");
			llamada.setInt(1, emp.getLegajo());
			llamada.setInt(2, emp.getCategoriaempleado().getIdCategoriaEmpleado());
			llamada.setInt(3, emp.getCalificacionprofesional().getIdCalificacionProfesional());
			llamada.setInt(4, emp.getActivo());
			llamada.setInt(5, emp.getObrasocial().getIdObraSocial());
			llamada.setDate(6, java.sql.Date.valueOf(emp.getFechaIngreso()));
			llamada.setInt(7, emp.getHorasSemanales());
			llamada.execute();
			System.out.println("Objeto editado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public ObservableList<Empleado> listar() {
		ObservableList<Empleado> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_Empleado()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Direccion dir = new Direccion();
				dir.setCalle(RS.getString("calle"));
				dir.setDepartamento(RS.getString("departamento"));
				dir.setIdDireccion(RS.getInt("idDireccion"));
				dir.setLocalidad(RS.getString("localidad"));
				dir.setNumero(RS.getString("numero"));
				dir.setPiso(RS.getString("piso"));
				dir.setProvincia(RS.getString("provincia"));
				Persona per = new Persona();
				per.setCuilcuit(RS.getString("CUIL_p"));
				per.setDireccion(dir);
				per.setDireccionTexto(RS.getString("direccionTexto"));
				per.setNombre(RS.getString("nombre_persona"));
				per.setTelefono(RS.getString("telefono"));
				per.setTipo(RS.getString("tipo"));
				Personafisica perF = new Personafisica();
				perF.setApellido(RS.getString("apellido"));
				//perF.setConyuge(conyuge);
				perF.setIdPersonaFisica(RS.getInt("idPersonaFisica"));
				perF.setNombrePila(RS.getString("nombrePila"));
				//perF.setPadre(padre);
				perF.setPersona(per);
				perF.setSexo(RS.getString("sexo"));
				Calificacionprofesional cal = new Calificacionprofesional();
				cal.setDescripcion(RS.getString("cal_descripcion"));
				cal.setIdCalificacionProfesional(RS.getInt("idCal"));
				cal.setImporteFijo(RS.getFloat("cal_importeFijo"));
				cal.setSalarioHora(RS.getFloat("cal_salarioHora"));
				Obrasocial obra = new Obrasocial();
				obra.setIdObraSocial(RS.getInt("idObra"));
				obra.setImporteFijo(RS.getFloat("o_importeFijo"));
				obra.setImportePorcentual(RS.getFloat("o_importePorcentual"));
				obra.setNombre(RS.getString("o_nombre"));
				Categoriaempleado cat = new Categoriaempleado();
				cat.setDescripcion(RS.getString("cat_descripcion"));
				cat.setIdCategoriaEmpleado(RS.getInt("idCat"));
				cat.setSalarioHora(RS.getFloat("cat_salarioHora"));
				Empleado emp = new Empleado();
				emp.setPersonafisica(perF);
				emp.setActivo(RS.getInt("activo"));
				emp.setFechaIngreso(RS.getDate("fechaIngreso").toLocalDate());
				emp.setLegajo(RS.getInt("Legajo"));
				emp.setHorasSemanales(RS.getInt("HorasSemanales"));
				emp.setCalificacionprofesional(cal);
				emp.setObrasocial(obra);
				emp.setCategoriaempleado(cat);
				emp.setHijos(this.listarHijos(emp));
				emp.setFamilia(this.listarFamilia(emp));
				emp.setConyuge(this.obtenerPareja(emp));
				lista.add(emp);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}

	public ObservableList<Empleado> listarActivos() {
		ObservableList<Empleado> lista = FXCollections.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_EmpleadoActivo()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Direccion dir = new Direccion();
				dir.setCalle(RS.getString("calle"));
				dir.setDepartamento(RS.getString("departamento"));
				dir.setIdDireccion(RS.getInt("idDireccion"));
				dir.setLocalidad(RS.getString("localidad"));
				dir.setNumero(RS.getString("numero"));
				dir.setPiso(RS.getString("piso"));
				dir.setProvincia(RS.getString("provincia"));
				Persona per = new Persona();
				per.setCuilcuit(RS.getString("CUIL_p"));
				per.setDireccion(dir);
				per.setDireccionTexto(RS.getString("direccionTexto"));
				per.setNombre(RS.getString("nombre_persona"));
				per.setTelefono(RS.getString("telefono"));
				per.setTipo(RS.getString("tipo"));
				Personafisica perF = new Personafisica();
				perF.setApellido(RS.getString("apellido"));
				// perF.setConyuge(conyuge);
				perF.setIdPersonaFisica(RS.getInt("idPersonaFisica"));
				perF.setNombrePila(RS.getString("nombrePila"));
				// perF.setPadre(padre);
				perF.setPersona(per);
				perF.setSexo(RS.getString("sexo"));
				Calificacionprofesional cal = new Calificacionprofesional();
				cal.setDescripcion(RS.getString("cal_descripcion"));
				cal.setIdCalificacionProfesional(RS.getInt("idCal"));
				cal.setImporteFijo(RS.getFloat("cal_importeFijo"));
				cal.setSalarioHora(RS.getFloat("cal_salarioHora"));
				Obrasocial obra = new Obrasocial();
				obra.setIdObraSocial(RS.getInt("idObra"));
				obra.setImporteFijo(RS.getFloat("o_importeFijo"));
				obra.setImportePorcentual(RS.getFloat("o_importePorcentual"));
				obra.setNombre(RS.getString("o_nombre"));
				Categoriaempleado cat = new Categoriaempleado();
				cat.setDescripcion(RS.getString("cat_descripcion"));
				cat.setIdCategoriaEmpleado(RS.getInt("idCat"));
				cat.setSalarioHora(RS.getFloat("cat_salarioHora"));
				Empleado emp = new Empleado();
				emp.setPersonafisica(perF);
				emp.setActivo(RS.getInt("activo"));
				emp.setFechaIngreso(RS.getDate("fechaIngreso").toLocalDate());
				emp.setLegajo(RS.getInt("Legajo"));
				emp.setHorasSemanales(RS.getInt("HorasSemanales"));
				emp.setCalificacionprofesional(cal);
				emp.setObrasocial(obra);
				emp.setCategoriaempleado(cat);
				emp.setHijos(this.listarHijos(emp));
				emp.setFamilia(this.listarFamilia(emp));
				emp.setConyuge(this.obtenerPareja(emp));
				lista.add(emp);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}

	public ObservableList<Personafisica> listarFamilia(Empleado emp) {
		ObservableList<Personafisica> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Listar_Familia(?)}");
			llamada.setInt(1, emp.getPersonafisica().getIdPersonaFisica());
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Direccion dir = new Direccion();
				dir.setCalle(RS.getString("calle"));
				dir.setDepartamento(RS.getString("departamento"));
				dir.setIdDireccion(RS.getInt("idDireccion"));
				dir.setLocalidad(RS.getString("localidad"));
				dir.setNumero(RS.getString("numero"));
				dir.setPiso(RS.getString("piso"));
				dir.setProvincia(RS.getString("provincia"));
				Persona per = new Persona();
				per.setCuilcuit(RS.getString("CUIL_p"));
				per.setDireccion(dir);
				per.setDireccionTexto(RS.getString("direccionTexto"));
				per.setNombre(RS.getString("nombre_persona"));
				per.setTelefono(RS.getString("telefono"));
				per.setTipo(RS.getString("tipo"));
				Personafisica perF = new Personafisica();
				perF.setApellido(RS.getString("apellido"));
				//perF.setConyuge(conyuge);
				perF.setIdPersonaFisica(RS.getInt("idPersonaFisica"));
				perF.setNombrePila(RS.getString("nombrePila"));
				//perF.setPadre(padre);
				perF.setPersona(per);
				perF.setSexo(RS.getString("sexo"));
				lista.add(perF);
			}

		} catch (SQLException e) {
			// e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}

	public ObservableList<Personafisica> listarHijos(Empleado emp) {
			ObservableList<Personafisica> lista = FXCollections
					.observableArrayList();
			conexion.conectar();
			try {
				llamada = conexion.connection
						.prepareCall("{call Listar_Hijos(?)}");
				llamada.setInt(1, emp.getPersonafisica().getIdPersonaFisica());
				llamada.execute();
				ResultSet RS = llamada.getResultSet();
				while (RS.next()) {
					Direccion dir = new Direccion();
					dir.setCalle(RS.getString("calle"));
					dir.setDepartamento(RS.getString("departamento"));
					dir.setIdDireccion(RS.getInt("idDireccion"));
					dir.setLocalidad(RS.getString("localidad"));
					dir.setNumero(RS.getString("numero"));
					dir.setPiso(RS.getString("piso"));
					dir.setProvincia(RS.getString("provincia"));
					Persona per = new Persona();
					per.setCuilcuit(RS.getString("CUIL_p"));
					per.setDireccion(dir);
					per.setDireccionTexto(RS.getString("direccionTexto"));
					per.setNombre(RS.getString("nombre_persona"));
					per.setTelefono(RS.getString("telefono"));
					per.setTipo(RS.getString("tipo"));
					Personafisica perF = new Personafisica();
					perF.setApellido(RS.getString("apellido"));
					//perF.setConyuge(conyuge);
					perF.setIdPersonaFisica(RS.getInt("idPersonaFisica"));
					perF.setNombrePila(RS.getString("nombrePila"));
					perF.setPadre(emp.getPersonafisica());
					perF.setPersona(per);
					perF.setSexo(RS.getString("sexo"));
					lista.add(perF);
				}

			} catch (SQLException e) {
			// e.printStackTrace();
			} finally {

				if (llamada != null) {

					try {
						llamada.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (conexion.connection != null) {
					try {
						conexion.connection.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

			}
			return lista;
		
	}

	public Personafisica obtenerPareja(Empleado emp) {
		Personafisica pareja = new Personafisica();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Obtener_Pareja(?)}");
			llamada.setInt(1, emp.getPersonafisica().getIdPersonaFisica());
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Direccion dir = new Direccion();
				dir.setCalle(RS.getString("calle"));
				dir.setDepartamento(RS.getString("departamento"));
				dir.setIdDireccion(RS.getInt("idDireccion"));
				dir.setLocalidad(RS.getString("localidad"));
				dir.setNumero(RS.getString("numero"));
				dir.setPiso(RS.getString("piso"));
				dir.setProvincia(RS.getString("provincia"));
				Persona per = new Persona();
				per.setCuilcuit(RS.getString("CUIL_p"));
				per.setDireccion(dir);
				per.setDireccionTexto(RS.getString("direccionTexto"));
				per.setNombre(RS.getString("nombre_persona"));
				per.setTelefono(RS.getString("telefono"));
				per.setTipo(RS.getString("tipo"));
				Personafisica perF = new Personafisica();
				pareja.setApellido(RS.getString("apellido"));
				pareja.setConyuge(emp.getPersonafisica());
				pareja.setIdPersonaFisica(RS.getInt("idPersonaFisica"));
				pareja.setNombrePila(RS.getString("nombrePila"));
				//perF.setPadre(padre);
				pareja.setPersona(per);
				pareja.setSexo(RS.getString("sexo"));
			}

		} catch (SQLException e) {
			// e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return pareja;
	}
	
	public ObservableList<Empleado> obtenerEmpleado(int legajo) {
		ObservableList<Empleado> lista = FXCollections.observableArrayList();
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call ObtenerEmpleado(?)}");
			llamada.setInt(1, legajo);
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Direccion dir = new Direccion();
				dir.setCalle(RS.getString("calle"));
				dir.setDepartamento(RS.getString("departamento"));
				dir.setIdDireccion(RS.getInt("idDireccion"));
				dir.setLocalidad(RS.getString("localidad"));
				dir.setNumero(RS.getString("numero"));
				dir.setPiso(RS.getString("piso"));
				dir.setProvincia(RS.getString("provincia"));
				Persona per = new Persona();
				per.setCuilcuit(RS.getString("CUIL_p"));
				per.setDireccion(dir);
				per.setDireccionTexto(RS.getString("direccionTexto"));
				per.setNombre(RS.getString("nombre_persona"));
				per.setTelefono(RS.getString("telefono"));
				per.setTipo(RS.getString("tipo"));
				Personafisica perF = new Personafisica();
				perF.setApellido(RS.getString("apellido"));
				// perF.setConyuge(conyuge);
				perF.setIdPersonaFisica(RS.getInt("idPersonaFisica"));
				perF.setNombrePila(RS.getString("nombrePila"));
				// perF.setPadre(padre);
				perF.setPersona(per);
				perF.setSexo(RS.getString("sexo"));
				Calificacionprofesional cal = new Calificacionprofesional();
				cal.setDescripcion(RS.getString("cal_descripcion"));
				cal.setIdCalificacionProfesional(RS.getInt("idCal"));
				cal.setImporteFijo(RS.getFloat("cal_importeFijo"));
				cal.setSalarioHora(RS.getFloat("cal_salarioHora"));
				Obrasocial obra = new Obrasocial();
				obra.setIdObraSocial(RS.getInt("idObra"));
				obra.setImporteFijo(RS.getFloat("o_importeFijo"));
				obra.setImportePorcentual(RS.getFloat("o_importePorcentual"));
				obra.setNombre(RS.getString("o_nombre"));
				Categoriaempleado cat = new Categoriaempleado();
				cat.setDescripcion(RS.getString("cat_descripcion"));
				cat.setIdCategoriaEmpleado(RS.getInt("idCat"));
				cat.setSalarioHora(RS.getFloat("cat_salarioHora"));
				Empleado emp = new Empleado();
				emp.setPersonafisica(perF);
				emp.setActivo(RS.getInt("activo"));
				emp.setFechaIngreso(RS.getDate("fechaIngreso").toLocalDate());
				emp.setLegajo(RS.getInt("Legajo"));
				emp.setHorasSemanales(RS.getInt("HorasSemanales"));
				emp.setCalificacionprofesional(cal);
				emp.setObrasocial(obra);
				emp.setCategoriaempleado(cat);
				emp.setHijos(this.listarHijos(emp));
				emp.setFamilia(this.listarFamilia(emp));
				emp.setConyuge(this.obtenerPareja(emp));
				lista.add(emp);
			}

		} catch (SQLException e) {
			// e.printStackTrace();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
}
