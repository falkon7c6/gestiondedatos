package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Sueldo;
import gestion.gestionFinal.util.DialogsUtil;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

public class SueldoDAO {
	databaseConnection conexion;

	private CallableStatement llamada;

	public SueldoDAO() {
		conexion = new databaseConnection();
	}

	public int insertar(Sueldo su) {
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Sueldo(?,?,?,?,?,?,?,?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setString(2, su.getLegajo());
			llamada.setString(3, su.getCentroCostos());
			llamada.setString(4, su.getApellidos());
			llamada.setString(5, su.getNombres());
			llamada.setString(6, su.getCategoriaEmpleado());
			llamada.setString(7, su.getCalificacionProfesional());
			llamada.setString(8, su.getObraSocial());
			llamada.setString(9, su.getSueldo());
			llamada.setString(10, su.getDeducciones());
			llamada.setString(11, su.getNetoCobrar());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return id;
	}
	
	
}
