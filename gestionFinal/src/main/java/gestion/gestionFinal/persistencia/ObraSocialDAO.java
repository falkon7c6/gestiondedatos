package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Obrasocial;
import gestion.gestionFinal.util.DialogsUtil;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ObraSocialDAO {
	databaseConnection conexion;

	private CallableStatement llamada;

	public ObraSocialDAO() {
		conexion = new databaseConnection();
	}

	public int insertar(Obrasocial OS) throws SQLException{
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Obra_Social(?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setString(2, OS.getNombre());
			llamada.setFloat(3, OS.getImporteFijo());
			llamada.setFloat(4, OS.getImportePorcentual());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return id;
	}

	public void borrar(int id) throws SQLException {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Obra_Social(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	
	public void editar(Obrasocial OS) {
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Obra_Social(? , ?, ?, ?)}");
			llamada.setInt(1, OS.getIdObraSocial());
			llamada.setString(2, OS.getNombre());
			llamada.setFloat(3, OS.getImporteFijo());
			llamada.setFloat(4, OS.getImportePorcentual());
			llamada.execute();
			System.out.println("categoria editada");
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		
		
	}
	
	
	
	public ObservableList<Obrasocial> listar() {
		ObservableList<Obrasocial> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {

			llamada = conexion.connection
					.prepareCall("{call Listar_Obra_Social()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {

				Obrasocial os = new Obrasocial();
				os.setIdObraSocial(RS.getInt(1));
				os.setNombre(RS.getString(2));
				os.setImporteFijo(RS.getFloat(3));
				os.setImportePorcentual(RS.getFloat(4));
				lista.add(os);
			}

		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
}


