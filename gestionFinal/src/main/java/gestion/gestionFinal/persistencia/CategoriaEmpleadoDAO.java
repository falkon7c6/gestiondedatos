package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Categoriaempleado;
import gestion.gestionFinal.util.DialogsUtil;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CategoriaEmpleadoDAO {

	databaseConnection conexion;

	private CallableStatement llamada;

	public CategoriaEmpleadoDAO() {
		conexion = new databaseConnection();
	}

	public int insertar(Categoriaempleado CE) throws SQLException{
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Categoria_Empleado(?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setString(2, CE.getDescripcion());
			llamada.setFloat(3, CE.getSalarioHora());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return id;
	}

	public void borrar(int id) throws SQLException{
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Categoria_Empleado(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	
	public void editar(Categoriaempleado CE) {
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Categoria_Empleado(? , ?, ?)}");
			llamada.setInt(1, CE.getIdCategoriaEmpleado());
			llamada.setString(2, CE.getDescripcion());
			llamada.setFloat(3, CE.getSalarioHora());
			llamada.execute();
			System.out.println("categoria editada");
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		
		
	}
	
	public ObservableList<Categoriaempleado> listar() {
		ObservableList<Categoriaempleado> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {

			llamada = conexion.connection
					.prepareCall("{call Listar_Categoria_Empleado()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {

				Categoriaempleado cp = new Categoriaempleado(
						RS.getInt("idCategoriaEmpleado"),
						RS.getString("descripcion"),
						RS.getFloat("salarioHora"));
				lista.add(cp);
			}

		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
}
