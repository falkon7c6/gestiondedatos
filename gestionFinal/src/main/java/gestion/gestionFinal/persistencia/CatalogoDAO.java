package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Catalogo;

import java.sql.CallableStatement;
import java.sql.SQLException;

public class CatalogoDAO {
	databaseConnection conexion;

	private CallableStatement llamada;

	public CatalogoDAO() {
		conexion = new databaseConnection();
	}
	
	public int insertar(Catalogo cat) throws SQLException{
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Catalogo(?,?,?)}");
			llamada.setInt(1, cat.getProducto().getIdProducto());
			llamada.setInt(2, cat.getProveedor().getIdProveedor());
			llamada.setFloat(3, cat.getPrecio());
			llamada.execute();
			System.out.println("Objeto ingresado");
		 
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return id;
	}

	public void borrar(int idProducto, int idProveedor) {
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Catalogo(?,?)}");
			llamada.setInt(1, idProducto);
			llamada.setInt(2, idProveedor);
			llamada.execute();
			System.out.println("Objeto borrado");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	
	public void editar(Catalogo cat) throws SQLException {
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Catalogo(?,?,?)}");
			llamada.setInt(1, cat.getProducto().getIdProducto());
			llamada.setInt(2, cat.getProveedor().getIdProveedor());
			llamada.setFloat(3, cat.getPrecio());
			llamada.execute();
			System.out.println("Objeto editado");
		} finally {
			if (llamada != null) {
				try {
					llamada.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
