package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Periodo;
import gestion.gestionFinal.util.DialogsUtil;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PeriodoDAO {
	databaseConnection conexion;

	private CallableStatement llamada;

	public PeriodoDAO() {
		conexion = new databaseConnection();
	}

	public int insertar(Periodo per) throws SQLException{
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Periodo(?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setString(4, per.getDescripcion());
			llamada.setString(2, per.getAnio());
			llamada.setString(3, per.getMes());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					// e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return id;
	}

	public void borrar(int id) throws SQLException{
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Periodo(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("Objeto borrado");
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	
	public void editar(Periodo per) {
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Periodo(? , ?, ?, ?)}");
			llamada.setInt(1, per.getIdPeriodo());
			llamada.setString(4, per.getDescripcion());
			llamada.setString(2, per.getAnio());
			llamada.setString(3, per.getMes());
			llamada.execute();
			System.out.println("periodo editado");
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		
		
	}
	
	
	
	public ObservableList<Periodo> listar() {
		ObservableList<Periodo> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {

			llamada = conexion.connection
					.prepareCall("{call Listar_Periodo()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {

				Periodo per = new Periodo();
				per.setIdPeriodo(RS.getInt(1));
				per.setDescripcion(RS.getString(4));
				per.setAnio(RS.getString(2));
				per.setMes(RS.getString(3));
				lista.add(per);
			}

		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}
}
