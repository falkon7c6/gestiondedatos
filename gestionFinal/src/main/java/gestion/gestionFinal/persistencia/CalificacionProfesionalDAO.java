package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.modelo.Calificacionprofesional;
import gestion.gestionFinal.util.DialogsUtil;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CalificacionProfesionalDAO {

	databaseConnection conexion;

	private CallableStatement llamada;

	public CalificacionProfesionalDAO() {
		conexion = new databaseConnection();
	}

	public int insertar(Calificacionprofesional CP) throws SQLException{
		int id = 0;
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Alta_Calificacion_Profesional(?,?,?,?)}");
			llamada.registerOutParameter(1, Types.INTEGER);
			llamada.setString(2, CP.getDescripcion());
			llamada.setFloat(3, CP.getSalarioHora());
			llamada.setFloat(4, CP.getImporteFijo());
			llamada.execute();
			id = llamada.getInt(1);
			System.out.println("objeto insertado con id = " + id);
		
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return id;
	}

	public void borrar(int id) throws SQLException{
		conexion.conectar();
		try {
			llamada = conexion.connection
					.prepareCall("{call Baja_Calificacion_Profesional(?)}");
			llamada.setInt(1, id);
			llamada.execute();
			System.out.println("calific prof borrada");
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	
	public void editar(Calificacionprofesional cp) {
		conexion.conectar();
		try {
			llamada = conexion.connection.prepareCall("{call Modificacion_Calificacion_Profesional(? , ? , ?, ?)}");
			llamada.setInt(1, cp.getIdCalificacionProfesional());
			llamada.setString(2, cp.getDescripcion());
			llamada.setFloat(3, cp.getSalarioHora());
			llamada.setFloat(4, cp.getImporteFijo());
			llamada.execute();
			System.out.println("calificacion editada");
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		
		
	}
	
	
	
	public ObservableList<Calificacionprofesional> listar() {
		ObservableList<Calificacionprofesional> lista = FXCollections
				.observableArrayList();
		conexion.conectar();
		try {

			llamada = conexion.connection
					.prepareCall("{call Listar_Calificacion_Profesional()}");
			llamada.execute();
			ResultSet RS = llamada.getResultSet();
			while (RS.next()) {
				Calificacionprofesional cp = new Calificacionprofesional();
				cp.setIdCalificacionProfesional(RS
						.getInt("idCalificacionProfesional"));
				cp.setDescripcion(RS.getString("descripcion"));
				cp.setImporteFijo(RS.getFloat("importeFijo"));
				cp.setSalarioHora(RS.getFloat("salarioHora"));

				System.out.println(RS.getFloat("salarioHora"));
				System.out.println("" + cp.getImporteFijo() + " "
						+ cp.getSalarioHora());
				lista.add(cp);
			}

		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return lista;
	}

}
