package gestion.gestionFinal.persistencia;

import javafx.collections.ObservableList;

public interface interfaceDAO {
	public int insertar(Object o);
	public void borrar(int id);
	public void editar(Object o);
	public ObservableList<Object> listar();
}
