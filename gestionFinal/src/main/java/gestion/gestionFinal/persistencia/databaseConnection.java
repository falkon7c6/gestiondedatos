package gestion.gestionFinal.persistencia;

import gestion.gestionFinal.util.DialogsUtil;
import io.datafx.controller.context.ApplicationContext;
import io.datafx.controller.context.FXMLApplicationContext;
import io.datafx.controller.injection.scopes.ApplicationScoped;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@ApplicationScoped
public class databaseConnection {

	public Connection connection = null;
	public Statement st = null;
	public CallableStatement myStmt = null;
	public String texto;
	private String usuario;
	private String clave;

	@FXMLApplicationContext
	ApplicationContext context;

	public databaseConnection() {


		this.context = ApplicationContext.getInstance();
		this.usuario = (String) context.getRegisteredObject("usuario");
		System.out.println(usuario);
		this.clave = (String) context.getRegisteredObject("clave");
		System.out.println(clave);
	}
			

	public void conectar() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			this.connection = DriverManager.getConnection(
					"jdbc:mysql://LocalHost:3306/gestionfinal", this.usuario,
					this.clave);
			this.st = connection.createStatement();
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL",
					"Error al conectar con la Base de Datos",
					"nombre de usuario o contraseña no validos");

		}
		

	}
	
	public void estaIniciada(){
		System.out.println("existe la instancia");
	}
	
	public Connection getConnection(){
		if(this.connection != null){
			conectar();
		}
		return this.connection;
	}

}