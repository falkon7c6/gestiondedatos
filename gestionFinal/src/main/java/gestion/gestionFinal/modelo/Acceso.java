package gestion.gestionFinal.modelo;



import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;



import java.time.LocalDate;
import java.util.Date;


/**
 * The persistent class for the acceso database table.
 * 
 */

public class Acceso{



	private SimpleIntegerProperty idAcceso = new SimpleIntegerProperty();


	private ObjectProperty<LocalDate> entrada = new SimpleObjectProperty<LocalDate>();


	private ObjectProperty<LocalDate> salida = new SimpleObjectProperty<LocalDate>();

	//bi-directional many-to-one association to Usuario
	private Usuario usuario;

	public Acceso() {
	}
	
	public Acceso(int idAcceso, LocalDate entrada, LocalDate salida) {
		this.idAcceso.set(idAcceso);
		this.entrada.set(entrada);
		this.salida.set(salida);
	}

	public final SimpleIntegerProperty idAccesoProperty() {
		return this.idAcceso;
	}

	public final int getIdAcceso() {
		return this.idAccesoProperty().get();
	}

	public final void setIdAcceso(final int idAcceso) {
		this.idAccesoProperty().set(idAcceso);
	}

	public final ObjectProperty<LocalDate> entradaProperty() {
		return this.entrada;
	}

	public final java.time.LocalDate getEntrada() {
		return this.entradaProperty().get();
	}

	public final void setEntrada(final java.time.LocalDate entrada) {
		this.entradaProperty().set(entrada);
	}

	public final ObjectProperty<LocalDate> salidaProperty() {
		return this.salida;
	}

	public final java.time.LocalDate getSalida() {
		return this.salidaProperty().get();
	}

	public final void setSalida(final java.time.LocalDate salida) {
		this.salidaProperty().set(salida);
	}
	

}