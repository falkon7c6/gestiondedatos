package gestion.gestionFinal.modelo;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;


/**
 * The persistent class for the liquidacion database table.
 * 
 */

public class Liquidacion{


	private SimpleIntegerProperty idLiquidacion = new SimpleIntegerProperty();


	private SimpleObjectProperty<LocalDate> fechaPago = new SimpleObjectProperty<LocalDate>();
	
	private SimpleObjectProperty<LocalDate> fechaLiquidacion = new SimpleObjectProperty<LocalDate>();

	private SimpleFloatProperty total = new SimpleFloatProperty();

	//bi-directional many-to-one association to Linealiquidacion
	private ObservableList<Linealiquidacion> lineas;

	//bi-directional many-to-one association to Empleado
	private SimpleObjectProperty<Empleado> empleado = new SimpleObjectProperty<Empleado>();

	//bi-directional many-to-one association to Periodo
	private SimpleObjectProperty<Periodo> periodo = new SimpleObjectProperty<Periodo>();

	public Liquidacion() {
	}

	public Liquidacion(int idLiquidacion, LocalDate fechaPago, float total, Empleado empleado,
			Periodo periodo) {

		this.idLiquidacion.set(idLiquidacion);
		this.fechaPago.set(fechaPago);
		this.total.set(total);
		this.empleado.set(empleado);
		this.periodo.set(periodo);
	}

	public final SimpleIntegerProperty idLiquidacionProperty() {
		return this.idLiquidacion;
	}

	public final int getIdLiquidacion() {
		return this.idLiquidacionProperty().get();
	}

	public final void setIdLiquidacion(final int idLiquidacion) {
		this.idLiquidacionProperty().set(idLiquidacion);
	}

	public final SimpleObjectProperty<LocalDate> fechaPagoProperty() {
		return this.fechaPago;
	}

	public final java.time.LocalDate getFechaPago() {
		return this.fechaPagoProperty().get();
	}

	public final void setFechaPago(final java.time.LocalDate fechaPago) {
		this.fechaPagoProperty().set(fechaPago);
	}

	public final SimpleFloatProperty totalProperty() {
		return this.total;
	}

	public final float getTotal() {
		return this.totalProperty().get();
	}

	public final void setTotal(final float total) {
		this.totalProperty().set(total);
	}

	public final SimpleObjectProperty<Empleado> empleadoProperty() {
		return this.empleado;
	}

	public final gestion.gestionFinal.modelo.Empleado getEmpleado() {
		return this.empleadoProperty().get();
	}

	public final void setEmpleado(
			final gestion.gestionFinal.modelo.Empleado empleado) {
		this.empleadoProperty().set(empleado);
	}

	public final SimpleObjectProperty<Periodo> periodoProperty() {
		return this.periodo;
	}

	public final gestion.gestionFinal.modelo.Periodo getPeriodo() {
		return this.periodoProperty().get();
	}

	public final void setPeriodo(final gestion.gestionFinal.modelo.Periodo periodo) {
		this.periodoProperty().set(periodo);
	}

	public final SimpleObjectProperty<LocalDate> fechaLiquidacionProperty() {
		return this.fechaLiquidacion;
	}

	public final java.time.LocalDate getFechaLiquidacion() {
		return this.fechaLiquidacionProperty().get();
	}

	public final void setFechaLiquidacion(final java.time.LocalDate fechaLiquidacion) {
		this.fechaLiquidacionProperty().set(fechaLiquidacion);
	}

	

}