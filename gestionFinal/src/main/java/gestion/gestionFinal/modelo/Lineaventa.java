package gestion.gestionFinal.modelo;

import java.math.BigDecimal;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;


/**
 * The persistent class for the lineaventa database table.
 * 
 */

public class Lineaventa{


	private SimpleIntegerProperty idLineaVenta = new SimpleIntegerProperty();

	private SimpleIntegerProperty cantidad = new SimpleIntegerProperty();

	private SimpleFloatProperty descuento = new SimpleFloatProperty();

	private SimpleFloatProperty precioUnitario = new SimpleFloatProperty();

	private SimpleFloatProperty retenciones = new SimpleFloatProperty();

	private SimpleFloatProperty subtotal = new SimpleFloatProperty();

	//bi-directional many-to-one association to Producto
	private SimpleObjectProperty<Producto> producto = new SimpleObjectProperty<Producto>();

	//bi-directional many-to-one association to Venta
	private SimpleObjectProperty<Venta> venta = new SimpleObjectProperty<Venta>();

	public Lineaventa() {
	}


	public final SimpleIntegerProperty idLineaVentaProperty() {
		return this.idLineaVenta;
	}

	public final int getIdLineaVenta() {
		return this.idLineaVentaProperty().get();
	}

	public final void setIdLineaVenta(final int idLineaVenta) {
		this.idLineaVentaProperty().set(idLineaVenta);
	}

	public final SimpleIntegerProperty cantidadProperty() {
		return this.cantidad;
	}

	public final int getCantidad() {
		return this.cantidadProperty().get();
	}

	public final void setCantidad(final int cantidad) {
		this.cantidadProperty().set(cantidad);
	}

	public final SimpleFloatProperty descuentoProperty() {
		return this.descuento;
	}

	public final float getDescuento() {
		return this.descuentoProperty().get();
	}

	public final void setDescuento(final float descuento) {
		this.descuentoProperty().set(descuento);
	}

	public final SimpleFloatProperty precioUnitarioProperty() {
		return this.precioUnitario;
	}

	public final float getPrecioUnitario() {
		return this.precioUnitarioProperty().get();
	}

	public final void setPrecioUnitario(final float precioUnitario) {
		this.precioUnitarioProperty().set(precioUnitario);
	}

	public final SimpleFloatProperty retencionesProperty() {
		return this.retenciones;
	}

	public final float getRetenciones() {
		return this.retencionesProperty().get();
	}

	public final void setRetenciones(final float retenciones) {
		this.retencionesProperty().set(retenciones);
	}

	public final SimpleFloatProperty subtotalProperty() {
		return this.subtotal;
	}

	public final float getSubtotal() {
		return this.subtotalProperty().get();
	}

	public final void setSubtotal(final float subtotal) {
		this.subtotalProperty().set(subtotal);
	}

	public final SimpleObjectProperty<Producto> productoProperty() {
		return this.producto;
	}

	public final gestion.gestionFinal.modelo.Producto getProducto() {
		return this.productoProperty().get();
	}

	public final void setProducto(
			final gestion.gestionFinal.modelo.Producto producto) {
		this.productoProperty().set(producto);
	}

	public final SimpleObjectProperty<Venta> ventaProperty() {
		return this.venta;
	}

	public final gestion.gestionFinal.modelo.Venta getVenta() {
		return this.ventaProperty().get();
	}

	public final void setVenta(final gestion.gestionFinal.modelo.Venta venta) {
		this.ventaProperty().set(venta);
	}
	
	private SimpleFloatProperty iva = new SimpleFloatProperty();

	public final SimpleFloatProperty ivaProperty() {
		return this.iva;
	}

	public final float getIva() {
		return this.ivaProperty().get();
	}

	public final void setIva(final float iva) {
		this.ivaProperty().set(iva);
	}
	

	
}