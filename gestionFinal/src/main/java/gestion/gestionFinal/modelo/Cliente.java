package gestion.gestionFinal.modelo;

import java.util.List;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * The persistent class for the cliente database table.
 * 
 */

public class Cliente {

	private SimpleIntegerProperty idCliente = new SimpleIntegerProperty();

	// bi-directional many-to-one association to Persona
	private SimpleObjectProperty<Persona> persona = new SimpleObjectProperty<Persona>();

	// bi-directional many-to-one association to Venta
	private List<Venta> ventas;

	public Cliente() {
	}

	public Cliente(int id, Persona persona) {
		this.idCliente.set(id);
		this.persona.set(persona);
	}

	public List<Venta> getVentas() {
		return this.ventas;
	}

	public void setVentas(List<Venta> ventas) {
		this.ventas = ventas;
	}

	public Venta addVenta(Venta venta) {
		getVentas().add(venta);
		venta.setCliente(this);

		return venta;
	}

	public Venta removeVenta(Venta venta) {
		getVentas().remove(venta);
		venta.setCliente(null);

		return venta;
	}

	public final SimpleIntegerProperty idClienteProperty() {
		return this.idCliente;
	}

	public final int getIdCliente() {
		return this.idClienteProperty().get();
	}

	public final void setIdCliente(final int idCliente) {
		this.idClienteProperty().set(idCliente);
	}

	public final SimpleObjectProperty<Persona> personaProperty() {
		return this.persona;
	}

	public final gestion.gestionFinal.modelo.Persona getPersona() {
		return this.personaProperty().get();
	}

	public final void setPersona(
			final gestion.gestionFinal.modelo.Persona persona) {
		this.personaProperty().set(persona);
	}

	@Override
	public String toString() {
		String cadena = "ID: " + getIdCliente() + " - " + getPersona().getNombre() + " - "
				+ getPersona().getCuilcuit() + " - "
				+ getPersona().getDireccionTexto();
		return cadena;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idCliente == null) ? 0 : idCliente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (idCliente == null) {
			if (other.idCliente != null)
				return false;
		} else if (
				//!idCliente.equals(other.idCliente)
				idCliente.get() != other.idCliente.get()
				//this.getPersona().getCuilcuit() == other.getPersona().getCuilcuit()
				)
			return false;
		return true;
	}
	
	

}