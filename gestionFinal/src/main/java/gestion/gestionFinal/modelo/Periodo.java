package gestion.gestionFinal.modelo;


import java.util.List;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * The persistent class for the periodo database table.
 * 
 */

public class Periodo {
	
	private SimpleIntegerProperty idPeriodo = new SimpleIntegerProperty();

	private SimpleStringProperty anio = new SimpleStringProperty();

	private SimpleStringProperty descripcion = new SimpleStringProperty();

	private SimpleStringProperty mes = new SimpleStringProperty();

	//bi-directional many-to-one association to Compra
	private List<Compra> compras;

	//bi-directional many-to-one association to Liquidacion
	private List<Liquidacion> liquidacions;

	//bi-directional many-to-one association to Venta
	private List<Venta> ventas;

	public Periodo() {
	}

	public Periodo(int idPeriodo, String anio, String descripcion, String mes){

		this.idPeriodo.set(idPeriodo);
		this.anio.set(anio);
		this.descripcion.set(descripcion);
		this.mes.set(mes);
	}

	public final SimpleIntegerProperty idPeriodoProperty() {
		return this.idPeriodo;
	}

	public final int getIdPeriodo() {
		return this.idPeriodoProperty().get();
	}

	public final void setIdPeriodo(final int idPeriodo) {
		this.idPeriodoProperty().set(idPeriodo);
	}

	public final SimpleStringProperty anioProperty() {
		return this.anio;
	}

	public final java.lang.String getAnio() {
		return this.anioProperty().get();
	}

	public final void setAnio(final java.lang.String anio) {
		this.anioProperty().set(anio);
	}

	public final SimpleStringProperty descripcionProperty() {
		return this.descripcion;
	}

	public final java.lang.String getDescripcion() {
		return this.descripcionProperty().get();
	}

	public final void setDescripcion(final java.lang.String descripcion) {
		this.descripcionProperty().set(descripcion);
	}

	public final SimpleStringProperty mesProperty() {
		return this.mes;
	}

	public final java.lang.String getMes() {
		return this.mesProperty().get();
	}

	public final void setMes(final java.lang.String mes) {
		this.mesProperty().set(mes);
	}

	
	

}