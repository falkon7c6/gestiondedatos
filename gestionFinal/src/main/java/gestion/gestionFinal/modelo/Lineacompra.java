package gestion.gestionFinal.modelo;

import java.io.Serializable;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;


/**
 * The persistent class for the lineacompra database table.
 * 
 */

public class Lineacompra{


	private SimpleIntegerProperty idLineaCompra = new SimpleIntegerProperty();

	private SimpleIntegerProperty cantidad = new SimpleIntegerProperty();

	private SimpleFloatProperty precio = new SimpleFloatProperty();

	private SimpleFloatProperty subtotal = new SimpleFloatProperty();

	//bi-directional many-to-one association to Compra
	private SimpleObjectProperty<Compra> compra = new SimpleObjectProperty<Compra>();

	//bi-directional many-to-one association to Producto
	private SimpleObjectProperty<Producto> producto = new SimpleObjectProperty<Producto>();

	public Lineacompra() {
	}
	
	public Lineacompra(int idLineaCompra, int cantidad, float precio, float subtotal
				, Compra compra, Producto producto){
		this.idLineaCompra.set(idLineaCompra);
		this.cantidad.set(cantidad);
		this.precio.set(precio);
		this.subtotal.set(subtotal);
		this.compra.set(compra);
		this.producto.set(producto);
	}

	public final SimpleIntegerProperty idLineaCompraProperty() {
		return this.idLineaCompra;
	}

	public final int getIdLineaCompra() {
		return this.idLineaCompraProperty().get();
	}

	public final void setIdLineaCompra(final int idLineaCompra) {
		this.idLineaCompraProperty().set(idLineaCompra);
	}

	public final SimpleIntegerProperty cantidadProperty() {
		return this.cantidad;
	}

	public final int getCantidad() {
		return this.cantidadProperty().get();
	}

	public final void setCantidad(final int cantidad) {
		this.cantidadProperty().set(cantidad);
	}

	public final SimpleFloatProperty precioProperty() {
		return this.precio;
	}

	public final float getPrecio() {
		return this.precioProperty().get();
	}

	public final void setPrecio(final float precio) {
		this.precioProperty().set(precio);
	}

	public final SimpleFloatProperty subtotalProperty() {
		return this.subtotal;
	}

	public final float getSubtotal() {
		return this.subtotalProperty().get();
	}

	public final void setSubtotal(final float subtotal) {
		this.subtotalProperty().set(subtotal);
	}

	public final SimpleObjectProperty<Compra> compraProperty() {
		return this.compra;
	}

	public final gestion.gestionFinal.modelo.Compra getCompra() {
		return this.compraProperty().get();
	}

	public final void setCompra(final gestion.gestionFinal.modelo.Compra compra) {
		this.compraProperty().set(compra);
	}

	public final SimpleObjectProperty<Producto> productoProperty() {
		return this.producto;
	}

	public final gestion.gestionFinal.modelo.Producto getProducto() {
		return this.productoProperty().get();
	}

	public final void setProducto(
			final gestion.gestionFinal.modelo.Producto producto) {
		this.productoProperty().set(producto);
	}
	
	private SimpleFloatProperty iva = new SimpleFloatProperty();

	public final SimpleFloatProperty ivaProperty() {
		return this.iva;
	}

	public final float getIva() {
		return this.ivaProperty().get();
	}

	public final void setIva(final float iva) {
		this.ivaProperty().set(iva);
	}
	
	
	
	

}