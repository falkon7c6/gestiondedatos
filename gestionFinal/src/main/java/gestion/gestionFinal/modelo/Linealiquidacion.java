package gestion.gestionFinal.modelo;


import java.math.BigDecimal;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * The persistent class for the linealiquidacion database table.
 * 
 */

public class Linealiquidacion{



	private SimpleIntegerProperty idDetalleLiquidacion = new SimpleIntegerProperty();

	private SimpleStringProperty descripcion = new SimpleStringProperty();

	private SimpleFloatProperty importe = new SimpleFloatProperty();

	//bi-directional many-to-one association to Conceptoliquidacion
	private SimpleObjectProperty<Conceptoliquidacion> conceptoliquidacion 
					= new SimpleObjectProperty<Conceptoliquidacion>();

	//bi-directional many-to-one association to Liquidacion
	private SimpleObjectProperty<Liquidacion> liquidacion 
					= new SimpleObjectProperty<Liquidacion>();

	public Linealiquidacion() {
	}

	public Linealiquidacion(int idDetalleLiquidacion, String descripcion,
			float importe, Conceptoliquidacion conceptoliquidacion,
			Liquidacion liquidacion) {
		
		this.idDetalleLiquidacion.set(idDetalleLiquidacion);
		this.descripcion.set(descripcion);
		this.importe.set(importe);
		this.conceptoliquidacion.set(conceptoliquidacion);
		this.liquidacion.set(liquidacion);
	}

	public final SimpleIntegerProperty idDetalleLiquidacionProperty() {
		return this.idDetalleLiquidacion;
	}

	public final int getIdDetalleLiquidacion() {
		return this.idDetalleLiquidacionProperty().get();
	}

	public final void setIdDetalleLiquidacion(final int idDetalleLiquidacion) {
		this.idDetalleLiquidacionProperty().set(idDetalleLiquidacion);
	}

	public final SimpleStringProperty descripcionProperty() {
		return this.descripcion;
	}

	public final java.lang.String getDescripcion() {
		return this.descripcionProperty().get();
	}

	public final void setDescripcion(final java.lang.String descripcion) {
		this.descripcionProperty().set(descripcion);
	}

	public final SimpleFloatProperty importeProperty() {
		return this.importe;
	}

	public final float getImporte() {
		return this.importeProperty().get();
	}

	public final void setImporte(final float importe) {
		this.importeProperty().set(importe);
	}

	public final SimpleObjectProperty<Conceptoliquidacion> conceptoliquidacionProperty() {
		return this.conceptoliquidacion;
	}

	public final gestion.gestionFinal.modelo.Conceptoliquidacion getConceptoliquidacion() {
		return this.conceptoliquidacionProperty().get();
	}

	public final void setConceptoliquidacion(
			final gestion.gestionFinal.modelo.Conceptoliquidacion conceptoliquidacion) {
		this.conceptoliquidacionProperty().set(conceptoliquidacion);
	}

	public final SimpleObjectProperty<Liquidacion> liquidacionProperty() {
		return this.liquidacion;
	}

	public final gestion.gestionFinal.modelo.Liquidacion getLiquidacion() {
		return this.liquidacionProperty().get();
	}

	public final void setLiquidacion(
			final gestion.gestionFinal.modelo.Liquidacion liquidacion) {
		this.liquidacionProperty().set(liquidacion);
	}
	
	

	
}