package gestion.gestionFinal.modelo;


import java.util.List;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * The persistent class for the categoriaempleado database table.
 * 
 */

public class Categoriaempleado {
	
	private SimpleIntegerProperty idCategoriaEmpleado = new SimpleIntegerProperty();

	private SimpleStringProperty descripcion = new SimpleStringProperty();

	private SimpleFloatProperty salarioHora = new SimpleFloatProperty();;

	//bi-directional many-to-one association to Empleado
	private List<Empleado> empleados;

	public Categoriaempleado() {
	}

	public Categoriaempleado(int idCategoriaEmpleado, String descripcion,
			Float salarioHora) {

		this.idCategoriaEmpleado.set(idCategoriaEmpleado);
		this.descripcion.set(descripcion);
		this.salarioHora.set(salarioHora);
	}
	
	

	@Override
	public String toString() {
		return descripcion.get();
	}

	public final SimpleIntegerProperty idCategoriaEmpleadoProperty() {
		return this.idCategoriaEmpleado;
	}

	public final int getIdCategoriaEmpleado() {
		return this.idCategoriaEmpleadoProperty().get();
	}

	public final void setIdCategoriaEmpleado(final int idCategoriaEmpleado) {
		this.idCategoriaEmpleadoProperty().set(idCategoriaEmpleado);
	}

	public final SimpleStringProperty descripcionProperty() {
		return this.descripcion;
	}

	public final java.lang.String getDescripcion() {
		return this.descripcionProperty().get();
	}

	public final void setDescripcion(final java.lang.String descripcion) {
		this.descripcionProperty().set(descripcion);
	}

	public final SimpleFloatProperty salarioHoraProperty() {
		return this.salarioHora;
	}

	public final Float getSalarioHora() {
		return this.salarioHoraProperty().get();
	}

	public final void setSalarioHora(final Float salarioHora) {
		this.salarioHoraProperty().set(salarioHora);
	}

	
}