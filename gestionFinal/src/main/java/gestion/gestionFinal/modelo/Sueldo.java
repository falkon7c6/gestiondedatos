package gestion.gestionFinal.modelo;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;




/**
 * The persistent class for the sueldo database table.
 * 
 */

public class Sueldo {


	private SimpleIntegerProperty idReciboSueldo = new SimpleIntegerProperty();

	private SimpleStringProperty apellidos = new SimpleStringProperty();

	private SimpleStringProperty calificacionProfesional = new SimpleStringProperty();

	private SimpleStringProperty categoriaEmpleado = new SimpleStringProperty();

	private SimpleStringProperty centroCostos = new SimpleStringProperty();

	private SimpleStringProperty deducciones = new SimpleStringProperty();

	private SimpleStringProperty legajo = new SimpleStringProperty();

	private SimpleStringProperty netoCobrar = new SimpleStringProperty();

	private SimpleStringProperty nombres = new SimpleStringProperty();

	private SimpleStringProperty obraSocial = new SimpleStringProperty();

	private SimpleStringProperty sueldo = new SimpleStringProperty();

	public Sueldo() {
	}

	public Sueldo(int idReciboSueldo, String apellidos,
			String calificacionProfesional, String categoriaEmpleado,
			String centroCostos, String deducciones, String legajo,
			String netoCobrar, String nombres, String obraSocial, String sueldo) {

		this.idReciboSueldo.set(idReciboSueldo);
		this.apellidos.set(apellidos);
		this.calificacionProfesional.set(calificacionProfesional);
		this.categoriaEmpleado.set(categoriaEmpleado);
		this.centroCostos.set(centroCostos);
		this.deducciones.set(deducciones);
		this.legajo.set(legajo);
		this.netoCobrar.set(netoCobrar);
		this.nombres.set(nombres);
		this.obraSocial.set(obraSocial);
		this.sueldo.set(sueldo);
	}

	public final SimpleIntegerProperty idReciboSueldoProperty() {
		return this.idReciboSueldo;
	}

	public final int getIdReciboSueldo() {
		return this.idReciboSueldoProperty().get();
	}

	public final void setIdReciboSueldo(final int idReciboSueldo) {
		this.idReciboSueldoProperty().set(idReciboSueldo);
	}

	public final SimpleStringProperty apellidosProperty() {
		return this.apellidos;
	}

	public final java.lang.String getApellidos() {
		return this.apellidosProperty().get();
	}

	public final void setApellidos(final java.lang.String apellidos) {
		this.apellidosProperty().set(apellidos);
	}

	public final SimpleStringProperty calificacionProfesionalProperty() {
		return this.calificacionProfesional;
	}

	public final java.lang.String getCalificacionProfesional() {
		return this.calificacionProfesionalProperty().get();
	}

	public final void setCalificacionProfesional(
			final java.lang.String calificacionProfesional) {
		this.calificacionProfesionalProperty().set(calificacionProfesional);
	}

	public final SimpleStringProperty categoriaEmpleadoProperty() {
		return this.categoriaEmpleado;
	}

	public final java.lang.String getCategoriaEmpleado() {
		return this.categoriaEmpleadoProperty().get();
	}

	public final void setCategoriaEmpleado(final java.lang.String categoriaEmpleado) {
		this.categoriaEmpleadoProperty().set(categoriaEmpleado);
	}

	public final SimpleStringProperty centroCostosProperty() {
		return this.centroCostos;
	}

	public final java.lang.String getCentroCostos() {
		return this.centroCostosProperty().get();
	}

	public final void setCentroCostos(final java.lang.String centroCostos) {
		this.centroCostosProperty().set(centroCostos);
	}

	public final SimpleStringProperty deduccionesProperty() {
		return this.deducciones;
	}

	public final java.lang.String getDeducciones() {
		return this.deduccionesProperty().get();
	}

	public final void setDeducciones(final java.lang.String deducciones) {
		this.deduccionesProperty().set(deducciones);
	}

	public final SimpleStringProperty legajoProperty() {
		return this.legajo;
	}

	public final java.lang.String getLegajo() {
		return this.legajoProperty().get();
	}

	public final void setLegajo(final java.lang.String legajo) {
		this.legajoProperty().set(legajo);
	}

	public final SimpleStringProperty netoCobrarProperty() {
		return this.netoCobrar;
	}

	public final java.lang.String getNetoCobrar() {
		return this.netoCobrarProperty().get();
	}

	public final void setNetoCobrar(final java.lang.String netoCobrar) {
		this.netoCobrarProperty().set(netoCobrar);
	}

	public final SimpleStringProperty nombresProperty() {
		return this.nombres;
	}

	public final java.lang.String getNombres() {
		return this.nombresProperty().get();
	}

	public final void setNombres(final java.lang.String nombres) {
		this.nombresProperty().set(nombres);
	}

	public final SimpleStringProperty obraSocialProperty() {
		return this.obraSocial;
	}

	public final java.lang.String getObraSocial() {
		return this.obraSocialProperty().get();
	}

	public final void setObraSocial(final java.lang.String obraSocial) {
		this.obraSocialProperty().set(obraSocial);
	}

	public final SimpleStringProperty sueldoProperty() {
		return this.sueldo;
	}

	public final java.lang.String getSueldo() {
		return this.sueldoProperty().get();
	}

	public final void setSueldo(final java.lang.String sueldo) {
		this.sueldoProperty().set(sueldo);
	}

	

}