package gestion.gestionFinal.modelo;

import java.time.LocalDate;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;


/**
 * The persistent class for the empleado database table.
 * 
 */

public class Empleado{


	private SimpleIntegerProperty legajo = new SimpleIntegerProperty();

	private SimpleIntegerProperty activo = new SimpleIntegerProperty();

	private SimpleObjectProperty<LocalDate> fechaIngreso = new SimpleObjectProperty<LocalDate>();

	private SimpleIntegerProperty horasSemanales = new SimpleIntegerProperty();


	private SimpleObjectProperty<Calificacionprofesional> calificacionprofesional 
							= new SimpleObjectProperty<Calificacionprofesional>();

	private SimpleObjectProperty<Categoriaempleado> categoriaempleado = new SimpleObjectProperty<Categoriaempleado>();

	private SimpleObjectProperty<Obrasocial> obrasocial = new SimpleObjectProperty<Obrasocial>();

	private SimpleObjectProperty<Personafisica> personafisica = new SimpleObjectProperty<Personafisica>();

	private SimpleListProperty<Personafisica> hijos = new SimpleListProperty<Personafisica>();

	private SimpleListProperty<Personafisica> familia = new SimpleListProperty<Personafisica>();

	private SimpleObjectProperty<Personafisica> conyuge = new SimpleObjectProperty<Personafisica>();

	public Empleado() {
	}

	public Empleado(int legajo, byte activo, LocalDate fechaIngreso,
			int horasSemanales, Calificacionprofesional calificacionprofesional,
			Categoriaempleado categoriaempleado, Obrasocial obrasocial,
			Personafisica personafisica) {
		
		this.legajo.set(legajo);
		this.activo.set(activo);
		this.fechaIngreso.set(fechaIngreso);
		this.horasSemanales.set(horasSemanales);
		this.calificacionprofesional.set(calificacionprofesional);
		this.categoriaempleado.set(categoriaempleado);
		this.obrasocial.set(obrasocial);
		this.personafisica.set(personafisica);
	}

	public final SimpleIntegerProperty legajoProperty() {
		return this.legajo;
	}

	@Override
	public String toString() {
		return "" + legajo.get() + " " + this.getPersonafisica().getApellido() + " " + this.getPersonafisica().getPersona().getNombre() + " " + this.getPersonafisica().getIdPersonaFisica();
	}

	public final int getLegajo() {
		return this.legajoProperty().get();
	}

	public final void setLegajo(final int legajo) {
		this.legajoProperty().set(legajo);
	}

	public final SimpleIntegerProperty activoProperty() {
		return this.activo;
	}

	public final int getActivo() {
		return this.activoProperty().get();
	}

	public final void setActivo(final int activo) {
		this.activoProperty().set(activo);
	}

	public final SimpleObjectProperty<LocalDate> fechaIngresoProperty() {
		return this.fechaIngreso;
	}

	public final java.time.LocalDate getFechaIngreso() {
		return this.fechaIngresoProperty().get();
	}

	public final void setFechaIngreso(final java.time.LocalDate fechaIngreso) {
		this.fechaIngresoProperty().set(fechaIngreso);
	}

	public final SimpleIntegerProperty horasSemanalesProperty() {
		return this.horasSemanales;
	}

	public final int getHorasSemanales() {
		return this.horasSemanalesProperty().get();
	}

	public final void setHorasSemanales(final int horasSemanales) {
		this.horasSemanalesProperty().set(horasSemanales);
	}

	public final SimpleObjectProperty<Calificacionprofesional> calificacionprofesionalProperty() {
		return this.calificacionprofesional;
	}

	public final gestion.gestionFinal.modelo.Calificacionprofesional getCalificacionprofesional() {
		return this.calificacionprofesionalProperty().get();
	}

	public final void setCalificacionprofesional(
			final gestion.gestionFinal.modelo.Calificacionprofesional calificacionprofesional) {
		this.calificacionprofesionalProperty().set(calificacionprofesional);
	}

	public final SimpleObjectProperty<Categoriaempleado> categoriaempleadoProperty() {
		return this.categoriaempleado;
	}

	public final gestion.gestionFinal.modelo.Categoriaempleado getCategoriaempleado() {
		return this.categoriaempleadoProperty().get();
	}

	public final void setCategoriaempleado(
			final gestion.gestionFinal.modelo.Categoriaempleado categoriaempleado) {
		this.categoriaempleadoProperty().set(categoriaempleado);
	}

	public final SimpleObjectProperty<Obrasocial> obrasocialProperty() {
		return this.obrasocial;
	}

	public final gestion.gestionFinal.modelo.Obrasocial getObrasocial() {
		return this.obrasocialProperty().get();
	}

	public final void setObrasocial(
			final gestion.gestionFinal.modelo.Obrasocial obrasocial) {
		this.obrasocialProperty().set(obrasocial);
	}

	public final SimpleObjectProperty<Personafisica> personafisicaProperty() {
		return this.personafisica;
	}

	public final gestion.gestionFinal.modelo.Personafisica getPersonafisica() {
		return this.personafisicaProperty().get();
	}

	public final void setPersonafisica(
			final gestion.gestionFinal.modelo.Personafisica personafisica) {
		this.personafisicaProperty().set(personafisica);
	}

	public final SimpleListProperty<Personafisica> hijosProperty() {
		return this.hijos;
	}

	public final javafx.collections.ObservableList<gestion.gestionFinal.modelo.Personafisica> getHijos() {
		return this.hijosProperty().get();
	}

	public final void setHijos(
			final javafx.collections.ObservableList<gestion.gestionFinal.modelo.Personafisica> hijos) {
		this.hijosProperty().set(hijos);
	}

	public final SimpleListProperty<Personafisica> familiaProperty() {
		return this.familia;
	}

	public final javafx.collections.ObservableList<gestion.gestionFinal.modelo.Personafisica> getFamilia() {
		return this.familiaProperty().get();
	}

	public final void setFamilia(
			final javafx.collections.ObservableList<gestion.gestionFinal.modelo.Personafisica> familia) {
		this.familiaProperty().set(familia);
	}

	public final SimpleObjectProperty<Personafisica> conyugeProperty() {
		return this.conyuge;
	}

	public final gestion.gestionFinal.modelo.Personafisica getConyuge() {
		return this.conyugeProperty().get();
	}

	public final void setConyuge(
			final gestion.gestionFinal.modelo.Personafisica conyuge) {
		this.conyugeProperty().set(conyuge);
	}
	
	
	

}