package gestion.gestionFinal.modelo;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;




/**
 * The persistent class for the parametro database table.
 * 
 */

public class Parametro{
	
	private SimpleIntegerProperty idIndice = new SimpleIntegerProperty();

	private SimpleStringProperty descripcion = new SimpleStringProperty();

	private SimpleStringProperty nombre = new SimpleStringProperty();

	private SimpleStringProperty tipo = new SimpleStringProperty();

	private SimpleStringProperty valor = new SimpleStringProperty();

	public Parametro() {
	}

	public Parametro(int idIndice, String descripcion, String nombre,
			String tipo, String valor) {
		super();
		this.idIndice.set(idIndice);
		this.descripcion.set(descripcion);
		this.nombre.set(nombre);
		this.tipo.set(tipo);
		this.valor.set(valor);
	}

	public final SimpleIntegerProperty idIndiceProperty() {
		return this.idIndice;
	}

	public final int getIdIndice() {
		return this.idIndiceProperty().get();
	}

	public final void setIdIndice(final int idIndice) {
		this.idIndiceProperty().set(idIndice);
	}

	public final SimpleStringProperty descripcionProperty() {
		return this.descripcion;
	}

	public final java.lang.String getDescripcion() {
		return this.descripcionProperty().get();
	}

	public final void setDescripcion(final java.lang.String descripcion) {
		this.descripcionProperty().set(descripcion);
	}

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final java.lang.String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final java.lang.String nombre) {
		this.nombreProperty().set(nombre);
	}

	public final SimpleStringProperty tipoProperty() {
		return this.tipo;
	}

	public final java.lang.String getTipo() {
		return this.tipoProperty().get();
	}

	public final void setTipo(final java.lang.String tipo) {
		this.tipoProperty().set(tipo);
	}

	public final SimpleStringProperty valorProperty() {
		return this.valor;
	}

	public final java.lang.String getValor() {
		return this.valorProperty().get();
	}

	public final void setValor(final java.lang.String valor) {
		this.valorProperty().set(valor);
	}

	

}