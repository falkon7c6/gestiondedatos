package gestion.gestionFinal.modelo;

import java.time.LocalDate;
import java.util.List;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 * The persistent class for the compra database table.
 * 
 */

public class Compra{

	private SimpleIntegerProperty idCompra = new SimpleIntegerProperty();

	

	private SimpleStringProperty condicionIVA = new SimpleStringProperty();



	private SimpleObjectProperty<LocalDate> fecha = new SimpleObjectProperty();

	private SimpleStringProperty numeroComprobante = new SimpleStringProperty();

	private SimpleStringProperty observaciones = new SimpleStringProperty();

	private SimpleStringProperty tipoFactura = new SimpleStringProperty();

	private SimpleFloatProperty total = new SimpleFloatProperty();

	//bi-directional many-to-one association to Empleado
	private SimpleObjectProperty<Empleado> empleado = new SimpleObjectProperty();

	//bi-directional many-to-one association to Periodo
	private SimpleObjectProperty<Periodo> periodo = new SimpleObjectProperty();

	//bi-directional many-to-one association to Proveedor
	private SimpleObjectProperty<Proveedor> proveedor = new SimpleObjectProperty();

	//bi-directional many-to-one association to Lineacompra
	private ObservableList<Lineacompra> lineacompras = FXCollections.observableArrayList();

	public Compra() {
	}

	public Compra(int id, String bonificacion, String condicionIVA
			,String condicionVenta, String numeroComprobante, String observaciones
			,String tipoFactura, Float total){
		this.idCompra.set(id);
		
		this.condicionIVA.set(condicionIVA);
		
		
		this.numeroComprobante.set(numeroComprobante);
		this.observaciones.set(observaciones);
		this.tipoFactura.set(tipoFactura);
		this.total.set(total);
		
	}

	public final SimpleIntegerProperty idCompraProperty() {
		return this.idCompra;
	}

	public final int getIdCompra() {
		return this.idCompraProperty().get();
	}

	public final void setIdCompra(final int idCompra) {
		this.idCompraProperty().set(idCompra);
	}


	public final SimpleStringProperty condicionIVAProperty() {
		return this.condicionIVA;
	}

	public final java.lang.String getCondicionIVA() {
		return this.condicionIVAProperty().get();
	}

	public final void setCondicionIVA(final java.lang.String condicionIVA) {
		this.condicionIVAProperty().set(condicionIVA);
	}

	public final SimpleStringProperty numeroComprobanteProperty() {
		return this.numeroComprobante;
	}

	public final java.lang.String getNumeroComprobante() {
		return this.numeroComprobanteProperty().get();
	}

	public final void setNumeroComprobante(final java.lang.String numeroComprobante) {
		this.numeroComprobanteProperty().set(numeroComprobante);
	}

	public final SimpleStringProperty observacionesProperty() {
		return this.observaciones;
	}

	public final java.lang.String getObservaciones() {
		return this.observacionesProperty().get();
	}

	public final void setObservaciones(final java.lang.String observaciones) {
		this.observacionesProperty().set(observaciones);
	}

	public final SimpleStringProperty tipoFacturaProperty() {
		return this.tipoFactura;
	}

	public final java.lang.String getTipoFactura() {
		return this.tipoFacturaProperty().get();
	}

	public final void setTipoFactura(final java.lang.String tipoFactura) {
		this.tipoFacturaProperty().set(tipoFactura);
	}

	public final SimpleFloatProperty totalProperty() {
		return this.total;
	}

	public final float getTotal() {
		return this.totalProperty().get();
	}

	public final void setTotal(final float total) {
		this.totalProperty().set(total);
	}

	public final SimpleObjectProperty<LocalDate> fechaProperty() {
		return this.fecha;
	}

	public final java.time.LocalDate getFecha() {
		return this.fechaProperty().get();
	}

	public final void setFecha(final java.time.LocalDate fecha) {
		this.fechaProperty().set(fecha);
	}

	public final SimpleObjectProperty<Empleado> empleadoProperty() {
		return this.empleado;
	}

	public final gestion.gestionFinal.modelo.Empleado getEmpleado() {
		return this.empleadoProperty().get();
	}

	public final void setEmpleado(
			final gestion.gestionFinal.modelo.Empleado empleado) {
		this.empleadoProperty().set(empleado);
	}

	public final SimpleObjectProperty<Periodo> periodoProperty() {
		return this.periodo;
	}

	public final gestion.gestionFinal.modelo.Periodo getPeriodo() {
		return this.periodoProperty().get();
	}

	public final void setPeriodo(final gestion.gestionFinal.modelo.Periodo periodo) {
		this.periodoProperty().set(periodo);
	}

	public final SimpleObjectProperty<Proveedor> proveedorProperty() {
		return this.proveedor;
	}

	public final gestion.gestionFinal.modelo.Proveedor getProveedor() {
		return this.proveedorProperty().get();
	}

	public final void setProveedor(
			final gestion.gestionFinal.modelo.Proveedor proveedor) {
		this.proveedorProperty().set(proveedor);
	}

	
	

}