package gestion.gestionFinal.modelo;


import java.util.List;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 * The persistent class for the producto database table.
 * 
 */

public class Producto{
	

	
	private SimpleIntegerProperty idProducto = new SimpleIntegerProperty();

	private SimpleStringProperty descripcion = new SimpleStringProperty();

	private SimpleStringProperty deshabilitado = new SimpleStringProperty();

	private SimpleStringProperty nombre = new SimpleStringProperty();

	//private SimpleFloatProperty precioCompra = new SimpleFloatProperty();
	
	private SimpleIntegerProperty stock = new SimpleIntegerProperty();

	private SimpleFloatProperty precioVenta = new SimpleFloatProperty();
	
	private ObservableList<Catalogo> listaCatalogo = FXCollections.observableArrayList();
	

	//bi-directional many-to-one association to Categoriaproducto
	private SimpleObjectProperty<Categoriaproducto> categoriaproducto 
		= new SimpleObjectProperty<Categoriaproducto>();

	//bi-directional many-to-one association to Marcaproducto
	private SimpleObjectProperty<Marcaproducto> marcaproducto 
		= new SimpleObjectProperty<Marcaproducto>();

	public Producto() {
	}

	public Producto(int idProducto, String descripcion, String deshabilitado,
			String nombre, int stock, Float precioVenta,
			Categoriaproducto categoriaproducto, Marcaproducto marcaproducto) {

		this.idProducto.set(idProducto);
		this.descripcion.set(descripcion);
		this.deshabilitado.set(deshabilitado);
		this.nombre.set(nombre);
		//this.precioCompra.set(precioCompra);
		this.precioVenta.set(precioVenta);
		this.categoriaproducto.set(categoriaproducto);
		this.marcaproducto.set(marcaproducto);
		this.stock.set(stock);
	}
	
	

	@Override
	public String toString() {
		String valor = "" + this.getNombre() +" "+ this.getDescripcion() + " " + this.marcaproducto.get().getNombre();
		return valor;
	}

	public final SimpleIntegerProperty idProductoProperty() {
		return this.idProducto;
	}

	public final int getIdProducto() {
		return this.idProductoProperty().get();
	}

	public final void setIdProducto(final int idProducto) {
		this.idProductoProperty().set(idProducto);
	}

	public final SimpleStringProperty descripcionProperty() {
		return this.descripcion;
	}

	public final java.lang.String getDescripcion() {
		return this.descripcionProperty().get();
	}

	public final void setDescripcion(final java.lang.String descripcion) {
		this.descripcionProperty().set(descripcion);
	}

	public final SimpleStringProperty deshabilitadoProperty() {
		return this.deshabilitado;
	}

	public final String getDeshabilitado() {
		return this.deshabilitadoProperty().get();
	}

	public final void setDeshabilitado(final String deshabilitado) {
		this.deshabilitadoProperty().set(deshabilitado);
	}

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final java.lang.String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final java.lang.String nombre) {
		this.nombreProperty().set(nombre);
	}


	public final SimpleFloatProperty precioVentaProperty() {
		return this.precioVenta;
	}

	public final float getPrecioVenta() {
		return this.precioVentaProperty().get();
	}

	public final void setPrecioVenta(final float precioVenta) {
		this.precioVentaProperty().set(precioVenta);
	}

	public final SimpleObjectProperty<Categoriaproducto> categoriaproductoProperty() {
		return this.categoriaproducto;
	}

	public final gestion.gestionFinal.modelo.Categoriaproducto getCategoriaproducto() {
		return this.categoriaproductoProperty().get();
	}

	public final void setCategoriaproducto(
			final gestion.gestionFinal.modelo.Categoriaproducto categoriaproducto) {
		this.categoriaproductoProperty().set(categoriaproducto);
	}

	public final SimpleObjectProperty<Marcaproducto> marcaproductoProperty() {
		return this.marcaproducto;
	}

	public final gestion.gestionFinal.modelo.Marcaproducto getMarcaproducto() {
		return this.marcaproductoProperty().get();
	}

	public final void setMarcaproducto(
			final gestion.gestionFinal.modelo.Marcaproducto marcaproducto) {
		this.marcaproductoProperty().set(marcaproducto);
	}

	public final SimpleIntegerProperty stockProperty() {
		return this.stock;
	}

	public final int getStock() {
		return this.stockProperty().get();
	}

	public final void setStock(final int stock) {
		this.stockProperty().set(stock);
	}

	public ObservableList<Catalogo> getListaCatalogo() {
		return listaCatalogo;
	}

	public void setListaCatalogo(ObservableList<Catalogo> listaCatalogo) {
		this.listaCatalogo = listaCatalogo;
	}
	
	
	

	

}