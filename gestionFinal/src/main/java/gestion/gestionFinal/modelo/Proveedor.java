package gestion.gestionFinal.modelo;

import java.util.List;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 * The persistent class for the proveedor database table.
 * 
 */

public class Proveedor{
	


	private SimpleIntegerProperty idProveedor = new SimpleIntegerProperty();

	//bi-directional many-to-one association to Compra
	private List<Compra> compras;

	//bi-directional many-to-one association to Persona
	private SimpleObjectProperty<Persona> persona = new SimpleObjectProperty<Persona>();
	
	private ObservableList<Catalogo> listaCatalogo = FXCollections.observableArrayList();

	public Proveedor() {
	}

	public Proveedor(int idProveedor, List<Compra> compras, Persona persona) {
		this.idProveedor.set(idProveedor);
		this.compras = compras;
		this.persona.set(persona);
	}
	
	

	@Override
	public String toString() {
		String valor = "" + this.getIdProveedor() + " " + this.getPersona().getNombre() + " " + this.getPersona().getCuilcuit(); 
		return valor;
	}

	public final SimpleIntegerProperty idProveedorProperty() {
		return this.idProveedor;
	}

	public final int getIdProveedor() {
		return this.idProveedorProperty().get();
	}

	public final void setIdProveedor(final int idProveedor) {
		this.idProveedorProperty().set(idProveedor);
	}

	public final SimpleObjectProperty<Persona> personaProperty() {
		return this.persona;
	}

	public final gestion.gestionFinal.modelo.Persona getPersona() {
		return this.personaProperty().get();
	}

	public final void setPersona(final gestion.gestionFinal.modelo.Persona persona) {
		this.personaProperty().set(persona);
	}

	

}