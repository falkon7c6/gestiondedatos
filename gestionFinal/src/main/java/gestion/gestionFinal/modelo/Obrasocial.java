package gestion.gestionFinal.modelo;



import java.math.BigDecimal;
import java.util.List;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * The persistent class for the obrasocial database table.
 * 
 */


public class Obrasocial{

	private SimpleIntegerProperty idObraSocial = new SimpleIntegerProperty();

	private SimpleFloatProperty importeFijo = new SimpleFloatProperty();

	private SimpleFloatProperty importePorcentual = new SimpleFloatProperty();

	private SimpleStringProperty nombre = new SimpleStringProperty();

	//bi-directional many-to-one association to Empleado

	private List<Empleado> empleados;



	public Obrasocial(int idObraSocial, float importeFijo,
			float importePorcentual, String nombre) {
	
		this.idObraSocial.set(idObraSocial);
		this.importeFijo.set(importeFijo);
		this.importePorcentual.set(importePorcentual);
		this.nombre.set(nombre);
	}
	
	public Obrasocial(){
		
	}
	

	@Override
	public String toString() {
		return nombre.get();
	}



	public final SimpleIntegerProperty idObraSocialProperty() {
		return this.idObraSocial;
	}

	public final int getIdObraSocial() {
		return this.idObraSocialProperty().get();
	}

	public final void setIdObraSocial(final int idObraSocial) {
		this.idObraSocialProperty().set(idObraSocial);
	}

	public final SimpleFloatProperty importeFijoProperty() {
		return this.importeFijo;
	}

	public final float getImporteFijo() {
		return this.importeFijoProperty().get();
	}

	public final void setImporteFijo(final float importeFijo) {
		this.importeFijoProperty().set(importeFijo);
	}

	public final SimpleFloatProperty importePorcentualProperty() {
		return this.importePorcentual;
	}

	public final float getImportePorcentual() {
		return this.importePorcentualProperty().get();
	}

	public final void setImportePorcentual(final float importePorcentual) {
		this.importePorcentualProperty().set(importePorcentual);
	}

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final java.lang.String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final java.lang.String nombre) {
		this.nombreProperty().set(nombre);
	}

	


}