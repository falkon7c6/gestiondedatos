package gestion.gestionFinal.modelo;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Catalogo {
	
	private SimpleObjectProperty<Producto> producto 
	= new SimpleObjectProperty<Producto>();
	
	private SimpleObjectProperty<Proveedor> proveedor 
	= new SimpleObjectProperty<Proveedor>();
	
	private SimpleFloatProperty precio = new SimpleFloatProperty();
	
	public Catalogo(){
		
	}
	
	public Catalogo(Producto prod, Proveedor prov, float stock){
		this.producto.set(prod);
		this.proveedor.set(prov);
		this.precio.set(stock);
	}


	public final SimpleObjectProperty<Producto> productoProperty() {
		return this.producto;
	}

	public final gestion.gestionFinal.modelo.Producto getProducto() {
		return this.productoProperty().get();
	}

	public final void setProducto(
			final gestion.gestionFinal.modelo.Producto producto) {
		this.productoProperty().set(producto);
	}

	public final SimpleObjectProperty<Proveedor> proveedorProperty() {
		return this.proveedor;
	}

	public final gestion.gestionFinal.modelo.Proveedor getProveedor() {
		return this.proveedorProperty().get();
	}

	public final void setProveedor(
			final gestion.gestionFinal.modelo.Proveedor proveedor) {
		this.proveedorProperty().set(proveedor);
	}

	public final SimpleFloatProperty precioProperty() {
		return this.precio;
	}

	public final float getPrecio() {
		return this.precioProperty().get();
	}

	public final void setPrecio(final float precio) {
		this.precioProperty().set(precio);
	}



	
	
	
	
}
