package gestion.gestionFinal.modelo;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * The persistent class for the calificacionprofesional database table.
 * 
 */

public class Calificacionprofesional{
	
	private SimpleIntegerProperty idCalificacionProfesional = new SimpleIntegerProperty();

	private SimpleStringProperty descripcion = new SimpleStringProperty();

	private SimpleFloatProperty importeFijo = new SimpleFloatProperty();

	private SimpleFloatProperty salarioHora = new SimpleFloatProperty();

	public Calificacionprofesional() {
	}

	public Calificacionprofesional(int idCalificacionProfesional,
			String descripcion, Float importeFijo, Float salarioHora) {

		this.idCalificacionProfesional.set(idCalificacionProfesional);
		this.descripcion.set(descripcion);
		this.importeFijo.set(idCalificacionProfesional);
		this.salarioHora.set(idCalificacionProfesional);
	}
	
	public Calificacionprofesional(String descripcion, Float importeFijo, Float salarioHora) {

		this.descripcion.set(descripcion);
		this.importeFijo.set(importeFijo);
		this.salarioHora.set(salarioHora);
	}

	
	
	@Override
	public String toString() {
		return descripcion.get();
	}

	public final SimpleIntegerProperty idCalificacionProfesionalProperty() {
		return this.idCalificacionProfesional;
	}

	public final int getIdCalificacionProfesional() {
		return this.idCalificacionProfesionalProperty().get();
	}

	public final void setIdCalificacionProfesional(
			final int idCalificacionProfesional) {
		this.idCalificacionProfesionalProperty().set(idCalificacionProfesional);
	}

	public final SimpleStringProperty descripcionProperty() {
		return this.descripcion;
	}

	public final java.lang.String getDescripcion() {
		return this.descripcionProperty().get();
	}

	public final void setDescripcion(final java.lang.String descripcion) {
		this.descripcionProperty().set(descripcion);
	}

	public final SimpleFloatProperty importeFijoProperty() {
		return this.importeFijo;
	}

	public final float getImporteFijo() {
		return this.importeFijoProperty().get();
	}

	public final void setImporteFijo(final float importeFijo) {
		this.importeFijoProperty().set(importeFijo);
	}

	public final SimpleFloatProperty salarioHoraProperty() {
		return this.salarioHora;
	}

	public final float getSalarioHora() {
		return this.salarioHoraProperty().get();
	}

	public final void setSalarioHora(final float salarioHora) {
		this.salarioHoraProperty().set(salarioHora);
	}

	

}