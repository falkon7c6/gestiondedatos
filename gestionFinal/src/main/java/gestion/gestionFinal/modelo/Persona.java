package gestion.gestionFinal.modelo;


import java.util.List;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * The persistent class for the persona database table.
 * 
 */

public class Persona{
	
	private SimpleStringProperty cuilcuit = new SimpleStringProperty();

	private SimpleStringProperty direccionTexto = new SimpleStringProperty();

	private SimpleStringProperty nombre = new SimpleStringProperty();

	private SimpleStringProperty telefono = new SimpleStringProperty();

	private SimpleStringProperty tipo = new SimpleStringProperty();

	//bi-directional many-to-one association to Cliente
	private List<Cliente> clientes;

	//bi-directional many-to-one association to Direccion
	private SimpleObjectProperty<Direccion> direccion = new SimpleObjectProperty<Direccion>();

	//bi-directional many-to-one association to Personafisica
	private List<Personafisica> personafisicas;

	//bi-directional many-to-one association to Proveedor
	private List<Proveedor> proveedors;

	//bi-directional many-to-one association to Usuario
	private List<Usuario> usuarios;

	public Persona() {
	}

	public Persona(String cuilcuit, String direccionTexto, String nombre,
			String telefono, String tipo,
			Direccion direccion) {

		this.cuilcuit.set(cuilcuit);
		this.direccionTexto.set(direccionTexto);
		this.nombre.set(nombre);
		this.telefono.set(telefono);
		this.tipo.set(tipo);
		this.direccion.set(direccion);
	}

	
	
	@Override
	public String toString() {
		return "Persona [cuilcuit=" + cuilcuit + ", direccionTexto="
				+ direccionTexto + ", nombre=" + nombre + ", telefono="
				+ telefono + ", tipo=" + tipo + "]";
	}

	public final SimpleStringProperty cuilcuitProperty() {
		return this.cuilcuit;
	}

	public final String getCuilcuit() {
		return this.cuilcuitProperty().get();
	}

	public final void setCuilcuit(final String cuilcuit) {
		this.cuilcuitProperty().set(cuilcuit);
	}

	public final SimpleStringProperty direccionTextoProperty() {
		return this.direccionTexto;
	}

	public final java.lang.String getDireccionTexto() {
		return this.direccionTextoProperty().get();
	}

	public final void setDireccionTexto(final java.lang.String direccionTexto) {
		this.direccionTextoProperty().set(direccionTexto);
	}

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final java.lang.String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final java.lang.String nombre) {
		this.nombreProperty().set(nombre);
	}

	public final SimpleStringProperty telefonoProperty() {
		return this.telefono;
	}

	public final java.lang.String getTelefono() {
		return this.telefonoProperty().get();
	}

	public final void setTelefono(final java.lang.String telefono) {
		this.telefonoProperty().set(telefono);
	}

	public final SimpleStringProperty tipoProperty() {
		return this.tipo;
	}

	public final java.lang.String getTipo() {
		return this.tipoProperty().get();
	}

	public final void setTipo(final java.lang.String tipo) {
		this.tipoProperty().set(tipo);
	}

	public final SimpleObjectProperty<Direccion> direccionProperty() {
		return this.direccion;
	}

	public final gestion.gestionFinal.modelo.Direccion getDireccion() {
		return this.direccionProperty().get();
	}

	public final void setDireccion(
			final gestion.gestionFinal.modelo.Direccion direccion) {
		this.direccionProperty().set(direccion);
	}
	
	

	

}