package gestion.gestionFinal.modelo;



import java.util.List;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * The persistent class for the conceptoliquidacion database table.
 * 
 */

public class Conceptoliquidacion{

	private SimpleIntegerProperty idConceptoLiquidacion = new SimpleIntegerProperty();

	private SimpleStringProperty descripcion = new SimpleStringProperty();

	private SimpleIntegerProperty deshabilitado = new SimpleIntegerProperty();

	private SimpleFloatProperty monto = new SimpleFloatProperty();

	private SimpleStringProperty nombre = new SimpleStringProperty();

	private SimpleStringProperty tipo = new SimpleStringProperty();

	//bi-directional many-to-one association to Linealiquidacion
	private List<Linealiquidacion> linealiquidacions;

	public Conceptoliquidacion() {
	}
	
	public Conceptoliquidacion(int id, String descripcion, int deshabilitado, float monto, String nombre,
			String tipo){
		this.idConceptoLiquidacion.set(id);
		this.descripcion.set(descripcion);
		this.deshabilitado.set(deshabilitado);
		this.monto.set(monto);
		this.nombre.set(nombre);
		this.tipo.set(tipo);
		
	}

	public final SimpleIntegerProperty idConceptoLiquidacionProperty() {
		return this.idConceptoLiquidacion;
	}

	public final int getIdConceptoLiquidacion() {
		return this.idConceptoLiquidacionProperty().get();
	}

	public final void setIdConceptoLiquidacion(final int idConceptoLiquidacion) {
		this.idConceptoLiquidacionProperty().set(idConceptoLiquidacion);
	}

	public final SimpleStringProperty descripcionProperty() {
		return this.descripcion;
	}

	public final java.lang.String getDescripcion() {
		return this.descripcionProperty().get();
	}

	public final void setDescripcion(final java.lang.String descripcion) {
		this.descripcionProperty().set(descripcion);
	}

	public final SimpleIntegerProperty deshabilitadoProperty() {
		return this.deshabilitado;
	}

	public final int getDeshabilitado() {
		return this.deshabilitadoProperty().get();
	}

	public final void setDeshabilitado(final int deshabilitado) {
		this.deshabilitadoProperty().set(deshabilitado);
	}

	public final SimpleFloatProperty montoProperty() {
		return this.monto;
	}

	public final float getMonto() {
		return this.montoProperty().get();
	}

	public final void setMonto(final float monto) {
		this.montoProperty().set(monto);
	}

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final java.lang.String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final java.lang.String nombre) {
		this.nombreProperty().set(nombre);
	}

	public final SimpleStringProperty tipoProperty() {
		return this.tipo;
	}

	public final java.lang.String getTipo() {
		return this.tipoProperty().get();
	}

	public final void setTipo(final java.lang.String tipo) {
		this.tipoProperty().set(tipo);
	}

	
}