package gestion.gestionFinal.modelo;

import java.io.Serializable;
import java.util.List;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * The persistent class for the categoriaproducto database table.
 * 
 */

public class Categoriaproducto{

	private SimpleIntegerProperty idCategoriaProducto = new SimpleIntegerProperty();

	private SimpleStringProperty nombre = new SimpleStringProperty();

	//bi-directional many-to-one association to Producto
	private List<Producto> productos;

	public Categoriaproducto(int idCategoriaProducto, String nombre) {
		this.idCategoriaProducto.set(idCategoriaProducto);
		this.nombre.set(nombre);
	}

	public Categoriaproducto(){
		
	}
	
	@Override
	public String toString() {
		return this.getNombre();
	}



	public final SimpleIntegerProperty idCategoriaProductoProperty() {
		return this.idCategoriaProducto;
	}

	public final int getIdCategoriaProducto() {
		return this.idCategoriaProductoProperty().get();
	}

	public final void setIdCategoriaProducto(final int idCategoriaProducto) {
		this.idCategoriaProductoProperty().set(idCategoriaProducto);
	}

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final java.lang.String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final java.lang.String nombre) {
		this.nombreProperty().set(nombre);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idCategoriaProducto == null) ? 0 : idCategoriaProducto
						.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categoriaproducto other = (Categoriaproducto) obj;
		if (idCategoriaProducto == null) {
			if (other.idCategoriaProducto != null)
				return false;
		} else if (
				//!idCategoriaProducto.equals(other.idCategoriaProducto)
				this.getIdCategoriaProducto() != other.getIdCategoriaProducto()
				)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (
				//!nombre.equals(other.nombre)
				!this.getNombre().equals(other.getNombre())
				)
			return false;
		return true;
	}

	

}