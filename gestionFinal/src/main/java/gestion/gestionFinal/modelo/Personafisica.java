package gestion.gestionFinal.modelo;


import java.util.List;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;


/**
 * The persistent class for the personafisica database table.
 * 
 */

public class Personafisica{



	private SimpleIntegerProperty idPersonaFisica = new SimpleIntegerProperty();

	private SimpleStringProperty apellido = new SimpleStringProperty();

	private SimpleStringProperty nombrePila = new SimpleStringProperty();

	private SimpleStringProperty sexo = new SimpleStringProperty();

	//bi-directional many-to-one association to Persona
	private SimpleObjectProperty<Persona> persona = new SimpleObjectProperty<Persona>();

	//bi-directional many-to-one association to Personafisica
	private SimpleObjectProperty<Personafisica> padre = new SimpleObjectProperty<Personafisica>();

	//bi-directional many-to-one association to Personafisica
	private SimpleObjectProperty<Personafisica> conyuge = new SimpleObjectProperty<Personafisica>();



	public Personafisica() {
	}

	public Personafisica(int idPersonaFisica, String apellido,
			String nombrePila, String sexo, Persona persona, Personafisica padre, Personafisica conyuge) {

		this.idPersonaFisica.set(idPersonaFisica);
		this.apellido.set(apellido);
		this.nombrePila.set(nombrePila);
		this.sexo.set(sexo);
		this.persona.set(persona);
		this.padre.set(padre);
		this.conyuge.set(conyuge);
	}
	
	

	@Override
	public String toString() {
		return persona.get().getNombre() + " " + apellido.get();
	}

	public final SimpleIntegerProperty idPersonaFisicaProperty() {
		return this.idPersonaFisica;
	}

	public final int getIdPersonaFisica() {
		return this.idPersonaFisicaProperty().get();
	}

	public final void setIdPersonaFisica(final int idPersonaFisica) {
		this.idPersonaFisicaProperty().set(idPersonaFisica);
	}

	public final SimpleStringProperty apellidoProperty() {
		return this.apellido;
	}

	public final java.lang.String getApellido() {
		return this.apellidoProperty().get();
	}

	public final void setApellido(final java.lang.String apellido) {
		this.apellidoProperty().set(apellido);
	}

	public final SimpleStringProperty nombrePilaProperty() {
		return this.nombrePila;
	}

	public final java.lang.String getNombrePila() {
		return this.nombrePilaProperty().get();
	}

	public final void setNombrePila(final java.lang.String nombrePila) {
		this.nombrePilaProperty().set(nombrePila);
	}

	public final SimpleStringProperty sexoProperty() {
		return this.sexo;
	}

	public final java.lang.String getSexo() {
		return this.sexoProperty().get();
	}

	public final void setSexo(final java.lang.String sexo) {
		this.sexoProperty().set(sexo);
	}

	public final SimpleObjectProperty<Persona> personaProperty() {
		return this.persona;
	}

	public final gestion.gestionFinal.modelo.Persona getPersona() {
		return this.personaProperty().get();
	}

	public final void setPersona(final gestion.gestionFinal.modelo.Persona persona) {
		this.personaProperty().set(persona);
	}

	public final SimpleObjectProperty<Personafisica> padreProperty() {
		return this.padre;
	}

	public final gestion.gestionFinal.modelo.Personafisica getPadre() {
		return this.padreProperty().get();
	}

	public final void setPadre(final gestion.gestionFinal.modelo.Personafisica padre) {
		this.padreProperty().set(padre);
	}

	public final SimpleObjectProperty<Personafisica> conyugeProperty() {
		return this.conyuge;
	}

	public final gestion.gestionFinal.modelo.Personafisica getConyuge() {
		return this.conyugeProperty().get();
	}

	public final void setConyuge(
			final gestion.gestionFinal.modelo.Personafisica conyuge) {
		this.conyugeProperty().set(conyuge);
	}

	

}