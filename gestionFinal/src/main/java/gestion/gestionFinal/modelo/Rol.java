package gestion.gestionFinal.modelo;


import java.util.List;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


public class Rol{
	private static final long serialVersionUID = 1L;


	private SimpleIntegerProperty idRol = new SimpleIntegerProperty();

	private SimpleStringProperty descripcionRol = new SimpleStringProperty();

	private SimpleStringProperty nombreRol = new SimpleStringProperty();

	//bi-directional many-to-many association to Usuario
	private List<Usuario> usuarios;

	public Rol() {
		
	}
	
	public Rol(int id, String descripcionRol, String nombreRol){
		this.idRol.set(id);
		this.descripcionRol.set(descripcionRol);
		this.nombreRol.set(descripcionRol);
	}

	public final SimpleIntegerProperty idRolProperty() {
		return this.idRol;
	}

	public final int getIdRol() {
		return this.idRolProperty().get();
	}

	public final void setIdRol(final int idRol) {
		this.idRolProperty().set(idRol);
	}

	public final SimpleStringProperty descripcionRolProperty() {
		return this.descripcionRol;
	}

	public final java.lang.String getDescripcionRol() {
		return this.descripcionRolProperty().get();
	}

	public final void setDescripcionRol(final java.lang.String descripcionRol) {
		this.descripcionRolProperty().set(descripcionRol);
	}

	public final SimpleStringProperty nombreRolProperty() {
		return this.nombreRol;
	}

	public final java.lang.String getNombreRol() {
		return this.nombreRolProperty().get();
	}

	public final void setNombreRol(final java.lang.String nombreRol) {
		this.nombreRolProperty().set(nombreRol);
	}

	

}