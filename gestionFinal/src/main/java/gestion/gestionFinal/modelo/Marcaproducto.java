package gestion.gestionFinal.modelo;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;




import java.util.List;


/**
 * The persistent class for the marcaproducto database table.
 * 
 */

public class Marcaproducto{


	private SimpleIntegerProperty idMarcaProducto = new SimpleIntegerProperty();

	private SimpleStringProperty nombre = new SimpleStringProperty();

	//bi-directional many-to-one association to Producto
	private List<Producto> productos;

	public Marcaproducto() {
	}

	public Marcaproducto(int id, String nombre){
		this.idMarcaProducto.set(id);
		this.nombre.set(nombre);
	}
	
	

	@Override
	public String toString() {
		return nombre.get();
	}

	public final SimpleIntegerProperty idMarcaProductoProperty() {
		return this.idMarcaProducto;
	}

	public final int getIdMarcaProducto() {
		return this.idMarcaProductoProperty().get();
	}

	public final void setIdMarcaProducto(final int idMarcaProducto) {
		this.idMarcaProductoProperty().set(idMarcaProducto);
	}

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final java.lang.String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final java.lang.String nombre) {
		this.nombreProperty().set(nombre);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idMarcaProducto == null) ? 0 : idMarcaProducto.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marcaproducto other = (Marcaproducto) obj;
		if (idMarcaProducto == null) {
			if (other.idMarcaProducto != null)
				return false;
		} else if (
				//!idMarcaProducto.equals(other.idMarcaProducto)
				this.getIdMarcaProducto() != other.getIdMarcaProducto()
				)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (
				//!nombre.equals(other.nombre)
				!this.getNombre().equals(other.getNombre())
				)
			return false;
		return true;
	}

	
	
}