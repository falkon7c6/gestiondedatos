package gestion.gestionFinal.modelo;


import java.time.LocalDate;
import java.util.List;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * The persistent class for the venta database table.
 * 
 */

public class Venta{



	private SimpleIntegerProperty idVenta = new SimpleIntegerProperty();

	private SimpleFloatProperty bonificacion = new SimpleFloatProperty();

	private SimpleStringProperty condicionIVA = new SimpleStringProperty();

	private SimpleStringProperty condicionVenta = new SimpleStringProperty();

	private SimpleObjectProperty<LocalDate> fecha = new SimpleObjectProperty<LocalDate>();

	private SimpleIntegerProperty numeroComprobante = new SimpleIntegerProperty();

	private SimpleStringProperty observaciones = new SimpleStringProperty();

	private SimpleIntegerProperty puntoVenta = new SimpleIntegerProperty();

	private SimpleStringProperty tipoFactura = new SimpleStringProperty();

	private SimpleFloatProperty total = new SimpleFloatProperty();

	//bi-directional many-to-one association to Lineaventa
	private List<Lineaventa> lineaventas;

	//bi-directional many-to-one association to Cliente
	private SimpleObjectProperty<Cliente> cliente = new SimpleObjectProperty<Cliente>();

	//bi-directional many-to-one association to Empleado
	private SimpleObjectProperty<Empleado> empleado = new SimpleObjectProperty<Empleado>();

	//bi-directional many-to-one association to Periodo
	private SimpleObjectProperty<Periodo> periodo = new SimpleObjectProperty<Periodo>();

	public Venta() {
	}

	public Venta(int idVenta, Float bonificacion, String condicionIVA,
			String condicionVenta, LocalDate fecha, int numeroComprobante,
			String observaciones, int puntoVenta, String tipoFactura,
			Float total, List<Lineaventa> lineaventas, Cliente cliente,
			Empleado empleado, Periodo periodo) {
		this.idVenta.set(idVenta);
		this.bonificacion.set(bonificacion);
		this.condicionIVA.set(condicionIVA);
		this.condicionVenta.set(condicionVenta);
		this.fecha.set(fecha);
		this.numeroComprobante.set(numeroComprobante);
		this.observaciones.set(observaciones);
		this.puntoVenta.set(puntoVenta);
		this.tipoFactura.set(tipoFactura);
		this.total.set(total);
		this.lineaventas = lineaventas;
		this.cliente.set(cliente);
		this.empleado.set(empleado);
		this.periodo.set(periodo);
	}

	public final SimpleIntegerProperty idVentaProperty() {
		return this.idVenta;
	}

	public final int getIdVenta() {
		return this.idVentaProperty().get();
	}

	public final void setIdVenta(final int idVenta) {
		this.idVentaProperty().set(idVenta);
	}

	public final SimpleFloatProperty bonificacionProperty() {
		return this.bonificacion;
	}

	public final float getBonificacion() {
		return this.bonificacionProperty().get();
	}

	public final void setBonificacion(final float bonificacion) {
		this.bonificacionProperty().set(bonificacion);
	}

	public final SimpleStringProperty condicionIVAProperty() {
		return this.condicionIVA;
	}

	public final java.lang.String getCondicionIVA() {
		return this.condicionIVAProperty().get();
	}

	public final void setCondicionIVA(final java.lang.String condicionIVA) {
		this.condicionIVAProperty().set(condicionIVA);
	}

	public final SimpleStringProperty condicionVentaProperty() {
		return this.condicionVenta;
	}

	public final java.lang.String getCondicionVenta() {
		return this.condicionVentaProperty().get();
	}

	public final void setCondicionVenta(final java.lang.String condicionVenta) {
		this.condicionVentaProperty().set(condicionVenta);
	}

	public final SimpleObjectProperty<LocalDate> fechaProperty() {
		return this.fecha;
	}

	public final java.time.LocalDate getFecha() {
		return this.fechaProperty().get();
	}

	public final void setFecha(final java.time.LocalDate fecha) {
		this.fechaProperty().set(fecha);
	}

	public final SimpleIntegerProperty numeroComprobanteProperty() {
		return this.numeroComprobante;
	}

	public final int getNumeroComprobante() {
		return this.numeroComprobanteProperty().get();
	}

	public final void setNumeroComprobante(final int string) {
		this.numeroComprobanteProperty().set(string);
	}

	public final SimpleStringProperty observacionesProperty() {
		return this.observaciones;
	}

	public final java.lang.String getObservaciones() {
		return this.observacionesProperty().get();
	}

	public final void setObservaciones(final java.lang.String observaciones) {
		this.observacionesProperty().set(observaciones);
	}

	public final SimpleIntegerProperty puntoVentaProperty() {
		return this.puntoVenta;
	}

	public final int getPuntoVenta() {
		return this.puntoVentaProperty().get();
	}

	public final void setPuntoVenta(final int puntoVenta) {
		this.puntoVentaProperty().set(puntoVenta);
	}

	public final SimpleStringProperty tipoFacturaProperty() {
		return this.tipoFactura;
	}

	public final java.lang.String getTipoFactura() {
		return this.tipoFacturaProperty().get();
	}

	public final void setTipoFactura(final java.lang.String tipoFactura) {
		this.tipoFacturaProperty().set(tipoFactura);
	}

	public final SimpleFloatProperty totalProperty() {
		return this.total;
	}

	public final float getTotal() {
		return this.totalProperty().get();
	}

	public final void setTotal(final float total) {
		this.totalProperty().set(total);
	}

	public final SimpleObjectProperty<Cliente> clienteProperty() {
		return this.cliente;
	}

	public final gestion.gestionFinal.modelo.Cliente getCliente() {
		return this.clienteProperty().get();
	}

	public final void setCliente(final gestion.gestionFinal.modelo.Cliente cliente) {
		this.clienteProperty().set(cliente);
	}

	public final SimpleObjectProperty<Empleado> empleadoProperty() {
		return this.empleado;
	}

	public final gestion.gestionFinal.modelo.Empleado getEmpleado() {
		return this.empleadoProperty().get();
	}

	public final void setEmpleado(
			final gestion.gestionFinal.modelo.Empleado empleado) {
		this.empleadoProperty().set(empleado);
	}

	public final SimpleObjectProperty<Periodo> periodoProperty() {
		return this.periodo;
	}

	public final gestion.gestionFinal.modelo.Periodo getPeriodo() {
		return this.periodoProperty().get();
	}

	public final void setPeriodo(final gestion.gestionFinal.modelo.Periodo periodo) {
		this.periodoProperty().set(periodo);
	}

	
	

}