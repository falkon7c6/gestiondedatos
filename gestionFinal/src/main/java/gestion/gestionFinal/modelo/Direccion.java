package gestion.gestionFinal.modelo;


import java.util.List;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 * The persistent class for the direccion database table.
 * 
 */

public class Direccion{


	private SimpleIntegerProperty idDireccion = new SimpleIntegerProperty();

	private SimpleStringProperty calle = new SimpleStringProperty();

	private SimpleStringProperty departamento = new SimpleStringProperty();

	private SimpleStringProperty localidad = new SimpleStringProperty();

	private SimpleStringProperty numero = new SimpleStringProperty();

	private SimpleStringProperty piso = new SimpleStringProperty();

	private SimpleStringProperty provincia = new SimpleStringProperty();

	//bi-directional many-to-one association to Persona

	private List<Persona> personas;

	public Direccion() {
	}

	public Direccion(int idDireccion, String calle, String departamento,
			String localidad, String numero, String piso, String provincia) {

		this.idDireccion.set(idDireccion);
		this.calle.set(calle);
		this.departamento.set(departamento);
		this.localidad.set(localidad);
		this.numero.set(numero);
		this.piso.set(piso);
		this.provincia.set(provincia);
	}

	
	
	@Override
	public String toString() {
		String direccion = calle.get() + " " + numero.get() + " " + piso.get() 
				+ " " + departamento.get() + " " + localidad.get() + " " + provincia.get();
		return direccion;
	}

	public final SimpleIntegerProperty idDireccionProperty() {
		return this.idDireccion;
	}

	public final int getIdDireccion() {
		return this.idDireccionProperty().get();
	}

	public final void setIdDireccion(final int idDireccion) {
		this.idDireccionProperty().set(idDireccion);
	}

	public final SimpleStringProperty calleProperty() {
		return this.calle;
	}

	public final java.lang.String getCalle() {
		return this.calleProperty().get();
	}

	public final void setCalle(final java.lang.String calle) {
		this.calleProperty().set(calle);
	}

	public final SimpleStringProperty departamentoProperty() {
		return this.departamento;
	}

	public final java.lang.String getDepartamento() {
		return this.departamentoProperty().get();
	}

	public final void setDepartamento(final java.lang.String departamento) {
		this.departamentoProperty().set(departamento);
	}

	public final SimpleStringProperty localidadProperty() {
		return this.localidad;
	}

	public final java.lang.String getLocalidad() {
		return this.localidadProperty().get();
	}

	public final void setLocalidad(final java.lang.String localidad) {
		this.localidadProperty().set(localidad);
	}

	public final SimpleStringProperty numeroProperty() {
		return this.numero;
	}

	public final java.lang.String getNumero() {
		return this.numeroProperty().get();
	}

	public final void setNumero(final java.lang.String numero) {
		this.numeroProperty().set(numero);
	}

	public final SimpleStringProperty pisoProperty() {
		return this.piso;
	}

	public final java.lang.String getPiso() {
		return this.pisoProperty().get();
	}

	public final void setPiso(final java.lang.String piso) {
		this.pisoProperty().set(piso);
	}

	public final SimpleStringProperty provinciaProperty() {
		return this.provincia;
	}

	public final java.lang.String getProvincia() {
		return this.provinciaProperty().get();
	}

	public final void setProvincia(final java.lang.String provincia) {
		this.provinciaProperty().set(provincia);
	}

	

}