package gestion.gestionFinal.controlador;

import gestion.gestionFinal.persistencia.databaseConnection;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

public class Seguridad {
	private databaseConnection conexion;
	private CallableStatement llamada;

	/*
	 * Ejemplos de uso Seguridad s = new Seguridad();
	 * s.concederPrivilegiosLiquidacion("33272"); s.altaUsuario("33273", "123",
	 * "administrador"); s.altaUsuario("33274", "123", "liquidacion");
	 * 
	 * System.out.println(s.obtenerRol("33273"));
	 * System.out.println(s.obtenerRol("33274"));
	 */

	public Seguridad() {
		conexion = new databaseConnection();
	}

	private void realizarLlamada() throws SQLException {
		try {
			llamada.execute();
		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
					System.out.println("Connection Closed");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void altaUsuario(String legajo, String clave, String rol)
			throws SQLException {
		conexion.conectar();
		llamada = conexion.connection.prepareCall("{call Alta_Usuario(?,?)}");
		llamada.setString(1, legajo);
		llamada.setString(2, clave);
		realizarLlamada();

		if (rol == "administracion") {
			concederPrivilegiosAdministracion(legajo);
		} else if (rol == "compraventa") {
			concederPrivilegiosCompraVenta(legajo);
		} else if (rol == "liquidacion") {
			concederPrivilegiosLiquidacion(legajo);
		}
	}

	void bajaLogicaUsuario(String legajo) throws SQLException {
		revocarPrivilegios(legajo);
	}

	void bajaUsuario(String legajo) throws SQLException {
		conexion.conectar();
		llamada = conexion.connection.prepareCall("{call Baja_Usuario(?)}");
		llamada.setString(1, legajo);
		realizarLlamada();
	}

	void concederPrivilegiosAdministracion(String legajo) throws SQLException {
		conexion.conectar();
		llamada = conexion.connection
				.prepareCall("{call Privilegios_Administracion(?)}");
		llamada.setString(1, legajo);
		realizarLlamada();
	}

	void concederPrivilegiosCompraVenta(String legajo) throws SQLException {
		conexion.conectar();
		llamada = conexion.connection
				.prepareCall("{call Privilegios_CompraVenta(?)}");
		llamada.setString(1, legajo);
		realizarLlamada();
	}

	void concederPrivilegiosLiquidacion(String legajo) throws SQLException {
		conexion.conectar();
		llamada = conexion.connection
				.prepareCall("{call Privilegios_Liquidacion(?)}");
		llamada.setString(1, legajo);
		realizarLlamada();
	}

	void revocarPrivilegios(String legajo) throws SQLException {
		conexion.conectar();
		llamada = conexion.connection
				.prepareCall("{call Revocar_Privilegios(?)}");
		llamada.setString(1, legajo);
		realizarLlamada();
	}

	String obtenerRol(String legajo) throws SQLException {
		String rol = "";
		conexion.conectar();
		try {

			llamada = conexion.connection
					.prepareCall("{call gestionfinal.obtenerRol(?, ?)}");
			llamada.setString(1, legajo);
			llamada.registerOutParameter(2, Types.VARCHAR);

			llamada.execute();
			rol = llamada.getString(2);

		} finally {

			if (llamada != null) {

				try {
					llamada.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (conexion.connection != null) {

				try {
					conexion.connection.close();
					System.out.println("Connection Closed");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return rol;
	}

	ArrayList<String> obtenerLegajosPorRol(String Rol) {
		// TODO
		ArrayList<String> legajos = new ArrayList<>();
		return legajos;
	}

}