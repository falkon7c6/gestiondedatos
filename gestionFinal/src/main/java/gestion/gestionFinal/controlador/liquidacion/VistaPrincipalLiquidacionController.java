package gestion.gestionFinal.controlador.liquidacion;

import gestion.gestionFinal.modelo.Conceptoliquidacion;
import gestion.gestionFinal.modelo.Empleado;
import gestion.gestionFinal.modelo.Linealiquidacion;
import gestion.gestionFinal.modelo.Liquidacion;
import gestion.gestionFinal.modelo.Periodo;
import gestion.gestionFinal.modelo.Personafisica;
import gestion.gestionFinal.modelo.Sueldo;
import gestion.gestionFinal.persistencia.ConceptoDAO;
import gestion.gestionFinal.persistencia.EmpleadoDAO;
import gestion.gestionFinal.persistencia.LineaLiquidacionDAO;
import gestion.gestionFinal.persistencia.LiquidacionDAO;
import gestion.gestionFinal.persistencia.SueldoDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.ReportUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/liquidacion/VistaPrincipalLiquidacion.fxml")
public class VistaPrincipalLiquidacionController {
	@FXML
	private TableView<Linealiquidacion> tabla;
	@FXML
	private TableColumn<Linealiquidacion, String> colNombre;
	@FXML
	private TableColumn<Linealiquidacion, String> colDescripcionConcepto;
	@FXML
	private TableColumn<Linealiquidacion, String> colTipo;
	@FXML
	private TableColumn<Linealiquidacion, String> colDescripcionLinea;
	@FXML
	private TableColumn<Linealiquidacion, String> colImporte;
	@FXML
	@ActionTrigger("guardar")
	private Button botonGuardar;
	@FXML
	@ActionTrigger("atras")
	private Button botonAtras;
	@FXML
	@ActionTrigger("agregar")
	private Button botonAgregar;
	@FXML
	@ActionTrigger("editar")
	private Button botonModificar;
	@FXML
	@ActionTrigger("borrar")
	private Button botonEliminar;
	@FXML
	private Label labelFecha;
	@FXML
	private Label labelHora;
	@FXML
	private Label labelEmpleado;
	@FXML
	private Label labelPeriodo;
	@FXML
	private Label labelTotal;
	@FXML
	@ActionTrigger("Seleccionar Empleado")
	private Button botonSeleccionar;
	@FXML
	private TextField textCuil;
	@FXML
	private TextField textLegajo;
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textApellido;
	@FXML
	private TextField textSexo;
	@FXML
	private TextField textTelefono;
	@FXML
	private TextField textDireccion;
	@FXML
	private TextField textCategoria;
	@FXML
	private TextField textCalificacionProfesional;
	@FXML
	private TextField textObraSocial;
	@FXML
	private TextField textActivo;
	@FXML
	private TextField textFamiliares;
	@FXML
	private DatePicker fechaPagoDatePicker;

	@FXMLViewFlowContext
	private ViewFlowContext context;

	Stage ventanaDialog;
	private Linealiquidacion lineaNueva;
	private ObservableList<Linealiquidacion> ItemsTabla;
	private Empleado EmpleadoSeleccionado;
	private Float total = 0F;
	private LocalDate fechaPago;
	private LocalDate fechaLiquidacion;
	private LiquidacionDAO liquidacionDAO;
	private LineaLiquidacionDAO lineaDAO;
	private Periodo periodo;
	private ConceptoDAO conceptoDAO;
	private ObservableList<Personafisica> familia;
	private EmpleadoDAO empleadoDAO;

	@PostConstruct
	public void Initialize() {
		ItemsTabla = FXCollections.observableArrayList();
		familia = FXCollections.observableArrayList();
		iniciarColumnas();
		tabla.setItems(ItemsTabla);
		fechaLiquidacion = LocalDate.now();
		fechaPago = LocalDate.now();
		liquidacionDAO = new LiquidacionDAO();
		lineaDAO = new LineaLiquidacionDAO();
		empleadoDAO = new EmpleadoDAO();
		deshabilitarABM();
		Empleado empleadoActual = (Empleado) context.getApplicationContext()
				.getRegisteredObject("empleadoActual");
		labelEmpleado.setText(empleadoActual.toString());
		labelFecha.setText(LocalDate.now().toString());
		periodo = (Periodo) context.getApplicationContext()
				.getRegisteredObject("periodo");
		labelPeriodo.setText(periodo.getDescripcion());
	}

	@ActionMethod("agregar")
	public void agregar() {
		ventanaDialog = crearDialogLinea(null);
		ventanaDialog.showAndWait();
		if (lineaNueva != null) {
			ItemsTabla.add(lineaNueva);
			total += lineaNueva.getImporte();
			labelTotal.setText(Float.toString(total));
			lineaNueva = null;
		} else {
			System.out.println("se cancelo");
		}
		ventanaDialog = null;
	}

	@ActionMethod("Seleccionar Empleado")
	public void SeleccionarEmpleado() {
		ventanaDialog = crearDialogEmpleado(null);
		ventanaDialog.showAndWait();
		if (EmpleadoSeleccionado != null) {
			familia = empleadoDAO.listarHijos(EmpleadoSeleccionado);
			textNombre.setText(EmpleadoSeleccionado.getPersonafisica()
					.getPersona().getNombre());
			textApellido.setText(EmpleadoSeleccionado.getPersonafisica()
					.getApellido());
			activo(EmpleadoSeleccionado.getActivo());
			textCalificacionProfesional.setText(EmpleadoSeleccionado
					.getCalificacionprofesional().toString());
			textCategoria.setText(EmpleadoSeleccionado.getCategoriaempleado()
					.toString());
			textCuil.setText(EmpleadoSeleccionado.getPersonafisica()
					.getPersona().getCuilcuit());
			textDireccion.setText(EmpleadoSeleccionado.getPersonafisica()
					.getPersona().getDireccionTexto());
			textFamiliares.setText("");
			textLegajo.setText(Integer.toString(EmpleadoSeleccionado
					.getLegajo()));
			textNombre.setText(EmpleadoSeleccionado.getPersonafisica()
					.getPersona().getNombre());
			textObraSocial.setText(EmpleadoSeleccionado.getObrasocial()
					.toString());
			textTelefono.setText(EmpleadoSeleccionado.getPersonafisica()
					.getPersona().getTelefono());
			textSexo.setText(EmpleadoSeleccionado.getPersonafisica().getSexo());
			textFamiliares.setText(familia.size() + " hijos");
			habilitarABM();

			// linea Sueldo categoria
			conceptoDAO = new ConceptoDAO();
			ObservableList<Conceptoliquidacion> listadoConceptosBase = FXCollections
					.observableArrayList();
			listadoConceptosBase = conceptoDAO.listarConceptosObligatorios();
			Float importecategoria = 0F;
			Float importeCalificacion = 0F;
			Float importeObra = 0F;
			Float importeFamilia = 0F;
			for (Conceptoliquidacion concepto : listadoConceptosBase) {
				Linealiquidacion linea = new Linealiquidacion();
				if (concepto.getNombre().equalsIgnoreCase("categoriaEmpleado")) {
					concepto.setMonto(EmpleadoSeleccionado
							.getCategoriaempleado().getSalarioHora()
							* EmpleadoSeleccionado.getHorasSemanales() * 4);
					importecategoria = EmpleadoSeleccionado
							.getCategoriaempleado().getSalarioHora()
							* EmpleadoSeleccionado.getHorasSemanales() * 4;
					linea.setImporte(importecategoria);
					linea.setDescripcion("sueldo por categoria");
				} else if (concepto.getNombre().equalsIgnoreCase(
						"calificacionProf")) {
					concepto.setMonto(EmpleadoSeleccionado
							.getCalificacionprofesional().getSalarioHora()
							* EmpleadoSeleccionado.getHorasSemanales() * 4);
					importeCalificacion = EmpleadoSeleccionado
							.getCalificacionprofesional().getSalarioHora()
							* EmpleadoSeleccionado.getHorasSemanales() * 4;
					linea.setDescripcion("sueldo por titulo");
					linea.setImporte(importeCalificacion);
				} else if (concepto.getNombre().equalsIgnoreCase("obraSocial")) {
					concepto.setMonto(0 - (EmpleadoSeleccionado.getObrasocial()
							.getImporteFijo() + (EmpleadoSeleccionado
							.getObrasocial().getImportePorcentual() * (importeCalificacion + importecategoria))));
					importeObra = concepto.getMonto();
					linea.setDescripcion("retencion por Obra Social");
					linea.setImporte(importeObra);
				} else if (concepto.getNombre().equalsIgnoreCase("familia")) {
					concepto.setMonto((importecategoria + importeCalificacion)
							* 0.10F * familia.size()); // TODO parametro
					importeFamilia = concepto.getMonto();
					linea.setDescripcion("pago por familia");
					linea.setImporte(importeFamilia);
				}
				linea.setConceptoliquidacion(concepto);
				ItemsTabla.add(linea);
			}
			// linea Sueldo obra social
			this.total = importeCalificacion + importecategoria + importeObra
					+ importeFamilia;
			this.labelTotal.setText(Float.toString(total));
			this.botonSeleccionar.setDisable(true);
		} else {
			System.out.println("no se selecciono ningun empleado");
		}
		ventanaDialog = null;
	}

	@ActionMethod("borrar")
	public void borrar() {
		int index = tabla.getSelectionModel().getSelectedIndex();
		if (index >= 0) {
			total = total
					- ((Linealiquidacion) tabla.getSelectionModel()
							.getSelectedItem()).getImporte();
			labelTotal.setText(Float.toString(total));
			ItemsTabla.remove(index);

		}
	}

	@ActionMethod("editar")
	public void editar() {

	}

	public void iniciarColumnas() {
		this.colDescripcionConcepto
				.setCellValueFactory(new Callback<CellDataFeatures<Linealiquidacion, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Linealiquidacion, String> p) {
						return p.getValue().getConceptoliquidacion()
								.descripcionProperty();
					}
				});
		this.colDescripcionLinea
				.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
		this.colImporte.setCellValueFactory(new PropertyValueFactory<>(
				"importe"));
		this.colNombre
				.setCellValueFactory(new Callback<CellDataFeatures<Linealiquidacion, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Linealiquidacion, String> p) {
						return p.getValue().getConceptoliquidacion()
								.nombreProperty();
					}
				});
		this.colTipo
				.setCellValueFactory(new Callback<CellDataFeatures<Linealiquidacion, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Linealiquidacion, String> p) {
						return p.getValue().getConceptoliquidacion()
								.tipoProperty();
					}
				});

	}

	public Stage crearDialogEmpleado(Empleado emp) {
		Stage stage = new Stage();
		AnchorPane root = null;
		try {
			root = FXMLLoader
					.load(getClass()
							.getResource(
									"/gestion/gestionFinal/vista/liquidacion/SeleccionarEmpleadoDialog.fxml"));
			Flow mainFlow = new Flow(SeleccionarEmpleadoDialogController.class);
			io.datafx.controller.flow.FlowHandler mainFlowHandler = mainFlow
					.createHandler();
			// agrega la animacion
			StackPane pane = mainFlowHandler.start(new AnimatedFlowContainer(
					Duration.millis(320), ContainerAnimations.ZOOM_IN));
			SeleccionarEmpleadoDialogController controladorDialog = (SeleccionarEmpleadoDialogController) mainFlowHandler
					.getCurrentViewContext().getController();
			controladorDialog.setControladorVentanaPrincipal(this);
			root.getChildren().add(pane);
			Scene escena = new Scene(root);
			escena.getStylesheets().add("/styles/NewFile.css");
			stage.setTitle("Categoria de Productos");
			stage.setScene(escena);
			stage.initModality(Modality.APPLICATION_MODAL);
			controladorDialog.setDialogStage(stage);
			// si la linea que le pasas no es nula, edita y no hace nueva
			if (emp != null) {
				controladorDialog.setEmpleadoActual(emp);
				// controladorDialog.editar();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stage;
	}

	public Stage crearDialogLinea(Linealiquidacion linea) {
		Stage stage = new Stage();
		AnchorPane root = null;
		try {
			root = FXMLLoader
					.load(getClass()
							.getResource(
									"/gestion/gestionFinal/vista/liquidacion/DialogLiquidacion.fxml"));
			Flow mainFlow = new Flow(DialogLiquidacionController.class);
			io.datafx.controller.flow.FlowHandler mainFlowHandler = mainFlow
					.createHandler();
			// agrega la animacion
			StackPane pane = mainFlowHandler.start(new AnimatedFlowContainer(
					Duration.millis(320), ContainerAnimations.ZOOM_IN));
			DialogLiquidacionController controladorDialog = (DialogLiquidacionController) mainFlowHandler
					.getCurrentViewContext().getController();
			controladorDialog.setControladorVentanaPrincipal(this);
			root.getChildren().add(pane);
			Scene escena = new Scene(root);
			escena.getStylesheets().add("/styles/NewFile.css");
			stage.setTitle("Conceptos de Liquidacion");
			stage.setScene(escena);
			stage.initModality(Modality.APPLICATION_MODAL);
			controladorDialog.setDialogStage(stage);
			// si la linea que le pasas no es nula, edita y no hace nueva
			if (linea != null) {
				controladorDialog.setLineaActual(linea);
				// controladorDialog.editar();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stage;
	}

	@ActionMethod("guardar")
	public void guardar() {
		int idLiquidacion = 0;
		try {
			if (this.validar() == true) {
				Float sueldoCredito = 0F;
				Float sueldoDebito = 0F;
				Float neto = 0F;
				Liquidacion liquidacion = new Liquidacion();
				liquidacion.setEmpleado(EmpleadoSeleccionado);
				liquidacion.setFechaLiquidacion(fechaLiquidacion);
				fechaPago = this.fechaPagoDatePicker.getValue();
				liquidacion.setFechaPago(fechaPago);
				liquidacion.setPeriodo(periodo);
				liquidacion.setTotal(total);
				idLiquidacion = liquidacionDAO.insertar(liquidacion);
				liquidacion.setIdLiquidacion(idLiquidacion);
				System.out.println("id liquidacion "
						+ liquidacion.getIdLiquidacion());
				for (Linealiquidacion linea : ItemsTabla) {
					int idLinea = 0;
					linea.setLiquidacion(liquidacion);
					idLinea = lineaDAO.insertar(linea);
					linea.setIdDetalleLiquidacion(idLinea);
					if (linea.getImporte() < 0) {
						sueldoDebito = sueldoDebito + linea.getImporte();
					} else {
						sueldoCredito = sueldoCredito + linea.getImporte();
					}
					System.out.println("id linea "
							+ linea.getIdDetalleLiquidacion());
				}
				neto = sueldoCredito + sueldoDebito;
				System.out.println("NETO " + neto);
				Sueldo sueldo = new Sueldo();
				SueldoDAO sueldoDAO = new SueldoDAO();
				sueldo.setApellidos(EmpleadoSeleccionado.getPersonafisica()
						.getApellido());
				sueldo.setNombres(EmpleadoSeleccionado.getPersonafisica()
						.getPersona().getNombre());
				sueldo.setCalificacionProfesional(EmpleadoSeleccionado
						.getCalificacionprofesional().toString());
				sueldo.setCategoriaEmpleado(EmpleadoSeleccionado
						.getCategoriaempleado().toString());
				sueldo.setCentroCostos("LOCAL");
				sueldo.setLegajo(String.valueOf(EmpleadoSeleccionado
						.getLegajo()));
				sueldo.setNetoCobrar(neto.toString());
				sueldo.setObraSocial(EmpleadoSeleccionado.getObrasocial()
						.toString());
				sueldo.setSueldo(sueldoCredito.toString());
				sueldo.setDeducciones(sueldoDebito.toString());
				sueldoDAO.insertar(sueldo);
			}
			// aca imprime reporte
			// imprime el reporte
			if (DialogsUtil.confirmationDialog("Reporte",
					"Se va a imprimir el comprobante de liquidacion",
					"Presione OK para continuar") == true) {
				Map<String, Object> parametrosMap = new HashMap();
				parametrosMap.put("idliquidacion", new Integer(idLiquidacion));
				ReportUtil reporterReportUtil = new ReportUtil();
				String reporteNombre = "liquidacion"
						+ this.getEmpleadoSeleccionado().getLegajo() + ".pdf";
				reporterReportUtil.imprimirPDF(
						"/reportes/ReciboHaberesPrueba.jasper", parametrosMap,
						reporteNombre);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean validar() {
		if (Validate.tableValidate(tabla, "tabla conceptos")
				&& Validate.datePickerValidate(fechaPagoDatePicker,
						"Fecha de Pago")) {
			return true;
		}
		return false;
	}

	public void habilitarABM() {
		this.botonGuardar.setDisable(false);
		this.botonAgregar.setDisable(false);
		this.botonEliminar.setDisable(false);
		this.botonModificar.setDisable(false);

	}

	public void deshabilitarABM() {
		this.botonGuardar.setDisable(true);
		this.botonAgregar.setDisable(true);
		this.botonEliminar.setDisable(true);
		this.botonModificar.setDisable(true);
	}

	public void activo(int activo) {
		if (activo == 0) {
			this.textActivo.setText("no");
		} else {
			this.textActivo.setText("si");
		}
	}

	@ActionMethod("editar")
	public void modificarLinea() {
		if (Validate.tableNotSelectedItemValidate(tabla)) {
			lineaNueva = tabla.getSelectionModel()
					.getSelectedItem();
			float importeAnterior = lineaNueva.getImporte();
			ventanaDialog = crearDialogLinea(lineaNueva);
			ventanaDialog.showAndWait();
			if (lineaNueva != null) {
				// ItemsTabla.add(lineaNueva);
				total -= importeAnterior;
				total += lineaNueva.getImporte();
				labelTotal.setText(Float.toString(total));
				lineaNueva = null;
			} else {
				System.out.println("se cancelo");
			}
			ventanaDialog = null;
		}
	}

	// SETS Y GETS

	public Stage getVentanaDialog() {
		return ventanaDialog;
	}

	public void setVentanaDialog(Stage ventanaDialog) {
		this.ventanaDialog = ventanaDialog;
	}

	public Linealiquidacion getLineaNueva() {
		return lineaNueva;
	}

	public void setLineaNueva(Linealiquidacion lineaNueva) {
		this.lineaNueva = lineaNueva;
	}

	public Empleado getEmpleadoSeleccionado() {
		return EmpleadoSeleccionado;
	}

	public void setEmpleadoSeleccionado(Empleado empleadoSeleccionado) {
		EmpleadoSeleccionado = empleadoSeleccionado;
	}

}
