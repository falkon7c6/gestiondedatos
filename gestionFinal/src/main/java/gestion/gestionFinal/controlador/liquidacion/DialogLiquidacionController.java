package gestion.gestionFinal.controlador.liquidacion;

import gestion.gestionFinal.modelo.Conceptoliquidacion;
import gestion.gestionFinal.modelo.Linealiquidacion;
import gestion.gestionFinal.persistencia.ConceptoDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/liquidacion/DialogLiquidacion.fxml")
public class DialogLiquidacionController {

	@FXML
	private TableView tabla;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TableColumn colDescripcion;
	@FXML
	private TableColumn colMonto;
	@FXML
	private TableColumn colTipo;
	@FXML
	private TableColumn colHabilitado;
	@FXML
	private TextField textDescripcion;
	@FXML
	private TextField textImporte;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	@FXML
	private TextField textFiltrar;

	@FXMLViewFlowContext
	private ViewFlowContext context;

	private Stage DialogStage;
	private VistaPrincipalLiquidacionController controladorVentanaPrincipal;
	private ObservableList<Conceptoliquidacion> itemsTabla;
	private ObservableList<Conceptoliquidacion> listaFiltrada;
	private Linealiquidacion lineaActual;
	private ConceptoDAO conceptoDAO;

	@PostConstruct
	public void Initialize() {
		System.out.println("entro a initialize");
		itemsTabla = FXCollections.observableArrayList();
		listaFiltrada = FXCollections.observableArrayList();
		iniciarCampos();
		iniciarColumnas();
		tabla.setItems(itemsTabla);
		if (lineaActual == null) {
			lineaActual = new Linealiquidacion();
		}
	}

	@ActionMethod("aceptar")
	public void aceptar() {
		try {
			if (controladorVentanaPrincipal != null) {
				if (tabla.getSelectionModel().getSelectedIndex() > -1
						&& Validate.textFieldValidate(textDescripcion,
								"Descripcion")) {
					lineaActual
							.setConceptoliquidacion((Conceptoliquidacion) tabla
									.getSelectionModel().getSelectedItem());

					lineaActual.setDescripcion(textDescripcion.getText());
					System.out.println("tipo: "
							+ lineaActual.getConceptoliquidacion().getTipo()); // TODO
					if (lineaActual.getConceptoliquidacion().getTipo()
							.equalsIgnoreCase("credito")) {
						lineaActual.setImporte(lineaActual
								.getConceptoliquidacion().getMonto());
					} else {
						lineaActual.setImporte(0 - lineaActual
								.getConceptoliquidacion().getMonto());
					}
					controladorVentanaPrincipal.setLineaNueva(lineaActual);
					lineaActual = null;
					DialogStage.close();
				} else {
					DialogsUtil.messageDialog("Mensaje",
							"seleccione un item de la tabla");
				}
			}
		} catch (NumberFormatException e) {
			System.out.println("error en el campo numerico");
		} catch (NullPointerException e) {
			System.out.println("cuando no java, cuando no");
		}
	}

	@ActionMethod("cancelar")
	public void cancelar() {
		DialogStage.close();
	}

	public void iniciarColumnas() {
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<>(
				"descripcion"));
		this.colHabilitado.setCellValueFactory(new PropertyValueFactory<>(
				"deshabilitado"));
		this.colMonto.setCellValueFactory(new PropertyValueFactory<>("monto"));
		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<>("nombre"));
		this.colTipo.setCellValueFactory(new PropertyValueFactory<>("tipo"));
	}

	public void iniciarCampos() {
		conceptoDAO = new ConceptoDAO();
		itemsTabla = conceptoDAO.listar();
		textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});

	}

	public void filtrar() {
		String filtrado = textFiltrar.getText().toLowerCase();
		this.listaFiltrada = FXCollections.observableArrayList();
		for (Conceptoliquidacion liqTemp : itemsTabla) {
			if (liqTemp.getNombre().toLowerCase().startsWith(filtrado)) {
				listaFiltrada.add(liqTemp);
			}
			// cambia la lista por la filtrada (la original no se pierde)
			tabla.setItems(listaFiltrada);
		}
	}

	public void editar() {
		tabla.getSelectionModel().select(lineaActual.getConceptoliquidacion());
		textImporte.setText(Float.toString(lineaActual.getImporte()));
		textDescripcion.setText(lineaActual.getDescripcion());
	}

	public Stage getDialogStage() {
		return DialogStage;
	}

	public void setDialogStage(Stage dialogStage) {
		DialogStage = dialogStage;
	}

	public VistaPrincipalLiquidacionController getControladorVentanaPrincipal() {
		return controladorVentanaPrincipal;
	}

	public void setControladorVentanaPrincipal(
			VistaPrincipalLiquidacionController controladorVentanaPrincipal) {
		this.controladorVentanaPrincipal = controladorVentanaPrincipal;
	}

	public Linealiquidacion getLineaActual() {
		return lineaActual;
	}

	public void setLineaActual(Linealiquidacion lineaActual) {
		this.lineaActual = lineaActual;
	}

}
