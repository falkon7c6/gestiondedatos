package gestion.gestionFinal.controlador.liquidacion;

import gestion.gestionFinal.modelo.Empleado;
import gestion.gestionFinal.persistencia.EmpleadoDAO;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/liquidacion/SeleccionarEmpleadoDialog.fxml")
public class SeleccionarEmpleadoDialogController {
	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView tabla;
	@FXML
	private TableColumn colLegajo;
	@FXML
	private TableColumn colCuilCuit;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TableColumn colApellido;
	@FXML
	private TableColumn colTelefono;
	@FXML
	private TableColumn colDireccion;
	@FXML
	private TableColumn colSexo;
	@FXML
	private TableColumn colNombrePila;
	@FXML
	private TableColumn colActivo;
	@FXML
	private TableColumn colFechaIngreso;
	@FXML
	private TableColumn colHorasSemanales;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	
	@FXMLViewFlowContext
	private ViewFlowContext context;
	
	private Stage DialogStage;
	private VistaPrincipalLiquidacionController controladorVentanaPrincipal;
	private Empleado empleadoActual;
	private ObservableList<Empleado> listaEmpleados;
	private ObservableList<Empleado> listaFiltrada;
	private EmpleadoDAO empleadoDAO;
	
	@PostConstruct
	public void Initialize(){
		empleadoDAO = new EmpleadoDAO();
		listaEmpleados = FXCollections.observableArrayList();
		listaFiltrada = FXCollections.observableArrayList();
		listaEmpleados = empleadoDAO.listarActivos();
		empleadoActual = new Empleado();
		iniciarColumnas();
		iniciarItems();
		tabla.setItems(listaEmpleados);
	}

	@ActionMethod("aceptar")
	public void aceptar(){
		try {
			if(controladorVentanaPrincipal != null){
				if (tabla.getSelectionModel().getSelectedIndex() >= 0){
					empleadoActual = (Empleado) tabla.getSelectionModel().getSelectedItem();
					controladorVentanaPrincipal.setEmpleadoSeleccionado(empleadoActual);
					empleadoActual = null;
					DialogStage.close();
				}
			}
		} catch(NullPointerException e){
			System.out.println("cuando no java, cuando no");
		}
	}
	
	
	@ActionMethod("cancelar")
	public void cancelar() {
		DialogStage.close();
	}
	
	public void iniciarColumnas(){
		this.colLegajo
		.setCellValueFactory(new PropertyValueFactory<>("legajo"));
this.colActivo
		.setCellValueFactory(new PropertyValueFactory<>("activo"));

this.colDireccion
		.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(
					CellDataFeatures<Empleado, String> p) {
				return p.getValue().getPersonafisica().getPersona()
						.direccionTextoProperty();
			}
		});


/*this.colCuilCuit
		.setCellValueFactory(new Callback<CellDataFeatures<Empleado, Integer>, ObservableValue<Number>>() {
			public ObservableValue<Number> call(
					CellDataFeatures<Empleado, Integer> p) {
				SimpleIntegerProperty cuil = new SimpleIntegerProperty (p
						.getValue().getPersonafisica().getPersona()
						.getCuilcuit());
				return cuil;
			}
		});*/

this.colApellido
		.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(
					CellDataFeatures<Empleado, String> p) {
				return p.getValue().getPersonafisica()
						.apellidoProperty();
			}
		});

this.colFechaIngreso.setCellValueFactory(new PropertyValueFactory<>(
		"fechaIngreso"));


this.colHorasSemanales.setCellValueFactory(new PropertyValueFactory<>(
		"horasSemanales"));

this.colNombre
		.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(
					CellDataFeatures<Empleado, String> p) {
				return p.getValue().getPersonafisica().getPersona()
						.nombreProperty();
			}
		});

this.colNombrePila
		.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(
					CellDataFeatures<Empleado, String> p) {
				return p.getValue().getPersonafisica()
						.nombrePilaProperty();
			}
		});

this.colSexo
		.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(
					CellDataFeatures<Empleado, String> p) {
				return p.getValue().getPersonafisica().sexoProperty();
			}
		});

this.colTelefono
		.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(
					CellDataFeatures<Empleado, String> p) {
				return p.getValue().getPersonafisica().getPersona()
						.telefonoProperty();
			}
		});
	}
	
	public void iniciarItems(){
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});
	}
	
	public void filtrar() {

		String filtrado = textFiltrar.getText().toLowerCase();
		this.listaFiltrada = FXCollections.observableArrayList();
		for (Empleado emp : listaEmpleados) {
			if (emp.getPersonafisica().getApellido().toLowerCase()
					.startsWith(filtrado)) {
				listaFiltrada.add(emp);
			}
			tabla.setItems(listaFiltrada);
		}
	}
	
	
	//GETS Y SETS

	public Stage getDialogStage() {
		return DialogStage;
	}

	public void setDialogStage(Stage dialogStage) {
		DialogStage = dialogStage;
	}

	public VistaPrincipalLiquidacionController getControladorVentanaPrincipal() {
		return controladorVentanaPrincipal;
	}

	public void setControladorVentanaPrincipal(
			VistaPrincipalLiquidacionController controladorVentanaPrincipal) {
		this.controladorVentanaPrincipal = controladorVentanaPrincipal;
	}

	public Empleado getEmpleadoActual() {
		return empleadoActual;
	}

	public void setEmpleadoActual(Empleado empleadoActual) {
		this.empleadoActual = empleadoActual;
	}
	
	
}
