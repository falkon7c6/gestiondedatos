package gestion.gestionFinal.controlador.cliente;

import gestion.gestionFinal.modelo.Cliente;
import gestion.gestionFinal.modelo.Direccion;
import gestion.gestionFinal.modelo.Persona;
import gestion.gestionFinal.persistencia.ClienteDAO;
import gestion.gestionFinal.persistencia.DireccionDAO;
import gestion.gestionFinal.persistencia.PersonaDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.ProvinciasUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import java.sql.SQLException;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Callback;

import javax.annotation.PostConstruct;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@FXMLController(value = "/gestion/gestionFinal/vista/cliente/vistaCliente.fxml")
public class VistaClienteController {

	// variables de uso
	public Stage stage;
	
	private Persona personaActual;
	
	
	private ObservableList<String> provincias;
	private ObservableList<Cliente> clientes;
	private ObservableList<Cliente> filtrados;
	int bandera = -1;

	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView tablaClientes;
	@FXML
	private TableColumn colId;
	@FXML
	private TableColumn colCuilCuit;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TableColumn colTelefono;
	@FXML
	private TableColumn<Cliente, String> colDireccion;
	@FXML
	@ActionTrigger("habilitar abm nuevo")
	private Button btnNuevo;
	@FXML
	@ActionTrigger("habilitar abm editar")
	private Button btnEditar;
	@FXML
	@ActionTrigger("borrar")
	private Button btnEliminar;
	@FXML
	@ActionTrigger("atras")
	private Button btnAtras;
	@FXML
	private AnchorPane panelABM;
	@FXML
	private TextField textCuitCuil;
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textTelefono;
	@FXML
	private TextField textCalle;
	@FXML
	private TextField textNumero;
	@FXML
	private TextField textPiso;
	@FXML
	private TextField textDepartamento;
	@FXML
	private ComboBox comboProvincia;
	@FXML
	private TextField textLocalidad;
	@FXML
	@ActionTrigger("nuevo cliente")
	private Button btnGuardar;
	@FXML
	@ActionTrigger("deshabilitar abm")
	private Button btnCancelar;
	
	//PERSISTENCIA
	private ClienteDAO clienteDao;
	private DireccionDAO dirDAO;
	private PersonaDAO personaDAO;


	@PostConstruct
	public void Initialize() {
		dirDAO = new DireccionDAO();
		personaDAO = new PersonaDAO();
		panelABM.setDisable(true);
		clienteDao = new ClienteDAO();
		//Provincias
		provincias = FXCollections.observableArrayList();
		provincias.addAll(ProvinciasUtil.getProvincias());
		comboProvincia.setItems(provincias);
		//Empleados
		clientes = FXCollections.observableArrayList();
		clientes = clienteDao.listar();
		iniciarColumnas();
		this.tablaClientes.setItems(this.clientes);
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});

		tablaClientes.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 0) {
							if (tablaClientes.getSelectionModel()
									.getSelectedItem() != null) {
								llenarEspacios((Cliente) tablaClientes
										.getSelectionModel().getSelectedItem());
							}
						}
					}
				});
	}

	public void filtrar() {
		
		String filtrado = textFiltrar.getText().toLowerCase();
		this.filtrados = FXCollections.observableArrayList();
		for (Cliente cli : clientes) {
			if (cli.getPersona().getNombre().toLowerCase()
					.startsWith(filtrado)) {
				filtrados.add(cli);
			}
			tablaClientes.setItems(filtrados);
		}
	}

	@ActionMethod("nuevo cliente")
	public void nuevoCliente() {
		try {
			if (this.validar() == true) {
				Cliente clienteTemp = null;
				Persona personaTemp = null;
				Direccion dirTemp = null;
				String cuilViejo = null;
				System.out.println("bandera : " + bandera);
				if (bandera == 0) {
					clienteTemp = new Cliente();
					personaTemp = new Persona();	
					dirTemp = new Direccion();
				}
				if (bandera == 1) {
					clienteTemp = (Cliente) tablaClientes.getSelectionModel()
							.getSelectedItem();
					personaTemp = clienteTemp.getPersona();
					cuilViejo = personaTemp.getCuilcuit();
					dirTemp = clienteTemp.getPersona().getDireccion();
					tablaClientes.setItems(clientes);
				}
				//DIRECCION
				dirTemp.setCalle(textCalle.getText());
				dirTemp.setDepartamento(textDepartamento.getText());
				dirTemp.setLocalidad(textLocalidad.getText());
				dirTemp.setNumero(textNumero.getText());
				dirTemp.setPiso(textPiso.getText());
				int index = this.comboProvincia.getSelectionModel().getSelectedIndex();
				if (index >= 0) {
					dirTemp.setProvincia((String) comboProvincia.getItems().get(index));
				}
				if (bandera == 1) {
					dirDAO.editar(dirTemp);
					
				}else{
				dirTemp.setIdDireccion(dirDAO.insertar(dirTemp));}
				//PERSONA

				personaTemp.setCuilcuit(textCuitCuil.getText());
				personaTemp.setDireccion(dirTemp);
				personaTemp.setDireccionTexto(dirTemp.toString());
				personaTemp.setNombre(textNombre.getText());
				personaTemp.setTelefono(textTelefono.getText());
				personaTemp.setTipo("Juridica");
				if (bandera == 1) {
					personaDAO.editar(personaTemp, cuilViejo);
				}else{
				personaDAO.insertar(personaTemp);}		
				//Cliente

				clienteTemp.setPersona(personaTemp);
				if (this.bandera == 0) {
					int idCliente = clienteDao.insertar(clienteTemp);
					clienteTemp.setIdCliente(idCliente);
					clientes.add(clienteTemp);
				}
				if (bandera == 1) {
					clienteDao.editar(clienteTemp);	
				}
				ObservableList temp = FXCollections.observableArrayList();
				temp.setAll(clientes);
				clientes.clear();
				clientes.setAll(temp);
				this.bandera = -1;
				panelABM.setDisable(true);
			}
		} catch (NumberFormatException e) {
			DialogsUtil.errorDialog("Error", "Error en los campos numericos", e.toString());
		} catch (SQLException e) {
			DialogsUtil.errorDialog("Error", "Error al insertar en la Base de datos", e.toString());
		}
	}

	@ActionMethod("borrar")
	public void borrarCliente() {
		int index;
		index = tablaClientes.getSelectionModel().getSelectedIndex();
		if (index >= 0) {
			try {
				Cliente temp = (Cliente) tablaClientes.getSelectionModel()
						.getSelectedItem();
				clienteDao.borrar(temp.getIdCliente());
				clientes.remove(tablaClientes.getSelectionModel()
						.getSelectedItem());
			} catch (MySQLIntegrityConstraintViolationException e) {
				DialogsUtil.errorDialog("ERROR",
						"No puede eliminar el cliente", e.toString());
			} catch (SQLException e) {
				DialogsUtil.errorDialog("ERROR",
						"Error al conectar a la base de datos", e.toString());
			}
		}
	}

	public void llenarEspacios(Cliente cli) {
		this.comboProvincia.getSelectionModel().select(
				cli.getPersona().getDireccion()
						.getProvincia());
		textCuitCuil.setText(cli
				.getPersona().getCuilcuit());
		this.textNombre
				.setText(cli.getPersona().getNombre());
		this.textTelefono.setText(cli.getPersona()
				.getTelefono());
		this.textCalle.setText(cli.getPersona()
				.getDireccion().getCalle());
		this.textDepartamento.setText(cli.getPersona()
				.getDireccion().getDepartamento());
		this.textNumero.setText(cli.getPersona()
				.getDireccion().getNumero());
		this.textPiso.setText(cli.getPersona()
				.getDireccion().getPiso());
		this.textDepartamento.setText(cli.getPersona()
				.getDireccion().getDepartamento());
		this.textLocalidad.setText(cli.getPersona()
				.getDireccion().getLocalidad());	
		}

	public void iniciarColumnas() {
		this.colId
				.setCellValueFactory(new PropertyValueFactory<>("idCliente"));

		this.colDireccion
				.setCellValueFactory(new Callback<CellDataFeatures<Cliente, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Cliente, String> p) {
						return p.getValue().getPersona()
								.direccionTextoProperty();
					}
				});

		
		this.colCuilCuit
		.setCellValueFactory(new Callback<CellDataFeatures<Cliente, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(
					CellDataFeatures<Cliente, String> p) {
				SimpleStringProperty cuil = new SimpleStringProperty (p
						.getValue().getPersona()
						.getCuilcuit());
				return cuil;
			}
		});

		this.colNombre
				.setCellValueFactory(new Callback<CellDataFeatures<Cliente, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Cliente, String> p) {
						return p.getValue().getPersona()
								.nombreProperty();
					}
				});

		this.colTelefono
				.setCellValueFactory(new Callback<CellDataFeatures<Cliente, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Cliente, String> p) {
						return p.getValue().getPersona()
								.telefonoProperty();
					}
				});
	}
	
	@ActionMethod("deshabilitar abm")
	public void deshabilitarABM() {
		panelABM.setDisable(true);
	}
	

	@ActionMethod("habilitar abm editar")
	public void HabilitarABMEditar() {
		this.bandera = 1;
		panelABM.setDisable(false);
		this.textCuitCuil.setDisable(true);
	}
	
	@ActionMethod("habilitar abm nuevo")
	public void HabilitarABMNuevo() {
		this.bandera = 0;
		panelABM.setDisable(false);
		this.textCuitCuil.setDisable(false);
	}

	public boolean validar() {
		if (Validate.textFieldValidate(textCalle, "Calle")
				&& Validate.ExpresionTextFieldValidate(textCuitCuil, "[0-9]{11}", "Cuil/Cuit", "11 numeros sin espacios")
				&& Validate.textFieldValidate(textDepartamento, "Departamento")				
				&& Validate.textFieldValidate(textLocalidad, "Localidad")
				&& Validate.textFieldValidate(textNombre, "Nombre")
				&& Validate.textFieldNumberValidate(textNumero, "Numero de Departamento")
				&& Validate.textFieldValidate(textPiso, "Piso")
				&& Validate.textFieldValidate(textTelefono, "Telefono")
				&& Validate.comboValidate(comboProvincia, "Provincia")
				) {
			System.out.println("Devuelve true");
			return true;
		}
		DialogsUtil.errorDialog("ERROR", "ERROR",
				"Todos los campos son obligatorios");
		return false;
	}
}
