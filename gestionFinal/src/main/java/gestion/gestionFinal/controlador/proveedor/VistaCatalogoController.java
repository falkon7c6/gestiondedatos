package gestion.gestionFinal.controlador.proveedor;

import gestion.gestionFinal.modelo.Catalogo;
import gestion.gestionFinal.modelo.Categoriaproducto;
import gestion.gestionFinal.modelo.Marcaproducto;
import gestion.gestionFinal.modelo.Producto;
import gestion.gestionFinal.modelo.Proveedor;
import gestion.gestionFinal.persistencia.CatalogoDAO;
import gestion.gestionFinal.persistencia.ProductoDAO;
import gestion.gestionFinal.persistencia.ProveedorDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;

import java.sql.SQLException;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/proveedor/VistaCatalogo.fxml")
public class VistaCatalogoController {
	@FXML
	@ActionTrigger("precio")
	private Button botonPrecio;
	@FXML
	private TableView tablaProductos;
	@FXML
	private TableColumn colId;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TableColumn colDescripcion;
	@FXML
	private TableColumn colCategoria;
	@FXML
	private TableColumn colMarca;
	@FXML
	@ActionTrigger("agregar")
	private Button botonAgregar;
	@FXML
	@ActionTrigger("quitar")
	private Button botonQuitar;
	@FXML
	private TableView tablaCatalogo;
	@FXML
	private TableColumn colId1;
	@FXML
	private TableColumn colNombre1;
	@FXML
	private TableColumn colDescripcion1;
	@FXML
	private TableColumn colCategoria1;
	@FXML
	private TableColumn colMarca1;
	@FXML
	private TableColumn colPrecioVenta1;
	@FXML
	@ActionTrigger("atras")
	private Button botonCerrar;

	@FXMLViewFlowContext
	private ViewFlowContext context;

	private ObservableList<Producto> itemsProducto;
	private ObservableList<Producto> itemsProveedor;
	private Proveedor proveedorActual;
	private ProveedorDAO proveedorDAO;
	private CatalogoDAO catalogoDAO;
	private ProductoDAO productoDAO;

	@PostConstruct
	public void Initialize() {
		itemsProducto = FXCollections.observableArrayList();
		itemsProveedor = FXCollections.observableArrayList();
		proveedorActual = new Proveedor();
		proveedorDAO = new ProveedorDAO();
		productoDAO = new ProductoDAO();
		try {
			proveedorActual = (Proveedor) context.getApplicationContext().getRegisteredObject("proveedor");
			catalogoDAO = new CatalogoDAO();
			itemsProducto = productoDAO.listar();
			itemsProveedor = productoDAO.listar(proveedorActual);
			filtrar();
			iniciarColumnas();
			this.tablaProductos.setItems(itemsProducto);
			this.tablaCatalogo.setItems(itemsProveedor);
		} catch (NullPointerException e) {
			DialogsUtil.messageDialog("", "por favor seleccione un proveedor de la tabla");
			
		}
		
	}

	private void filtrar() {
		for (int i = 0; i < itemsProducto.size(); i++) {
			for (int j = 0; j < itemsProveedor.size(); j++) {
				if(itemsProducto.get(i).getIdProducto() == itemsProveedor.get(j).getIdProducto()) {
					itemsProducto.remove(i);
				}
			}
		}
		
	}

	private void iniciarColumnas() {
		this.colId
				.setCellValueFactory(new PropertyValueFactory<Producto, Integer>(
						"idProducto"));
		this.colDescripcion
				.setCellValueFactory(new PropertyValueFactory<Producto, String>(
						"descripcion"));
		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<Producto, String>(
						"nombre"));
		this.colMarca
				.setCellValueFactory(new PropertyValueFactory<Marcaproducto, String>(
						"marcaproducto"));
		this.colCategoria
				.setCellValueFactory(new PropertyValueFactory<Categoriaproducto, String>(
						"categoriaproducto"));
		this.colId1
		.setCellValueFactory(new PropertyValueFactory<Producto, Integer>(
				"idProducto"));
this.colDescripcion1
		.setCellValueFactory(new PropertyValueFactory<Producto, String>(
				"descripcion"));
this.colNombre1
		.setCellValueFactory(new PropertyValueFactory<Producto, String>(
				"nombre"));
this.colMarca1
		.setCellValueFactory(new PropertyValueFactory<Marcaproducto, String>(
				"marcaproducto"));
this.colCategoria1
		.setCellValueFactory(new PropertyValueFactory<Categoriaproducto, String>(
				"categoriaproducto"));
this.colPrecioVenta1
.setCellValueFactory(new PropertyValueFactory<Categoriaproducto, String>(
		"precioVenta"));

	}

	public float iniciarDialogPrecio() {
		TextInputDialog dialog = new TextInputDialog("0.00");
		dialog.setTitle("");
		dialog.setHeaderText("Precio del producto");
		dialog.setContentText("ingrese la cantidad deseada");

		// Traditional way to get the response value.
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
			try {
				System.out.println("Your name: " + result.get());
				return Float.valueOf(result.get());
			} catch (NumberFormatException e) {
				System.out.println("NOO");
			}
		}else {
			return 0;
		}
		return 0;
	}

	@ActionMethod("agregar")
	public void agregar() {
		if(tablaProductos.getSelectionModel().getSelectedIndex() >= 0) {
			float precio = 0;
			try {
				Producto producto = (Producto) tablaProductos.getSelectionModel().getSelectedItem();
				precio = iniciarDialogPrecio();
				if(precio > 0) {
					producto.setPrecioVenta(precio);
					Catalogo temp = new Catalogo();
					temp.setProducto(producto);
					temp.setProveedor(proveedorActual);
					temp.setPrecio(producto.getPrecioVenta());
					catalogoDAO.insertar(temp);
					itemsProducto.remove(producto);
					itemsProveedor.add(producto);
					tablaProductos.getSelectionModel().clearSelection();
				}
				
			}catch(NumberFormatException e) {
				DialogsUtil.errorDialog("Error", "Error al ingresar precio", "por favor, ingrese solo valores numericos"
						+ ", los decimales"
						+ "van separados por un .");
			
			} catch (SQLException e) {
				DialogsUtil.errorDialog("Error", "Entrada duplicada en la base de datos", e.toString());
			}
			
		}
	}
	@ActionMethod("quitar")
	public void quitar() {
		if(tablaCatalogo.getSelectionModel().getSelectedIndex() >= 0) {
			Producto producto = (Producto) tablaCatalogo.getSelectionModel().getSelectedItem();
			catalogoDAO.borrar(producto.getIdProducto(), proveedorActual.getIdProveedor());
			itemsProveedor.remove(producto);
			itemsProducto.add(producto);
			tablaCatalogo.getSelectionModel().clearSelection();
		}
	}
	@ActionMethod("precio")
	public void modificar() {
		if (Validate.tableNotSelectedItemValidate(tablaCatalogo)) {
			float precio = 0;
			try {
				Producto producto = (Producto) tablaCatalogo
						.getSelectionModel().getSelectedItem();
				precio = iniciarDialogPrecio();
				if (precio > 0) {
					producto.setPrecioVenta(precio);
					Catalogo temp = new Catalogo();
					temp.setProducto(producto);
					temp.setProveedor(proveedorActual);
					temp.setPrecio(producto.getPrecioVenta());
					catalogoDAO.editar(temp);
					tablaCatalogo.getSelectionModel().clearSelection();
				}

			} catch (NumberFormatException e) {
				DialogsUtil.errorDialog("Error", "Error al ingresar precio",
						"por favor, ingrese solo valores numericos"
								+ ", los decimales" + "van separados por un .");

			} catch (SQLException e) {
				DialogsUtil.errorDialog("Error",
						"Entrada duplicada en la base de datos", e.toString());
			}
		}
	}
	
	public void cerrar() {
		
	}

}
