package gestion.gestionFinal.controlador.proveedor;

import gestion.gestionFinal.modelo.Direccion;
import gestion.gestionFinal.modelo.Persona;
import gestion.gestionFinal.modelo.Producto;
import gestion.gestionFinal.modelo.Proveedor;
import gestion.gestionFinal.persistencia.DireccionDAO;
import gestion.gestionFinal.persistencia.PersonaDAO;
import gestion.gestionFinal.persistencia.ProductoDAO;
import gestion.gestionFinal.persistencia.ProveedorDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.ProvinciasUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;

import java.sql.SQLException;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Callback;

import javax.annotation.PostConstruct;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@FXMLController(value = "/gestion/gestionFinal/vista/proveedor/VistaProveedor.fxml")
public class VistaProveedorController {

	// variables de uso
	public Stage stage;
	
	private Persona personaActual;
	
	
	private ObservableList<String> provincias;
	private ObservableList<Proveedor> proveedores;
	private ObservableList<Proveedor> filtrados;
	int bandera = -1;

	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView tablaProveedores;
	@FXML
	private TableColumn colId;
	@FXML
	private TableColumn colCuilCuit;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TableColumn colTelefono;
	@FXML
	private TableColumn<Proveedor, String> colDireccion;
	@FXML
	@ActionTrigger("habilitar abm nuevo")
	private Button btnNuevo;
	@FXML
	@ActionTrigger("habilitar abm editar")
	private Button btnEditar;
	@FXML
	@ActionTrigger("borrar")
	private Button btnEliminar;
	@FXML
	@ActionTrigger("atras")
	private Button btnAtras;
	@FXML
	private AnchorPane panelABM;
	@FXML
	private TextField textCuitCuil;
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textTelefono;
	@FXML
	private TextField textCalle;
	@FXML
	private TextField textNumero;
	@FXML
	private TextField textPiso;
	@FXML
	private TextField textDepartamento;
	@FXML
	private ComboBox comboProvincia;
	@FXML
	private TextField textLocalidad;
	@FXML
	@ActionTrigger("nuevo proveedor")
	private Button btnGuardar;
	@FXML
	@ActionTrigger("cancelar")
	private Button btnCancelar;
	@FXML
	@ActionTrigger("catalogo")
	private Button botonCatalogo;
	@FXML
	private ListView<Producto> listaProductos;
	
	@FXMLViewFlowContext
	private ViewFlowContext context;
	
	//PERSISTENCIA
	private ProveedorDAO proveedorDao;
	private DireccionDAO dirDAO;
	private PersonaDAO personaDAO;
	private ObservableList<Producto> itemsListaCatalogo = FXCollections.observableArrayList();

	@PostConstruct
	public void Initialize() {
		dirDAO = new DireccionDAO();
		personaDAO = new PersonaDAO();
		panelABM.setDisable(true);
		proveedorDao = new ProveedorDAO();
		//Provincias
		provincias = FXCollections.observableArrayList();
		provincias.addAll(ProvinciasUtil.getProvincias());
		comboProvincia.setItems(provincias);
		//Empleados
		proveedores = FXCollections.observableArrayList();
		proveedores = proveedorDao.listar();
		iniciarColumnas();
		this.tablaProveedores.setItems(this.proveedores);
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});

		tablaProveedores.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 0) {
							if (tablaProveedores.getSelectionModel()
									.getSelectedItem() != null) {
								llenarEspacios((Proveedor) tablaProveedores
										.getSelectionModel().getSelectedItem());
							}
						}
					}
				});
		this.listaProductos.setItems(itemsListaCatalogo);
	}

	public void filtrar() {
		
		String filtrado = textFiltrar.getText().toLowerCase();
		this.filtrados = FXCollections.observableArrayList();
		for (Proveedor prov : proveedores) {
			if (prov.getPersona().getNombre().toLowerCase()
					.startsWith(filtrado)) {
				filtrados.add(prov);
			}
			tablaProveedores.setItems(filtrados);
		}
	}

	@ActionMethod("nuevo proveedor")
	public void nuevoProveedor() {
		if (this.validar() == true) {
			try {
				Proveedor proveedorTemp = null;
				Persona personaTemp = null;
				Direccion dirTemp = null;
				String cuilViejo = null;
				System.out.println("bandera : " + bandera);
				if (bandera == 0) {
					proveedorTemp = new Proveedor();
					personaTemp = new Persona();
					dirTemp = new Direccion();
				}
				if (bandera == 1) {
					proveedorTemp = (Proveedor) tablaProveedores
							.getSelectionModel().getSelectedItem();
					personaTemp = proveedorTemp.getPersona();
					cuilViejo = personaTemp.getCuilcuit();
					dirTemp = proveedorTemp.getPersona().getDireccion();
					tablaProveedores.setItems(proveedores);
				}
				// DIRECCION
				dirTemp.setCalle(textCalle.getText());
				dirTemp.setDepartamento(textDepartamento.getText());
				dirTemp.setLocalidad(textLocalidad.getText());
				dirTemp.setNumero(textNumero.getText());
				dirTemp.setPiso(textPiso.getText());
				int index = this.comboProvincia.getSelectionModel()
						.getSelectedIndex();
				if (index >= 0) {
					dirTemp.setProvincia((String) comboProvincia.getItems()
							.get(index));
				}
				if (bandera == 1) {
					dirDAO.editar(dirTemp);

				} else {
					dirTemp.setIdDireccion(dirDAO.insertar(dirTemp));
				}
				// PERSONA

				personaTemp.setCuilcuit(textCuitCuil.getText());
				personaTemp.setDireccion(dirTemp);
				personaTemp.setDireccionTexto(dirTemp.toString());
				personaTemp.setNombre(textNombre.getText());
				personaTemp.setTelefono(textTelefono.getText());
				personaTemp.setTipo("Juridica");
				if (bandera == 1) {
					personaDAO.editar(personaTemp, cuilViejo);
				} else {
					personaDAO.insertar(personaTemp);
				}
				// Cliente

				proveedorTemp.setPersona(personaTemp);
				if (this.bandera == 0) {
					int idProveedor = proveedorDao.insertar(proveedorTemp);
					proveedorTemp.setIdProveedor(idProveedor);
					proveedores.add(proveedorTemp);
				}
				if (bandera == 1) {
					// proveedorDao.editar(proveedorTemp); TODO
				}
				ObservableList temp = FXCollections.observableArrayList();
				temp.setAll(proveedores);
				proveedores.clear();
				proveedores.setAll(temp);
				this.bandera = -1;
				panelABM.setDisable(true);

			} catch (NumberFormatException e) {
				DialogsUtil.errorDialog("ERROR", "Campos numericos",
						"Error en los campos numericos");
			} catch (SQLException e) {
				DialogsUtil.errorDialog("ERROR", "Error en la Base de datos",
						e.toString());
			}
		}
	}

	@ActionMethod("borrar")
	public void borrarProveedor() {
		int index;
		index = tablaProveedores.getSelectionModel().getSelectedIndex();
		if (index >= 0) {
			try {
				Proveedor temp = (Proveedor) tablaProveedores
						.getSelectionModel()
						.getSelectedItem();
				proveedorDao.borrar(temp.getIdProveedor());
				proveedores.remove(tablaProveedores.getSelectionModel()
						.getSelectedItem());
			} catch (MySQLIntegrityConstraintViolationException e) {
				DialogsUtil.errorDialog("Proveedor", "No pudo ser eliminado",
						"el proveedor tiene" + "compras pendientes");
			} catch (SQLException ex) {
				DialogsUtil.errorDialog("error", "Error en la base de datos",
						ex.toString());
			}
		}
	}

	public void llenarEspacios(Proveedor prov) {
		this.comboProvincia.getSelectionModel().select(
				prov.getPersona().getDireccion()
						.getProvincia());
		textCuitCuil.setText(prov
				.getPersona().getCuilcuit());
		this.textNombre
				.setText(prov.getPersona().getNombre());
		this.textTelefono.setText(prov.getPersona()
				.getTelefono());
		this.textCalle.setText(prov.getPersona()
				.getDireccion().getCalle());
		this.textDepartamento.setText(prov.getPersona()
				.getDireccion().getDepartamento());
		this.textNumero.setText(prov.getPersona()
				.getDireccion().getNumero());
		this.textPiso.setText(prov.getPersona()
				.getDireccion().getPiso());
		this.textDepartamento.setText(prov.getPersona()
				.getDireccion().getDepartamento());
		this.textLocalidad.setText(prov.getPersona()
				.getDireccion().getLocalidad());
		
		this.context.getApplicationContext().register("proveedor", prov);
		ProductoDAO tempDao = new ProductoDAO();
		this.listaProductos.setItems(tempDao.listar(prov));
		}
	

	

	public void iniciarColumnas() {
		this.colId
				.setCellValueFactory(new PropertyValueFactory<>("idProveedor"));

		this.colDireccion
				.setCellValueFactory(new Callback<CellDataFeatures<Proveedor, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Proveedor, String> p) {
						return p.getValue().getPersona()
								.direccionTextoProperty();
					}
				});

		
		this.colCuilCuit
		.setCellValueFactory(new Callback<CellDataFeatures<Proveedor, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(
					CellDataFeatures<Proveedor, String> p) {
				SimpleStringProperty cuil = new SimpleStringProperty (p
						.getValue().getPersona()
						.getCuilcuit());
				return cuil;
			}
		});

		this.colNombre
				.setCellValueFactory(new Callback<CellDataFeatures<Proveedor, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Proveedor, String> p) {
						return p.getValue().getPersona()
								.nombreProperty();
					}
				});

		this.colTelefono
				.setCellValueFactory(new Callback<CellDataFeatures<Proveedor, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Proveedor, String> p) {
						return p.getValue().getPersona()
								.telefonoProperty();
					}
				});
	}

	

	@ActionMethod("habilitar abm editar")
	public void HabilitarABMEditar() {
		if (Validate.tableNotSelectedItemValidate(tablaProveedores)) {
			this.bandera = 1;
			panelABM.setDisable(false);
			this.textCuitCuil.setDisable(true);
		}

	}
	
	@ActionMethod("habilitar abm nuevo")
	public void HabilitarABMNuevo() {
		this.bandera = 0;
		panelABM.setDisable(false);
		this.textCuitCuil.setDisable(false);
	}
	
	@ActionMethod("cancelar")
	public void cancelar() {
		this.panelABM.setDisable(true);
	}

	public Boolean validar() {
		if (Validate.textFieldValidate(this.textCalle, "calle")
				&& Validate.textFieldValidate(this.textDepartamento,
						"departamento")
				&& Validate.textFieldValidate(this.textLocalidad, "localidad")
				&& Validate.textFieldValidate(this.textNombre, "nombre")
				&& Validate.textFieldValidate(this.textNumero, "numero")
				&& Validate.textFieldValidate(this.textPiso, "piso")
				&& Validate.textFieldValidate(this.textTelefono, "telefono")
				&& Validate.comboValidate(this.comboProvincia, "provincia")
				&& Validate.ExpresionTextFieldValidate(textCuitCuil,
						"[0-9]{11}", "Cuil/Cuit", "11 numeros sin espacios")) {
			return true;
		}
		return false;
	}

}
