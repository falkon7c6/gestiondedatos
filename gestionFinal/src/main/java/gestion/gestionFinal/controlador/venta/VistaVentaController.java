package gestion.gestionFinal.controlador.venta;

import gestion.gestionFinal.modelo.Cliente;
import gestion.gestionFinal.modelo.Empleado;
import gestion.gestionFinal.modelo.Lineaventa;
import gestion.gestionFinal.modelo.Periodo;
import gestion.gestionFinal.modelo.Venta;
import gestion.gestionFinal.persistencia.ClienteDAO;
import gestion.gestionFinal.persistencia.LineaVentaDAO;
import gestion.gestionFinal.persistencia.ProductoDAO;
import gestion.gestionFinal.persistencia.VentaDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.ReportUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/venta/VistaVenta.fxml")
public class VistaVentaController {
	@FXML
	private TableView tabla;
	@FXML
	private TableColumn colProducto;
	@FXML
	private TableColumn colCantidad;
	@FXML
	private TableColumn colPrecioUnitario;
	@FXML
	private TableColumn colDescuento;
	@FXML
	private TableColumn colBonificacion;
	@FXML
	private TableColumn colRetenciones;
	@FXML
	private TableColumn colPercepciones;
	@FXML
	private TableColumn colIVA;
	@FXML
	private TableColumn colIBB;
	@FXML
	private TableColumn colSubtotal;
	@FXML
	@ActionTrigger("agregar")
	private Button botonAgregar;
	@FXML
	@ActionTrigger("editar")
	private Button botonModificar;
	@FXML
	@ActionTrigger("borrar")
	private Button botonBorrar;
	@FXML
	private ComboBox comboCliente;
	@FXML
	private ComboBox comboIVA;
	@FXML
	private ComboBox comboFactura;
	@FXML
	private TextField textTotal;
	@FXML
	private TextField textMonto;
	@FXML
	private TextField textVuelto;
	@FXML
	private TextField textBonificacion;
	@FXML
	private TextField textComprobante;
	@FXML
	@ActionTrigger("guardar")
	private Button botonGuardar;
	@FXML
	@ActionTrigger("atras")
	private Button botonAtras;
	@FXML
	private Label labelFecha;
	@FXML
	private Label labelHora;
	@FXML
	private Label labelEmpleado;
	@FXML
	private TextArea textAreaComentarios;

	@FXMLViewFlowContext
	private ViewFlowContext context;

	// VARIABLES
	Stage ventanaDialog;
	private Lineaventa lineaNueva;
	private ObservableList<Lineaventa> itemsTabla;
	private ObservableList<Cliente> listaCliente;
	private ObservableList<String> itemsIva;
	// private ObservableList<String> itemsFactura;
	private String tipoFactura;
	private Float total = 0F;
	private ClienteDAO clienteDAO;
	private Calendar fecha = Calendar.getInstance();
	private LocalDate date;
	private LocalTime time;
	private Empleado empleadoActual;
	private Periodo periodo;
	private VentaDAO ventaDAO;
	private LineaVentaDAO lineaDAO;
	private ProductoDAO prodDao;

	@PostConstruct
	public void Initialize() {

		// inicializamos las listas y las llenamos con datos test
		itemsTabla = FXCollections.observableArrayList();
		listaCliente = FXCollections.observableArrayList();
		itemsIva = FXCollections.observableArrayList();
		clienteDAO = new ClienteDAO();
		listaCliente = clienteDAO.listar();
		comboCliente.setItems(listaCliente);
		itemsIva.add("Responsable Inscripto");
		itemsIva.add("monotributista");
		itemsIva.add("consumidor final");
		comboIVA.setItems(itemsIva);
		comboIVA.setDisable(true);
		// evento para limpiar la tabla si cambiamos de proveedor (agregar
		// dialog de yes or no)
		iniciarColumnas();
		this.tabla.setItems(itemsTabla);
		date = LocalDate.now();
		this.labelFecha.setText("" + date.getDayOfMonth() + "-"
				+ date.getMonthValue() + "-" + date.getYear());
		empleadoActual = (Empleado) context.getApplicationContext()
				.getRegisteredObject("empleadoActual");
		periodo = (Periodo) context.getApplicationContext()
				.getRegisteredObject("periodo");
		labelEmpleado.setText(empleadoActual.toString());
		ventaDAO = new VentaDAO();
		lineaDAO = new LineaVentaDAO();
		prodDao = new ProductoDAO();
		botonGuardar.setDisable(true);
		deshabilitarABM();
		textTotal.setEditable(false);
		textVuelto.setEditable(false);
		this.textMonto.setOnKeyReleased(event -> {
			this.calcularTotal();
		});
		this.textBonificacion.setOnKeyReleased(event -> {
			this.calcularTotal();
		});
		
	}

	public void habilitarABM() {
		this.botonAgregar.setDisable(false);
		this.botonBorrar.setDisable(false);
		this.botonModificar.setDisable(false);
	}

	public void deshabilitarABM() {
		this.botonAgregar.setDisable(true);
		this.botonBorrar.setDisable(true);
		this.botonModificar.setDisable(true);
	}

	@ActionMethod("agregar")
	public void agregar() {
		System.out.println("agregar");

		try {
			Cliente clienteSeleccionado = listaCliente.get(comboCliente
					.getSelectionModel().getSelectedIndex());
			context.getApplicationContext().register("clienteSeleccionado",
					clienteSeleccionado);
			String condicionIVA = this.tipoFactura();
			context.getApplicationContext().register("IVA", condicionIVA);
		} catch (Exception ex) {
			System.out.println("error con el combo clientes");
			ex.printStackTrace();
		}
		ventanaDialog = crearDialog(null);
		ventanaDialog.showAndWait();
		if (lineaNueva != null) {
			itemsTabla.add(lineaNueva);
			lineaNueva = null;
			calcularTotal();
			botonGuardar.setDisable(false);
		} else {
			System.out.println("se cancelo");
		}
		ventanaDialog = null;

	}

	public Stage crearDialog(Lineaventa lineaNueva) {
		Stage stage = new Stage();
		AnchorPane root = null;
		try {
			root = FXMLLoader.load(getClass().getResource(
					"/gestion/gestionFinal/vista/venta/VistaLineaVenta.fxml"));
			Flow mainFlow = new Flow(VistaLineaVentaController.class);
			io.datafx.controller.flow.FlowHandler mainFlowHandler = mainFlow
					.createHandler();
			// agrega la animacion
			StackPane pane = mainFlowHandler.start(new AnimatedFlowContainer(
					Duration.millis(320), ContainerAnimations.ZOOM_IN));
			VistaLineaVentaController controladorDialog = (VistaLineaVentaController) mainFlowHandler
					.getCurrentViewContext().getController();
			controladorDialog.setControladorVentanaPrincipal(this);
			root.getChildren().add(pane);
			Scene escena = new Scene(root);
			escena.getStylesheets().add("/styles/NewFile.css");
			stage.setTitle("Nueva Linea de Venta");
			stage.setScene(escena);
			stage.initModality(Modality.APPLICATION_MODAL);
			controladorDialog.setDialogStage(stage);
			// si la linea que le pasas no es nula, edita y no hace nueva
			if (lineaNueva != null) {
				controladorDialog.setLineaActual(lineaNueva);
				controladorDialog.editar();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stage;
	}

	@ActionMethod("borrar")
	public void borrar() {
		System.out.println("borrar");
		int index = tabla.getSelectionModel().getSelectedIndex();
		if (index >= 0) {
			itemsTabla.remove(index);
			calcularTotal();
		}
	}

	@ActionMethod("editar")
	public void editar() {
		System.out.println("editar");
		if (this.tabla.getSelectionModel().getSelectedIndex() >= 0) {
			lineaNueva = (Lineaventa) tabla.getSelectionModel()
					.getSelectedItem();
			ventanaDialog = crearDialog(lineaNueva);
			ventanaDialog.showAndWait();
			lineaNueva = null;
			ventanaDialog = null;
			calcularTotal();
		}
	}


	public void iniciarColumnas() {
		this.colCantidad
				.setCellValueFactory(new PropertyValueFactory<Lineaventa, Integer>(
						"cantidad"));
		this.colPrecioUnitario.setCellValueFactory(new PropertyValueFactory<>(
				"precioUnitario"));
		this.colSubtotal.setCellValueFactory(new PropertyValueFactory<>(
				"subtotal"));
		this.colProducto
				.setCellValueFactory(new Callback<CellDataFeatures<Lineaventa, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Lineaventa, String> p) {
						return new SimpleStringProperty(p.getValue()
								.getProducto().toString());
					}
				});
		this.colDescuento.setCellValueFactory(new PropertyValueFactory<>(
				"descuento"));
		this.colIVA.setCellValueFactory(new PropertyValueFactory<>("iva"));

		comboCliente.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Cliente>() {
					@Override
					public void changed(
							ObservableValue<? extends Cliente> observable,
							Cliente oldValue, Cliente newValue) {
						comboIVA.setDisable(false);
						comboCliente.setDisable(true);
						itemsTabla.clear();

					}
				});
		comboIVA.getSelectionModel().selectedItemProperty()
		.addListener(new ChangeListener<String>() {
			@Override
			public void changed(
					ObservableValue<? extends String> observable,
					String oldValue, String newValue) {
				System.out.println("cambio iva");
						comboIVA.setDisable(true);
				habilitarABM();
				itemsTabla.clear();

			}
		});
	}

	public void calcularTotal() {
		if (itemsTabla.size() > 0) {
			total = 0F;
			for (Lineaventa lineaventa : itemsTabla) {
				total += lineaventa.getSubtotal() - lineaventa.getDescuento();
			}
			if (textBonificacion.getText() != null
					&& !this.textBonificacion.getText().isEmpty()) {
				total = total - Float.valueOf(textBonificacion.getText());
			}
			if (textMonto.getText() != null
					&& !this.textMonto.getText().isEmpty()) {
				Float vuelto = 0F;
				vuelto = Float.valueOf(textMonto.getText()) - total;
				textVuelto.setText("" + vuelto);
			}
			textTotal.setText(Float.toString(total));
		}
	}

	public String tipoFactura() {
		int index = this.comboIVA.getSelectionModel().getSelectedIndex();
		if (index >= 0) {
			try {
				switch (index) {
				case 0:
					System.out.println("factura: A");
					this.comboIVA.setDisable(true);
					return "A";

				case 1:
					System.out.println("factura: C");
					this.comboIVA.setDisable(true);
					return "B";
				case 2:
					System.out.println("factura: Ticket");
					this.comboIVA.setDisable(true);
					return "Ticket";
				}
			} catch (Exception ex) {
				System.out.println("error del combobox");
				ex.printStackTrace();
			}
		}

		return "";

	}

	@ActionMethod("guardar")
	public void guardar() {
		if (validar() == true) {
			try {
				Venta venta = new Venta();
				venta.setTipoFactura(tipoFactura());
				venta.setCondicionIVA((String) comboIVA
						.getSelectionModel().getSelectedItem());
				venta.setFecha(date);
				System.out.println("fecha" + date.toString());
				venta.setNumeroComprobante(Integer.parseInt(textComprobante.getText()));
				venta.setObservaciones(textAreaComentarios.getText());
				tipoFactura = tipoFactura();
				venta.setTipoFactura(tipoFactura);
				venta.setTotal(total);
				venta.setEmpleado(empleadoActual);
				venta.setPeriodo(periodo);
				venta.setBonificacion(Float.parseFloat(textBonificacion.getText()));
				venta.setCliente(listaCliente.get(comboCliente
						.getSelectionModel().getSelectedIndex()));
				venta.setIdVenta(ventaDAO.insertar(venta));
				int idVenta = venta.getIdVenta();
				for (Lineaventa linea : itemsTabla) {
					linea.setVenta(venta);
					int idLinea = lineaDAO.insertar(linea);
					linea.setIdLineaVenta(idLinea);
					int stock = prodDao.obtenerStock(linea.getProducto());
					stock = stock - linea.getCantidad();
					if (stock == 0) {
						prodDao.deshabilitar(linea.getProducto()
								.getIdProducto());
					}
					prodDao.actualizarStock(
							linea.getProducto().getIdProducto(), stock);
				}
				
				//imprime el reporte
				if(DialogsUtil.confirmationDialog("Reporte", "Se va a imprimir el comprobante de venta", 
						"Presione OK para continuar") == true) {
					Map<String, Object> parametrosMap = new HashMap();
		    		parametrosMap.put( "idventa", new Integer(idVenta));	
		    		ReportUtil reporterReportUtil = new ReportUtil();
		    		String reporteNombre = "" + venta.getTipoFactura() +" - " + idVenta + " - " + date + 
		    				" - " + textComprobante.getText() + ".pdf";
					reporterReportUtil.imprimirPDF(
							"/reportes/ventaprueba.jasper",
							parametrosMap,
		    				reporteNombre);
				}
				
				
				
				
				System.out.println("compra factura " + venta.getTipoFactura());
			} catch (Exception e) {
				DialogsUtil.errorDialog("Error", "ERROR", e.toString());
			}
		}
	}

	public boolean validar() {

		if (Validate.textFieldDecimalValidate(textMonto, "Monto")
				&& Validate.textFieldDecimalValidate(textBonificacion,
						"Bonificacion")
				&& Validate.textFieldNumberValidate(textComprobante,
						"Comprobante")
				&& Validate.comboValidate(comboCliente, "Cliente")
				&& Validate.comboValidate(comboIVA, "IVA")
				&& Validate
						.textAreaValidate(textAreaComentarios, "Comentarios", 45)
				&& Validate.tableValidate(tabla, "tabla de lineas venta")

		) {
			for (Lineaventa lineaventa : itemsTabla) {
				if (validarStock(lineaventa) != true) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public boolean validarStock(Lineaventa lineaVenta) {
		int cantidad = 0;
		for (Lineaventa temp : itemsTabla) {
			if (temp.getProducto().getIdProducto() == lineaVenta.getProducto()
					.getIdProducto()) {
				cantidad += temp.getCantidad();
			}
			System.out.println("cantidad " + cantidad);
		}
		if (lineaVenta.getProducto().getStock() >= cantidad) {
			return true;
		}
		DialogsUtil.messageDialog("stock", "el stock de "
				+ lineaVenta.getProducto().getNombre() + " no es suficiente");
		return false;
	}

	// SETS Y GETS QUE A NADIE LE INTERESAN
	public Lineaventa getLineaNueva() {
		return lineaNueva;
	}

	public void setLineaNueva(Lineaventa lineaActual) {
		this.lineaNueva = lineaActual;
	}

	public ObservableList<Cliente> getListaClientes() {
		return listaCliente;
	}

	public void setListaClientes(ObservableList<Cliente> listaClientes) {
		this.listaCliente = listaClientes;
	}

}
