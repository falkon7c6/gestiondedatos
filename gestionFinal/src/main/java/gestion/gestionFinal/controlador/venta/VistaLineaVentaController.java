package gestion.gestionFinal.controlador.venta;

import gestion.gestionFinal.modelo.Lineaventa;
import gestion.gestionFinal.modelo.Producto;
import gestion.gestionFinal.persistencia.ProductoDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/venta/VistaLineaVenta.fxml")
public class VistaLineaVentaController {
	@FXML
	private TableView tabla;
	@FXML
	private TableColumn colDescripcion;
	@FXML
	private TableColumn colMarca;
	@FXML
	private TableColumn colPrecioUnitario;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TextField textBuscar;
	@FXML
	private ComboBox comboBuscar;
	@FXML
	private TextField textCantidad;
	@FXML
	private TextField textDescuento;
	@FXML
	private Label labelSubtotal;

	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;

	@FXMLViewFlowContext
	private ViewFlowContext context;

	private ProductoDAO productoDAO;
	private Stage DialogStage; //
	private VistaVentaController controladorVentanaPrincipal;
	private ObservableList<Producto> itemsComboProductos;
	private ObservableList<Producto> filtrados;
	private Lineaventa lineaActual;
	private String IVA;

	// este metodo se invoca despues del constructor
	@PostConstruct
	public void Initialize() {
		itemsComboProductos = FXCollections.observableArrayList();
		IVA = (String) context.getApplicationContext().getRegisteredObject(
				"IVA");
		productoDAO = new ProductoDAO();
		itemsComboProductos.clear();
		try {
			itemsComboProductos = productoDAO.listarHabilitados();
		} catch (NullPointerException ex) {
			System.out.println("no selecciono ningun proveedor");
		}
		// inicializamos las columnas de la tabla para que se autoactualicen
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<>(
				"descripcion"));
		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<>("nombre"));
		this.colPrecioUnitario.setCellValueFactory(new PropertyValueFactory<>(
				"precioVenta"));
		this.colMarca
				.setCellValueFactory(new Callback<CellDataFeatures<Producto, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Producto, String> p) {
						return new SimpleStringProperty(p.getValue()
								.getMarcaproducto().getNombre());
					}
				});

		// relaciono la tabla con su lista de items
		tabla.setItems(itemsComboProductos);
		// diferencia si es un NUEVO o un MODIFICAR, esto sera eliminado
		if (this.lineaActual == null) {
			lineaActual = new Lineaventa();
		} else {
			tabla.getSelectionModel().select(lineaActual.getProducto());
			textCantidad.setText(Integer.toString(lineaActual.getCantidad()));
		}
		// Evento para autocalcular el subtotal al escribir en el text cantidad
		textCantidad.setOnKeyReleased(event -> {
			try {
				if (tabla.getSelectionModel().getSelectedIndex() >= 0) {
					labelSubtotal.setText(null);
					Float valor = 0F;
					Producto temp = (Producto) tabla.getSelectionModel()
							.getSelectedItem();
					valor = (temp.getPrecioVenta()
							* Integer.parseInt(textCantidad.getText()) - Float.parseFloat(textDescuento.getText()));
					labelSubtotal.setText(valor.toString());

				}
			} catch (NumberFormatException ex) {
				System.out.println("campos vacios");
			}
		});
		
		textDescuento.setOnKeyReleased(event -> {
			try {
				if (tabla.getSelectionModel().getSelectedIndex() >= 0) {
					labelSubtotal.setText(null);
					Float valor = 0F;
					Producto temp = (Producto) tabla.getSelectionModel()
							.getSelectedItem();
					valor = (temp.getPrecioVenta()
							* Integer.parseInt(textCantidad.getText()) - Float.parseFloat(textDescuento.getText()));
					System.out.println("valor: " + valor);
					labelSubtotal.setText(valor.toString());

				}
			} catch (NumberFormatException ex) {
				System.out.println("campos vacios");
			}
		});
		
		// evento para filtrar productos de la tabla
		this.textBuscar.setOnKeyReleased(event -> {
			filtrar();
		});

	}

	@FXML
	// metodo para el boton aceptar
	@ActionMethod("aceptar")
	public void aceptar() {
		if (validar() == true) {
			try {
				if (controladorVentanaPrincipal != null) {
					if (tabla.getSelectionModel().getSelectedIndex() > -1) {
						lineaActual.setProducto((Producto) tabla
								.getSelectionModel().getSelectedItem());
					}
					lineaActual.setCantidad(Integer.parseInt(this.textCantidad
							.getText()));
					lineaActual.setDescuento(Float.parseFloat(this.textDescuento.getText()));
					if (IVA == "A") {

						lineaActual.setPrecioUnitario(lineaActual.getProducto()
								.getPrecioVenta());
						lineaActual
								.setIva(lineaActual.getPrecioUnitario() * 0.21F);
						lineaActual.setSubtotal((lineaActual
								.getPrecioUnitario() + lineaActual.getIva())
								* lineaActual.getCantidad());
					} else {
						lineaActual.setPrecioUnitario(lineaActual.getProducto()
								.getPrecioVenta());
						lineaActual.setSubtotal(lineaActual.getPrecioUnitario()
								* lineaActual.getCantidad());
						lineaActual.setIva(0);
					}
					if(noNegativo() == true) {
						controladorVentanaPrincipal.setLineaNueva(lineaActual);
						lineaActual = null;
						DialogStage.close();
					}
					else {
						DialogsUtil.messageDialog("Linea Negativa", "El descuento es mayor o igual al valor del total");
					}
					

				}
			} catch (NumberFormatException e) {
				System.out.println("error en el campo numerico");
			} catch (NullPointerException e) {
				DialogsUtil
						.messageDialog("No se selecciono producto",
								"Debe seleccionar un Producto de la tabla antes de continuar");
			}
		}
	}

	@FXML
	@ActionMethod("cancelar")
	public void cancelar() {
		DialogStage.close();
	}

	public void editar() {
		tabla.getSelectionModel().select(lineaActual.getProducto());
		textCantidad.setText(Integer.toString(lineaActual.getCantidad()));
	}

	public void filtrar() {
		String filtrado = textBuscar.getText().toLowerCase();
		filtrados = FXCollections.observableArrayList();
		for (Producto prod : itemsComboProductos) {
			if (prod.getNombre().toLowerCase().startsWith(filtrado)) {
				filtrados.add(prod);
			}
			// cambia la lista por la filtrada (la original no se pierde)
			this.tabla.setItems(filtrados);
		}
	}
	
	public boolean noNegativo() {
		Float resta = this.lineaActual.getSubtotal() - lineaActual.getDescuento();
		if( resta > 0) {
			System.out.println(resta);
			return true;
			
		}
		System.out.println("es negativo");
		return false;
	}
	
	public boolean validar() {
		if (Validate.textFieldNumberValidate(textCantidad, "Cantidad")
				&& Validate.textFieldNumberValidate(textDescuento, "Descuento")
				&& Validate.tableNotSelectedItemValidate(tabla)
				&& controlStock()
				) {
			return true;
		}
		return false;
	}

	public boolean controlStock() {
		Producto producto = (Producto) tabla.getSelectionModel()
				.getSelectedItem();
		if (producto.getStock() >= Integer.parseInt(textCantidad.getText())) {
			return true;
		}
		DialogsUtil.messageDialog("Stock insuficiente",
				"El stock actual no es suficiente para la cantidad"
						+ " indicada");
		return false;
	}

	// GETS y SETS
	public Lineaventa getLineaActual() {
		return lineaActual;
	}

	public void setLineaActual(Lineaventa lineaActual) {
		this.lineaActual = lineaActual;
	}

	public Stage getDialogStage() {
		return DialogStage;
	}

	public void setDialogStage(Stage dialogStage) {
		DialogStage = dialogStage;
	}

	public VistaVentaController getControladorVentanaPrincipal() {
		return controladorVentanaPrincipal;
	}

	public void setControladorVentanaPrincipal(
			VistaVentaController controladorVentanaPrincipal) {
		this.controladorVentanaPrincipal = controladorVentanaPrincipal;
	}
}
