package gestion.gestionFinal.controlador.producto;

import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import gestion.gestionFinal.modelo.Categoriaproducto;
import gestion.gestionFinal.modelo.Marcaproducto;
import gestion.gestionFinal.persistencia.CategoriaProductoDAO;
import gestion.gestionFinal.persistencia.MarcaProductoDAO;
import gestion.gestionFinal.persistencia.databaseConnection;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

@FXMLController(value = "/gestion/gestionFinal/vista/producto/DialogMarcaProducto.fxml")
public class DialogMarcaProductoController {
	
	
	@FXML
	private ImageView ImageView;
	@FXML
	private TextField textNombre;
	@FXML
	@ActionTrigger("aceptar")
	private Button btnAceptar;

	public Stage escenario;
	public Categoriaproducto categoria;
	public vistaProductoController vistaProducto;
	private MarcaProductoDAO conexion;
	@FXML
	@ActionTrigger("cerrar")
	private Button btnAtras;

	@PostConstruct
	public void Init() {
		textNombre.setText("marca aqui");
		conexion = new MarcaProductoDAO();
	}

	@ActionMethod("cerrar")
	public void cerrar() {
		escenario.close();
	}

	@ActionMethod("aceptar")
	public void Aceptar() {
		try {
			Marcaproducto marca = new Marcaproducto();
			marca.setNombre(this.textNombre.getText());
			int id;
			id = conexion.insertar(marca);
			marca.setIdMarcaProducto(id);
			this.vistaProducto.marca = marca;
			escenario.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void existe() {
		System.out.println("existe");
	}

}
