package gestion.gestionFinal.controlador.producto;

import gestion.gestionFinal.modelo.Categoriaproducto;
import gestion.gestionFinal.modelo.Marcaproducto;
import gestion.gestionFinal.modelo.Producto;
import gestion.gestionFinal.persistencia.CategoriaProductoDAO;
import gestion.gestionFinal.persistencia.MarcaProductoDAO;
import gestion.gestionFinal.persistencia.ProductoDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;

import java.io.IOException;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/producto/vistaProducto.fxml")
public class vistaProductoController {

	//variables de uso
	public Stage stage;
	public Categoriaproducto categoria = null;
	public Marcaproducto marca = null;
	private ObservableList<Categoriaproducto> listaCategorias;
	private ObservableList<Marcaproducto> listaMarcas;
	private ObservableList<Producto> listaProductos;
	int bandera = -1;

	//componentes de la vista
	@FXML
	AnchorPane panelABM;
	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView<Producto> tablaProductos;
	@FXML
	private TableColumn colId;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TableColumn colDescripcion;
	@FXML
	private TableColumn colCategoria;
	@FXML
	private TableColumn colMarca;
	@FXML
	private TableColumn colStock;
	@FXML
	private TableColumn colPrecioVenta;
	@FXML
	private TableColumn colDeshabilitado;
	
	//componentes con metodos asociados
	@FXML
	@ActionTrigger("habilitar abm nuevo")
	private Button btnNuevo;
	@FXML
	@ActionTrigger("habilitar abm editar")
	private Button btnEditar;
	@FXML
	@ActionTrigger("borrar")
	private Button btnEliminar;
	@FXML
	@ActionTrigger("nuevo producto")
	private Button btnGuardar;
	@FXML
	@ActionTrigger("cancelar")
	private Button btnCancelar;
	@FXML
	@ActionTrigger("nueva categoria")
	private Button btnNuevaCategoria;
	@FXML
	@ActionTrigger("atras")
	private Button btnAtras;
	@FXML
	@ActionTrigger("nueva marca")
	private Button btnNuevaMarca;
	
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textDescripcion;
	@FXML
	private TextField textPrecioVenta;
	@FXML
	private TextField textStock;
	
	@FXML
	private ComboBox<Categoriaproducto> comboCategoria;
	@FXML
	private ComboBox<Marcaproducto> comboMarca;
	
	@FXML
	private CheckBox checkBoxDeshabilitado;

	private ProductoDAO productoDAO;
	//metodo que se carga automaticamente despues del constructor
	//inicia todos los valores, listeners, etc
	@PostConstruct
	public void Initialize() {
		productoDAO = new ProductoDAO();
		listaCategorias = FXCollections.observableArrayList();
		CategoriaProductoDAO categoriaDAO = new CategoriaProductoDAO();
		listaCategorias = categoriaDAO.listar();
		comboCategoria.setItems(listaCategorias);
		listaMarcas = FXCollections.observableArrayList();
		MarcaProductoDAO marcaDAO = new MarcaProductoDAO();
		listaMarcas = marcaDAO.listar();
		comboMarca.setItems(listaMarcas);
		listaProductos = FXCollections.observableArrayList();


		this.colId
				.setCellValueFactory(new PropertyValueFactory<Producto, Integer>(
						"idProducto"));
		this.colDescripcion
				.setCellValueFactory(new PropertyValueFactory<Producto, String>(
						"descripcion"));
		this.colDeshabilitado
				.setCellValueFactory(new PropertyValueFactory<Producto, Integer>(
						"deshabilitado"));
		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<Producto, String>(
						"nombre"));
		this.colStock
				.setCellValueFactory(new PropertyValueFactory<Producto, Integer>(
						"stock"));
		this.colPrecioVenta
				.setCellValueFactory(new PropertyValueFactory<Producto, Float>(
						"precioVenta"));
		this.colMarca
				.setCellValueFactory(new PropertyValueFactory<Marcaproducto, String>(
						"marcaproducto"));
		this.colCategoria
				.setCellValueFactory(new PropertyValueFactory<Categoriaproducto, String>(
						"categoriaproducto"));
		listaProductos = productoDAO.listar();
		tablaProductos.setItems(listaProductos);
		
		this.textFiltrar.setOnKeyReleased(event -> {
			this.filtrar();
		});

		this.tablaProductos.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 1) {
							if (tablaProductos.getSelectionModel()
									.getSelectedItem() != null) {
								llenarEspacios(tablaProductos
										.getSelectionModel().getSelectedItem());
							}
						}

					}

				});
	}

	//habilita el panel de abajo para nuevo prod
	@ActionMethod("habilitar abm nuevo")
	public void HabilitarABMNuevo() {
		this.bandera = 0;
		panelABM.setDisable(false);
	}

	//habilita para editar
	@ActionMethod("habilitar abm editar")
	public void HabilitarABMEditar() {
		this.bandera = 1;
		panelABM.setDisable(false);
	}

	//metodo para filtrar un producto
	public void filtrar() {
		System.out.println("ingreso: " + textFiltrar.getText());
		String filtrado = this.textFiltrar.getText();
		System.out.println(filtrado);
		ObservableList<Producto> listaFiltrada = FXCollections
				.observableArrayList();
		for (Producto prod : listaProductos) {
			if (prod.getNombre().startsWith(filtrado)) {
				listaFiltrada.add(prod);
			}
			this.tablaProductos.setItems(listaFiltrada);
		}
	}

	//instancia una ventana modal para crear una categoria de manera rapida
	@ActionMethod("nueva categoria")
	public void nuevaCategoria() {
		// ventana donde se abrira el dialog
		stage = new Stage();
		AnchorPane root = null;
		try {
			// cargamos los elementos del dialog en un nodo
			root = FXMLLoader
					.load(getClass()
							.getResource(
									"/gestion/gestionFinal/vista/producto/DialogCategoriaProducto.fxml"));
			// definimos el flujo y su animacion
			Flow mainFlow = new Flow(DialogCategoriaProductoController.class);
			FlowHandler mainFlowHandler = mainFlow.createHandler();
			StackPane pane = mainFlowHandler.start(new AnimatedFlowContainer(
					Duration.millis(320), ContainerAnimations.ZOOM_IN));
			// obtenemos una referencia al controlador del dialog
			DialogCategoriaProductoController controlador = (DialogCategoriaProductoController) mainFlowHandler
					.getCurrentViewContext().getController();
			// pasamos una referencia de este controlador al controlador del
			// dialog
			controlador.vistaProducto = this;
			// ponemos un titulo al dialog y etc etc etc
			root.getChildren().add(pane);
			Scene scene = new Scene(root);
			scene.getStylesheets().add("/styles/NewFile.css");
			stage.setTitle("Categoria de Productos");
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			controlador.escenario = stage;
			// detenemos la ejecucion de la ventana principal hasta que se
			// termine de ejecutar el dialog
			stage.showAndWait();
			// comprobamos que se haya creado una categoria y la manipulamos
			if (categoria != null) {
				listaCategorias.add(categoria);
				categoria = null;
			} else {
				System.out.println("categoria nula");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (FlowException e) {
			e.printStackTrace();
		}

	}
	
	@ActionMethod("nueva marca")
	public void nuevaMarca() {
		// ventana donde se abrira el dialog
		stage = new Stage();
		AnchorPane root = null;
		try {
			// cargamos los elementos del dialog en un nodo
			root = FXMLLoader
					.load(getClass()
							.getResource(
									"/gestion/gestionFinal/vista/producto/DialogMarcaProducto.fxml"));
			// definimos el flujo y su animacion
			Flow mainFlow = new Flow(DialogMarcaProductoController.class);
			FlowHandler mainFlowHandler = mainFlow.createHandler();
			StackPane pane = mainFlowHandler.start(new AnimatedFlowContainer(
					Duration.millis(320), ContainerAnimations.ZOOM_IN));
			// obtenemos una referencia al controlador del dialog
			DialogMarcaProductoController controlador = (DialogMarcaProductoController) mainFlowHandler
					.getCurrentViewContext().getController();
			// pasamos una referencia de este controlador al controlador del
			// dialog
			controlador.vistaProducto = this;
			// ponemos un titulo al dialog y etc etc etc
			root.getChildren().add(pane);
			Scene scene = new Scene(root);
			scene.getStylesheets().add("/styles/NewFile.css");
			stage.setTitle("Marca de Productos");
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			controlador.escenario = stage;
			// detenemos la ejecucion de la ventana principal hasta que se
			// termine de ejecutar el dialog
			stage.showAndWait();
			// comprobamos que se haya creado una categoria y la manipulamos
			if (marca != null) {
				listaMarcas.add(marca);
				marca = null;
			} else {
				System.out.println("marca nula");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (FlowException e) {
			e.printStackTrace();
		}

	}

	//crea o edita un prodicto
	@ActionMethod("nuevo producto")
	public void nuevoProducto() {
		try {
			if(validar() == true) {
				Producto productoTemp = null;
				if (bandera == 0) {
					productoTemp = new Producto();
					
				}
				if (bandera == 1) {
					productoTemp = this.tablaProductos.getSelectionModel()
							.getSelectedItem();
				}
				productoTemp.setDescripcion(this.textDescripcion.getText());
				if (this.checkBoxDeshabilitado.selectedProperty().get() == true) {
					productoTemp.setDeshabilitado("1");
				} else {
					productoTemp.setDeshabilitado("0");
				}
				productoTemp.setNombre(this.textNombre.getText());
				productoTemp.setStock(Integer.parseInt(this.textStock
						.getText()));
				productoTemp.setPrecioVenta(Float.parseFloat(this.textPrecioVenta
						.getText()));
				int index = this.comboCategoria.getSelectionModel().getSelectedIndex();
				//if (index >= 0) {
					productoTemp.setCategoriaproducto(comboCategoria.getItems().get(
							index));
				//}
				index = comboMarca.getSelectionModel().getSelectedIndex();
				System.out.println("indice " + index);
				if (index >= 0) {
					productoTemp.setMarcaproducto(comboMarca.getItems().get(index));
				}
				if (this.bandera == 0) {
					int id  = productoDAO.insertar(productoTemp);
					productoTemp.setIdProducto(id);
					listaProductos.add(productoTemp);
				}
				if(bandera == 1){
					productoDAO.editar(productoTemp);
					
				}
				this.bandera = -1;
				panelABM.setDisable(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//borra un producto
	@ActionMethod("borrar")
	public void borrarProducto() {
		int index;
		index = tablaProductos.getSelectionModel().getSelectedIndex();
		if (index >= 0) {
			try {
				productoDAO.borrar(tablaProductos.getSelectionModel().getSelectedItem().getIdProducto());
				listaProductos.remove(tablaProductos
						.getSelectionModel().getSelectedItem());
			} catch (SQLException e) {
				DialogsUtil.errorDialog("ERROR SQL", "Error en la base de datos", "No se pudo borrar el producto");
			}
		}
	}

	//llena los espacios de abajo cada vez que se selecciona un item de la tabla
	public void llenarEspacios(Producto prod) {
		this.textDescripcion.setText(prod.getDescripcion());
		this.textNombre.setText(prod.getNombre());
		this.textStock.setText(String.valueOf(prod.getStock()));
		this.textPrecioVenta.setText(String.valueOf(prod.getPrecioVenta()));
		this.comboCategoria.getSelectionModel().select(prod.getCategoriaproducto());
		this.comboMarca.getSelectionModel().select(prod.getMarcaproducto());
		if (prod.getDeshabilitado().charAt(0) == 1) {
			this.checkBoxDeshabilitado.selectedProperty().set(true);
		} else {
			this.checkBoxDeshabilitado.selectedProperty().set(false);
		}
	}
	
	//cancela editar o nuevo
	@ActionMethod("cancelar")
	public void cancelar(){
		this.panelABM.setDisable(true);
		this.bandera = -1;
	}
	
	public boolean validar() {
		if (Validate.textFieldValidate(textDescripcion, "Descripcion") &&
				Validate.textFieldValidate(textNombre, "Nombre") &&
				Validate.textFieldDecimalValidate(textPrecioVenta, "Precio de Venta") &&
				Validate.textFieldNumberValidate(textStock, "Stock") &&
				Validate.comboValidate(comboCategoria, "Categoria del Producto") &&
				Validate.comboValidate(comboMarca, "Marca del Producto")
				) {
			return true;
		}
		return false;
	}
}