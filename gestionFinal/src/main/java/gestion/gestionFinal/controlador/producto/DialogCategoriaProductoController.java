package gestion.gestionFinal.controlador.producto;

import gestion.gestionFinal.modelo.Categoriaproducto;
import gestion.gestionFinal.persistencia.CategoriaProductoDAO;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import java.sql.SQLException;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import javax.annotation.PostConstruct;

@FXMLController(value="/gestion/gestionFinal/vista/producto/DialogCategoriaProducto.fxml")
public class DialogCategoriaProductoController {
	
	
	@FXML
	private ImageView ImageView;
	@FXML
	private TextField textNombre;
	
	@FXML
	@ActionTrigger("aceptar")
	private Button btnAceptar;
	
	public Stage escenario;
	public Categoriaproducto categoria;
	public vistaProductoController vistaProducto;
	private CategoriaProductoDAO conexion;
	
	@FXML
	@ActionTrigger("cerrar")
	private Button btnAtras;

	@PostConstruct
	public void Init(){
		textNombre.setText("nombre de la categoria");
		conexion = new CategoriaProductoDAO();
	}
	
	@ActionMethod("cerrar")
	public void cerrar(){
		escenario.close();
	}
	
	@ActionMethod("aceptar")
	public void Aceptar(){
		
		try {
			Categoriaproducto categoria = new Categoriaproducto();
			categoria.setNombre(this.textNombre.getText());
			int id;
			id = conexion.insertar(categoria);
			categoria.setIdCategoriaProducto(id);
			vistaProducto.categoria = categoria;
			escenario.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void existe(){
		System.out.println("existe");
	}
	
	
}
