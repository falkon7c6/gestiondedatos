package gestion.gestionFinal.controlador.persona;

import gestion.gestionFinal.modelo.Calificacionprofesional;
import gestion.gestionFinal.modelo.Categoriaempleado;
import gestion.gestionFinal.modelo.Categoriaproducto;
import gestion.gestionFinal.modelo.Direccion;
import gestion.gestionFinal.modelo.Empleado;
import gestion.gestionFinal.modelo.Marcaproducto;
import gestion.gestionFinal.modelo.Obrasocial;
import gestion.gestionFinal.modelo.Persona;
import gestion.gestionFinal.modelo.Personafisica;
import gestion.gestionFinal.persistencia.CalificacionProfesionalDAO;
import gestion.gestionFinal.persistencia.CategoriaEmpleadoDAO;
import gestion.gestionFinal.persistencia.DireccionDAO;
import gestion.gestionFinal.persistencia.EmpleadoDAO;
import gestion.gestionFinal.persistencia.ObraSocialDAO;
import gestion.gestionFinal.persistencia.PersonaDAO;
import gestion.gestionFinal.persistencia.PersonaFisicaDAO;
import gestion.gestionFinal.persistencia.UsuarioDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.ProvinciasUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;

import java.sql.SQLException;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Callback;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/persona/VistaPrincipalEmpleado.fxml")
public class VistaPrincipalEmpleadoController {

	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView tablaEmpleados;
	@FXML
	private TableColumn colLegajo;
	@FXML
	private TableColumn colCuilCuit;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TableColumn colApellido;
	@FXML
	private TableColumn colTelefono;
	@FXML
	private TableColumn<Empleado, String> colDireccion;
	@FXML
	private TableColumn colSexo;
	@FXML
	private TableColumn colNombrePila;

	@FXML
	private TableColumn colConyuge;
	@FXML
	private TableColumn colActivo;
	@FXML
	private TableColumn colFechaIngreso;
	@FXML
	private TableColumn colHorasSemanales;
	@FXML
	@ActionTrigger("habilitar abm nuevo")
	private Button btnNuevo;
	@FXML
	@ActionTrigger("habilitar abm editar")
	private Button btnEditar;
	@FXML
	@ActionTrigger("borrar")
	private Button btnEliminar;
	@FXML
	@ActionTrigger("atras")
	private Button btnAtras;
	@FXML
	private AnchorPane panelABM;
	@FXML
	private TextField textCuitCuil;
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textApellido;
	@FXML
	private TextField textNombrePila;
	@FXML
	private ComboBox comboSexo;
	@FXML
	private TextField textTelefono;
	@FXML
	private TextField textCalle;
	@FXML
	private TextField textNumero;
	@FXML
	private TextField textPiso;
	@FXML
	private TextField textDepartamento;
	@FXML
	private ComboBox comboProvincia;
	@FXML
	private TextField textLocalidad;
	@FXML
	private DatePicker dateFechaIngreso;
	@FXML
	private TextField textHorasSemanales;
	@FXML
	private CheckBox checkBoxActivo;
	@FXML
	private ListView listGrupoFamiliar;
	@FXML
	@ActionTrigger("agregar familiar")
	private Button btnAgregarFlia;
	@FXML
	private ComboBox<Categoriaempleado> comboCategoria;
	@FXML
	private ComboBox comboObraSocial;
	@FXML
	private ComboBox comboCalificacionProfesional;
	@FXML
	@ActionTrigger("nuevo empleado")
	private Button btnGuardar;
	@FXML
	@ActionTrigger("deshabilitar abm")
	private Button btnCancelar;
	@FXMLViewFlowContext
	private ViewFlowContext context;

	@FXML
	private ComboBox<String> comboRol;

	// variables de uso
	public Stage stage;
	public Categoriaproducto categoria = null;
	public Marcaproducto marca = null;
	private Persona personaActual;
	private Persona conyuge;
	private Personafisica personaFisicaActual;
	private ObservableList<String> provincias;
	private ObservableList<String> sexos;
	private ObservableList<String> roles;
	private ObservableList<Personafisica> familia;
	private ObservableList<Personafisica> hijos;
	private ObservableList<Categoriaempleado> categorias;
	private ObservableList<Obrasocial> obrasSociales;
	private ObservableList<Calificacionprofesional> calificacionesprofesionales;
	private ObservableList<Empleado> empleados;
	private ObservableList<Empleado> filtrados;
	int bandera = -1;

	// PERSISTENCIA
	private EmpleadoDAO empleadoDao;
	private DireccionDAO dirDAO;
	private PersonaDAO personaDAO;
	private PersonaFisicaDAO personaFisDAO;
	private UsuarioDAO usuarioDAO;

	@PostConstruct
	public void Initialize() {
		dirDAO = new DireccionDAO();
		personaDAO = new PersonaDAO();
		personaFisDAO = new PersonaFisicaDAO();
		panelABM.setDisable(true);
		empleadoDao = new EmpleadoDAO();
		usuarioDAO = new UsuarioDAO();
		// Categorias
		categorias = FXCollections.observableArrayList();
		CategoriaEmpleadoDAO categoriasDAO = new CategoriaEmpleadoDAO();
		categorias = categoriasDAO.listar();
		comboCategoria.setItems(categorias);
		// Provincias
		provincias = FXCollections.observableArrayList();
		provincias.addAll(ProvinciasUtil.getProvincias());
		comboProvincia.setItems(provincias);
		// Sexos
		sexos = FXCollections.observableArrayList();
		sexos.addAll("MASCULINO", "FEMENINO", "OTRO");
		comboSexo.setItems(sexos);
		// Roles
		roles = FXCollections.observableArrayList();
		roles.addAll("administracion", "compraventa", "liquidacion");
		comboRol.setItems(roles);
		// Grupo Familiar
		familia = FXCollections.observableArrayList();
		hijos = FXCollections.observableArrayList();
		// Obra Social
		obrasSociales = FXCollections.observableArrayList();
		ObraSocialDAO obraSocialDao = new ObraSocialDAO();
		obrasSociales = obraSocialDao.listar();
		comboObraSocial.setItems(obrasSociales);
		// Calificacion Profesional
		calificacionesprofesionales = FXCollections.observableArrayList();
		CalificacionProfesionalDAO calificacionProfDao = new CalificacionProfesionalDAO();
		calificacionesprofesionales = calificacionProfDao.listar();
		comboCalificacionProfesional.setItems(calificacionesprofesionales);
		// Empleados
		empleados = FXCollections.observableArrayList();
		empleados = empleadoDao.listar();
		try {
			iniciarColumnas();
		} catch (NullPointerException e) {
			System.out.println("null pointer");
		}

		this.tablaEmpleados.setItems(this.empleados);
		this.btnAgregarFlia.setDisable(true);
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});

		tablaEmpleados.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 1) {
							if (tablaEmpleados.getSelectionModel()
									.getSelectedItem() != null) {
								llenarEspacios((Empleado) tablaEmpleados
										.getSelectionModel().getSelectedItem());
								llenarListaGrupoFamiliar((Empleado) tablaEmpleados
										.getSelectionModel().getSelectedItem());
							}
						}
					}
				});
	}

	public void llenarListaGrupoFamiliar(Empleado emp) {
		familia.clear();
		familia = empleadoDao.listarFamilia(emp);
		listGrupoFamiliar.setItems(familia);
	}

	public void filtrar() {

		String filtrado = textFiltrar.getText().toLowerCase();
		this.filtrados = FXCollections.observableArrayList();
		for (Empleado emp : empleados) {
			if (emp.getPersonafisica().getApellido().toLowerCase()
					.startsWith(filtrado)) {
				filtrados.add(emp);
			}
			tablaEmpleados.setItems(filtrados);
		}
	}

	@ActionMethod("nuevo empleado")
	public void nuevoEmpleado() {
		if (this.validar() == true) {
				try {
					Empleado empleadoTemp = null;
					Persona personaTemp = null;
					Personafisica personaFisTemp = null;
					Direccion dirTemp = null;
					String cuilViejo = null;
					if (bandera == 1) {
						empleadoTemp = (Empleado) this.tablaEmpleados
								.getSelectionModel().getSelectedItem();
						personaTemp = empleadoTemp.getPersonafisica()
								.getPersona();
						cuilViejo = personaTemp.getCuilcuit();
						personaFisTemp = empleadoTemp.getPersonafisica();
						dirTemp = empleadoTemp.getPersonafisica().getPersona()
								.getDireccion();
						tablaEmpleados.setItems(empleados);
					} else {
						empleadoTemp = new Empleado();
						personaTemp = new Persona();
						personaFisTemp = new Personafisica();
						dirTemp = new Direccion();
					}
					// DIRECCION
					dirTemp.setCalle(textCalle.getText());
					dirTemp.setDepartamento(textDepartamento.getText());
					dirTemp.setLocalidad(textLocalidad.getText());
					dirTemp.setNumero(textNumero.getText());
					dirTemp.setPiso(textPiso.getText());
					int index = this.comboProvincia.getSelectionModel()
							.getSelectedIndex();
					if (index >= 0) {
						dirTemp.setProvincia((String) comboProvincia.getItems()
								.get(index));
					}
					if (bandera == 1) {
						dirDAO.editar(dirTemp);

					} else {
						dirTemp.setIdDireccion(dirDAO.insertar(dirTemp));
					}
					// PERSONA

					personaTemp.setCuilcuit(textCuitCuil.getText());
					personaTemp.setDireccion(dirTemp);
					personaTemp.setDireccionTexto(dirTemp.toString());
					personaTemp.setNombre(textNombre.getText());
					personaTemp.setTelefono(textTelefono.getText());
					personaTemp.setTipo("Fisica");
					if (bandera == 1) {
						personaDAO.editar(personaTemp, cuilViejo);
					} else {
						personaDAO.insertar(personaTemp);
					}

					// PERSONA FISICA

					personaFisTemp.setPersona(personaTemp);
					personaFisTemp.setApellido(textApellido.getText());
					personaFisTemp.setNombrePila(textNombrePila.getText());
					index = comboSexo.getSelectionModel().getSelectedIndex();
					if (index >= 0) {
						personaFisTemp.setSexo((String) comboSexo.getItems()
								.get(index));
					}
					if (bandera == 1) {
						this.personaFisDAO.editar(personaFisTemp);

					} else {
						int idPerFis = personaFisDAO.insertar(personaFisTemp);
						personaFisTemp.setIdPersonaFisica(idPerFis);
					}
					// EMPLEADO
					empleadoTemp.setPersonafisica(personaFisTemp);
					index = comboCalificacionProfesional.getSelectionModel()
							.getSelectedIndex();
					if (index >= 0) {
						empleadoTemp
								.setCalificacionprofesional((Calificacionprofesional) comboCalificacionProfesional
										.getItems().get(index));
					}
					index = comboCategoria.getSelectionModel()
							.getSelectedIndex();
					if (index >= 0) {
						empleadoTemp
								.setCategoriaempleado((Categoriaempleado) comboCategoria
										.getItems().get(index));
					}
					empleadoTemp.setFechaIngreso(dateFechaIngreso.getValue());
					empleadoTemp.setHorasSemanales(Integer
							.parseInt(textHorasSemanales.getText()));
					index = comboObraSocial.getSelectionModel()
							.getSelectedIndex();
					if (index >= 0) {
						empleadoTemp.setObrasocial((Obrasocial) comboObraSocial
								.getItems().get(index));
					}
					if (this.checkBoxActivo.selectedProperty().get() == true) {
						empleadoTemp.setActivo(1);
					} else {
						empleadoTemp.setActivo(0);
					}
					if (this.bandera == 0) {
						int legajo = empleadoDao.insertar(empleadoTemp);
						empleadoTemp.setLegajo(legajo);
						empleados.add(empleadoTemp);
						this.AltaUsuario(legajo);
					}
					if (bandera == 1) {
						empleadoDao.editar(empleadoTemp);
						this.EditarUsuario(empleadoTemp.getLegajo());
					}
					ObservableList temp = FXCollections.observableArrayList();
					temp.setAll(empleados);
					empleados.clear();
					empleados.setAll(temp);
					this.bandera = -1;
					panelABM.setDisable(true);
				} catch (NumberFormatException e) {
					DialogsUtil.errorDialog("ERROR", "Campos numericos",
							"Error en los campos numericos");
				} catch (NullPointerException ex) {
					DialogsUtil
							.errorDialog("ERROR", "CUIL/CUIT", ex.toString());
					ex.printStackTrace();
				} catch (SQLException e) {
					DialogsUtil.errorDialog("ERROR",
							"Error en la Base de datos", e.toString());
				}
			}


	}

	@ActionMethod("borrar")
	public void borrarEmpleado() {
		int index;
		index = tablaEmpleados.getSelectionModel().getSelectedIndex();
		if (index >= 0) {
			try {
				Empleado temp = (Empleado) tablaEmpleados.getSelectionModel()
						.getSelectedItem();
				this.empleadoDao.borrarLogico(temp.getLegajo());
				this.usuarioDAO.bajaLogicaUsuario(String.valueOf(temp
						.getLegajo()));
				empleados.remove(tablaEmpleados.getSelectionModel()
						.getSelectedItem());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void llenarEspacios(Empleado emp) { // AQUI SE SELECCIONA EL EMPLEADO
												// PARA EL GRUPO FAMILIAR
		this.hijos = empleadoDao.listarHijos(emp);
		this.textHorasSemanales
				.setText(String.valueOf(emp.getHorasSemanales()));
		this.dateFechaIngreso.setValue(emp.getFechaIngreso());
		this.comboCalificacionProfesional.getSelectionModel().select(
				emp.getCalificacionprofesional());
		this.comboCategoria.getSelectionModel().select(
				emp.getCategoriaempleado());
		this.comboObraSocial.getSelectionModel().select(emp.getObrasocial());
		this.comboProvincia.getSelectionModel().select(
				emp.getPersonafisica().getPersona().getDireccion()
						.getProvincia());
		this.comboSexo.getSelectionModel().select(
				emp.getPersonafisica().getSexo());
		this.textCuitCuil.setText(String.valueOf(emp.getPersonafisica()
				.getPersona().getCuilcuit()));
		this.textApellido.setText(emp.getPersonafisica().getApellido());
		this.textNombrePila.setText(emp.getPersonafisica().getNombrePila());
		this.textNombre
				.setText(emp.getPersonafisica().getPersona().getNombre());
		this.textTelefono.setText(emp.getPersonafisica().getPersona()
				.getTelefono());
		this.textCalle.setText(emp.getPersonafisica().getPersona()
				.getDireccion().getCalle());
		this.textDepartamento.setText(emp.getPersonafisica().getPersona()
				.getDireccion().getDepartamento());
		this.textNumero.setText(emp.getPersonafisica().getPersona()
				.getDireccion().getNumero());
		this.textPiso.setText(emp.getPersonafisica().getPersona()
				.getDireccion().getPiso());
		this.textDepartamento.setText(emp.getPersonafisica().getPersona()
				.getDireccion().getDepartamento());
		this.textLocalidad.setText(emp.getPersonafisica().getPersona()
				.getDireccion().getLocalidad());
		if (emp.getActivo() == 1) {
			this.checkBoxActivo.selectedProperty().set(true);
		} else {
			this.checkBoxActivo.selectedProperty().set(false);
		}

		this.btnAgregarFlia.setDisable(false);
		context.getApplicationContext().register("padre", emp);
	}

	@ActionMethod("quitar familiar")
	public void quitarFamiliar() {
		int index = -1;
		index = listGrupoFamiliar.getSelectionModel().getSelectedIndex();
		if (index >= 0) {
			Empleado emp = (Empleado) tablaEmpleados.getSelectionModel()
					.getSelectedItem();
		}
		familia.remove(index);
	}

	public void iniciarColumnas() {
		this.colLegajo
				.setCellValueFactory(new PropertyValueFactory<>("legajo"));
		this.colActivo
				.setCellValueFactory(new PropertyValueFactory<>("activo"));
		this.colDireccion
				.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Empleado, String> p) {
						return p.getValue().getPersonafisica().getPersona()
								.direccionTextoProperty();
					}
				});

		this.colConyuge
				.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Empleado, String> p) {
						if (p.getValue().getConyuge() != null) {
							return new SimpleStringProperty(p.getValue()
									.getConyuge().getNombrePila());
						}
						return new SimpleStringProperty("NO");
					}
				});

		this.colCuilCuit
				.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Empleado, String> p) {
						SimpleStringProperty cuil = new SimpleStringProperty(p
								.getValue().getPersonafisica().getPersona()
								.getCuilcuit());
						return cuil;
					}
				});

		this.colApellido
				.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Empleado, String> p) {
						return p.getValue().getPersonafisica()
								.apellidoProperty();
					}
				});
		this.colFechaIngreso.setCellValueFactory(new PropertyValueFactory<>(
				"fechaIngreso"));


		this.colHorasSemanales.setCellValueFactory(new PropertyValueFactory<>(
				"horasSemanales"));

		this.colNombre
				.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Empleado, String> p) {
						return p.getValue().getPersonafisica().getPersona()
								.nombreProperty();
					}
				});

		this.colNombrePila
				.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Empleado, String> p) {
						return p.getValue().getPersonafisica()
								.nombrePilaProperty();
					}
				});

		this.colSexo
				.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Empleado, String> p) {
						return p.getValue().getPersonafisica().sexoProperty();
					}
				});

		this.colTelefono
				.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Empleado, String> p) {
						return p.getValue().getPersonafisica().getPersona()
								.telefonoProperty();
					}
				});
	}

	@ActionMethod("deshabilitar abm")
	public void deshabilitarABM() {
		this.panelABM.setDisable(true);
	}

	@ActionMethod("habilitar abm editar")
	public void HabilitarABMEditar() {
		this.bandera = 1;
		panelABM.setDisable(false);
	}

	@ActionMethod("habilitar abm nuevo")
	public void HabilitarABMNuevo() {
		this.bandera = 0;
		panelABM.setDisable(false);
	}

	public boolean comprobarUsuario() {
		if (Validate.comboValidate(comboRol, "Roles") == true) {
			return true;
		}
		DialogsUtil.messageDialog("Usuario",
						"No selecciono un rol o los passwords no coinciden o estan vacios");
		return false;
	}


	public void AltaUsuario(int id) {
		if (comprobarUsuario() == true) {
			try {
				usuarioDAO.insertar(String.valueOf(id), "1234");
				usuarioDAO.otorgarPrivilegios(String.valueOf(id), comboRol
						.getSelectionModel().getSelectedItem());
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	public void EditarUsuario(int id) {
		if (comprobarUsuario() == true) {
			try {
				usuarioDAO.otorgarPrivilegios(String.valueOf(id), comboRol
						.getSelectionModel().getSelectedItem());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean validar() {
		if (Validate.textFieldValidate(textCalle, "Calle")
				&& Validate.textFieldValidate(textApellido, "Apellido")
				// && this.textCuitCuil.getText().length() == 11
				&& Validate.ExpresionTextFieldValidate(textCuitCuil,
						"[0-9]{11}", "Cuil/Cuit", "11 numeros sin espacios")
				// && Validate.textFieldNumberValidate(textCuitCuil,
				// "Cuil/Cuit")
				&& Validate.textFieldValidate(textDepartamento, "Departamento")
				&& Validate.textFieldNumberValidate(textHorasSemanales,
						"Horas Semanales")
				&& Validate.textFieldValidate(textLocalidad, "Localidad")
				&& Validate.textFieldValidate(textNombre, "Nombre")
				&& Validate.textFieldValidate(textNombrePila, "Nombre de Pila")
				&& Validate.textFieldNumberValidate(textNumero,
						"Numero de Departamento")
				&& Validate.textFieldValidate(textPiso, "Piso")
				&& Validate.textFieldValidate(textTelefono, "Telefono")
				&& Validate.datePickerValidate(dateFechaIngreso,
						"Fecha de Ingreso")
				&& Validate.comboValidate(comboCategoria,
						"Categoria de Empleado")
				&& Validate.comboValidate(comboCalificacionProfesional,
						"calificacion profesional")
				&& Validate.comboValidate(comboObraSocial, "Obra Social")
				&& Validate.comboValidate(comboProvincia, "Provincia")
				&& Validate.comboValidate(comboRol, "combo Rol")
				&& Validate.comboValidate(comboSexo, "Sexo")) {
			return true;
		}
		DialogsUtil.errorDialog("ERROR", "ERROR",
				"todos los campos son obligatorios");
		return false;
	}
}
