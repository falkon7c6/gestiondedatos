package gestion.gestionFinal.controlador.persona;

import gestion.gestionFinal.modelo.Direccion;
import gestion.gestionFinal.modelo.Empleado;
import gestion.gestionFinal.modelo.Persona;
import gestion.gestionFinal.modelo.Personafisica;
import gestion.gestionFinal.persistencia.DireccionDAO;
import gestion.gestionFinal.persistencia.EmpleadoDAO;
import gestion.gestionFinal.persistencia.PersonaDAO;
import gestion.gestionFinal.persistencia.PersonaFisicaDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;

import java.sql.SQLException;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

import javax.annotation.PostConstruct;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@FXMLController(value = "/gestion/gestionFinal/vista/persona/GrupoFamiliar.fxml")
public class GrupoFamiliarController {
	@FXML
	private TableView tablaPersonas;
	@FXML
	private TableColumn colIdP;
	@FXML
	private TableColumn colCuilCuitP;
	@FXML
	private TableColumn colNombreP;
	@FXML
	private TableColumn colApellidoP;
	@FXML
	private TableColumn colNombrePilaP;
	@FXML
	private TableColumn colSexoP;
	@FXML
	private TextField textFiltrar;
	@FXML
	@ActionTrigger("habilitar abm nuevo")
	private Button btnNuevoFamiliar;
	@FXML
	@ActionTrigger("habilitar abm editar")
	private Button btnEditarFamiliar;
	@FXML
	@ActionTrigger("atras")
	private Button btnAtras;
	@FXML
	private TableView tablaFamilia;
	@FXML
	private TableColumn colIdF;
	@FXML
	private TableColumn colCuilCuitF;
	@FXML
	private TableColumn colNombreF;
	@FXML
	private TableColumn colApellidoF;
	@FXML
	private TableColumn colNombrePilaF;
	@FXML
	private TableColumn colSexoF;
	@FXML
	@ActionTrigger("agregar")
	private Button botonAgregar;
	@FXML
	@ActionTrigger("quitar")
	private Button botonQuitar;
	@FXML
	private AnchorPane panelABM;
	@FXML
	private TextField textCuitCuil;
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textApellido;
	@FXML
	private TextField textNombrePila;
	@FXML
	@ActionTrigger("guardar")
	private Button btnGuardar;
	@FXML
	@ActionTrigger("deshabilitar abm")
	private Button btnCancelar;
	@FXML
	private ComboBox<String> comboSexo;
	@FXML
	private ComboBox<String> comboRelacion;
	@FXMLViewFlowContext
	private ViewFlowContext context;

	// variables de uso
	private Empleado padre;
	private Personafisica conyuge;
	private ObservableList<String> sexos;
	private ObservableList<String> relaciones;
	private ObservableList<Personafisica> familia;
	private ObservableList<Personafisica> hijos;
	private ObservableList<Personafisica> filtrados;
	private ObservableList<Personafisica> personas;
	int bandera = -1;

	// PERSISTENCIA
	private DireccionDAO dirDAO;
	private PersonaDAO personaDAO;
	private PersonaFisicaDAO personaFisDAO;
	private EmpleadoDAO empleadoDAO;

	@PostConstruct
	public void Initialize() {
		if (iniciarVariables() == true) {
			iniciarColumnasP();
			iniciarColumnasF();
			iniciarCampos();
		}
	}

	public boolean iniciarVariables() {
		dirDAO = new DireccionDAO();
		personaDAO = new PersonaDAO();
		personaFisDAO = new PersonaFisicaDAO();
		empleadoDAO = new EmpleadoDAO();
		try {
			padre = (Empleado) context.getApplicationContext()
					.getRegisteredObject("padre");
			if (padre == null) {
				padreNulo();
				return false;
			}
			familia = empleadoDAO.listarFamilia(padre);
			personas = personaFisDAO.listarSinFamilia();
			hijos = empleadoDAO.listarHijos(padre);
			conyuge = empleadoDAO.obtenerPareja(padre);
		} catch (Exception e) {
			System.out.println("error en el contexto \n");
			e.printStackTrace();
		}
		return true;
	}

	private void padreNulo() {
		DialogsUtil.errorDialog("Error", "No se encontro padre",
				"No se selecciono ningun " + "padre de la tabla de empleados");
		this.panelABM.setDisable(true);
		this.tablaFamilia.setDisable(true);
		this.tablaPersonas.setDisable(true);
		this.botonAgregar.setDisable(true);
		this.botonQuitar.setDisable(true);
		this.btnCancelar.setDisable(true);
		this.btnEditarFamiliar.setDisable(true);
		this.btnGuardar.setDisable(true);
		this.textFiltrar.setDisable(true);
		this.btnNuevoFamiliar.setDisable(true);
	}

	public void iniciarCampos() {
		panelABM.setDisable(true);
		sexos = FXCollections.observableArrayList();
		sexos.addAll("MASCULINO", "FEMENINO", "OTRO");
		comboSexo.setItems(sexos);
		relaciones = FXCollections.observableArrayList();
		relaciones.addAll("hijo", "pareja");
		comboRelacion.setItems(relaciones);
		tablaFamilia.setItems(familia);
		tablaPersonas.setItems(personas);
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});

		tablaPersonas.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 1) {
							if (tablaPersonas.getSelectionModel()
									.getSelectedItem() != null) {
								llenarEspacios((Personafisica) tablaPersonas
										.getSelectionModel().getSelectedItem());
							}
						}

					}

				});

		tablaFamilia.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 1) {
							if (tablaFamilia.getSelectionModel()
									.getSelectedItem() != null) {
								llenarEspacios((Personafisica) tablaFamilia
										.getSelectionModel().getSelectedItem());
							}
						}

					}

				});

	}

	public void filtrar() {
		String filtrado = textFiltrar.getText().toLowerCase();
		this.filtrados = FXCollections.observableArrayList();
		for (Personafisica per : personas) {
			if (per.getApellido().toLowerCase().startsWith(filtrado)) {
				filtrados.add(per);
			}
			tablaPersonas.setItems(filtrados);
		}
	}

	@ActionMethod("guardar")
	public void nuevoFamiliar() {
		if (this.validar() == true) {
			try {
				Persona personaTemp = null;
				Personafisica personaFisTemp = null;
				String cuilViejo = null;
				Direccion dirTemp = null;
				int index = 0;
				if (bandera == 1) {
					personaFisTemp = (Personafisica) this.tablaFamilia
							.getSelectionModel().getSelectedItem();
					personaTemp = personaFisTemp.getPersona();
					cuilViejo = personaTemp.getCuilcuit();
					dirTemp = padre.getPersonafisica().getPersona()
							.getDireccion();
					// tablaFamilia.setItems(empleados); TODO por si no
					// actualiza tabla
				} else {
					personaTemp = new Persona();
					personaFisTemp = new Personafisica();
					dirTemp = padre.getPersonafisica().getPersona()
							.getDireccion();
				}
				// PERSONA
				personaTemp.setCuilcuit(textCuitCuil.getText());
				personaTemp.setDireccion(dirTemp);
				personaTemp.setDireccionTexto(dirTemp.toString());
				personaTemp.setNombre(textNombre.getText());
				personaTemp.setTelefono(padre.getPersonafisica().getPersona()
						.getTelefono());
				personaTemp.setTipo("Fisica");
				if (bandera == 1) {
					personaDAO.editar(personaTemp, cuilViejo);
				} else {
					personaDAO.insertar(personaTemp);
				}
				// PERSONA FISICA
				personaFisTemp.setPersona(personaTemp);
				personaFisTemp.setApellido(textApellido.getText());
				personaFisTemp.setNombrePila(textNombrePila.getText());
				if (this.comboRelacion.getSelectionModel().getSelectedItem()
						.equals("hijo")) {
					personaFisTemp.setPadre(padre.getPersonafisica());
				} else if (this.comboRelacion.getSelectionModel()
						.getSelectedItem().equals("pareja")) {
					personaFisTemp.setConyuge(padre.getPersonafisica());
					padre.setConyuge(personaFisTemp);
				}
				index = comboSexo.getSelectionModel().getSelectedIndex();
				if (index >= 0) {
					personaFisTemp.setSexo((String) comboSexo.getItems().get(
							index));
				}
				if (bandera == 1) {
					personaFisDAO.editar(personaFisTemp);

				} else {
					int idPerFis = personaFisDAO.insertar(personaFisTemp);
					personaFisTemp.setIdPersonaFisica(idPerFis);
					familia.add(personaFisTemp);
				}

				this.bandera = -1;
				panelABM.setDisable(true);
			} catch (NumberFormatException e) {
				DialogsUtil.errorDialog("ERROR", "Campos numericos",
						"Error en los campos numericos");
			} catch (NullPointerException ex) {
				DialogsUtil.errorDialog("ERROR", "CUIL/CUIT", ex.toString());
				ex.printStackTrace();
			} catch (MySQLIntegrityConstraintViolationException e) {
				DialogsUtil.messageDialog("Pareja",
						"El empleado ya tiene una pareja");
			} catch (SQLException e) {
				DialogsUtil.errorDialog("ERROR", "Error en la Base de datos",
						e.toString());
			}
		}
	}

	@ActionMethod("agregar")
	public void agregarFamiliar() {
		if (Validate.tableNotSelectedItemValidate(tablaPersonas) == true) {
			try {
				Personafisica personaFisTemp = (Personafisica) this.tablaPersonas
						.getSelectionModel().getSelectedItem();
				String resultado = this.preguntarRelacionFamiliar();
				if (resultado.equals("hijo")) {
					personaFisTemp.setPadre(padre.getPersonafisica());
					personaFisDAO.editar(personaFisTemp);
					this.familia.add(personaFisTemp);
					this.personas.remove(personaFisTemp);
				} else if (resultado.equals("pareja")
						&& padre.getConyuge() == null) {
					personaFisTemp.setConyuge(padre.getPersonafisica());
					padre.setConyuge(personaFisTemp);
					padre.getPersonafisica().setConyuge(personaFisTemp);
					personaFisDAO.editar(personaFisTemp);
					personaFisDAO.editar(padre.getPersonafisica());
					this.familia.add(personaFisTemp);
					this.personas.remove(personaFisTemp);
				} else {
					System.out.println("cancelo");
				}
			} catch (MySQLIntegrityConstraintViolationException e) {
				DialogsUtil.messageDialog("Pareja",
								"El empleado ya tiene una pareja o esa relacion ya existe con esa persona");
				e.printStackTrace();
			}
		}
	}

	@ActionMethod("quitar")
	public void quitarFamiliar() {
		if (Validate.tableNotSelectedItemValidate(tablaFamilia)) {
			int index;
			index = tablaFamilia.getSelectionModel().getSelectedIndex();
			if (index >= 0) {
				try {
					Personafisica temp = (Personafisica) tablaFamilia
							.getSelectionModel().getSelectedItem();
					if (temp.getPadre() != null) {
						if (padre.getPersonafisica().getIdPersonaFisica() == temp
								.getPadre().getIdPersonaFisica()) {
							temp.setPadre(null);
						}
					} else {
						temp.setConyuge(null);
						padre.setConyuge(null);
						padre.getPersonafisica().setConyuge(null);
					}
					// this.personaFisDAO.quitarPadre(temp.getIdPersonaFisica());
					personaFisDAO.editar(temp);
					personas.add(temp);
					familia.remove(temp);
				} catch (NullPointerException e) {
					e.printStackTrace();
				} catch (MySQLIntegrityConstraintViolationException e) {
					DialogsUtil.messageDialog("error en la base de datos",
							"error en la base de datos");
				}
			}
		}
	}

	public void llenarEspacios(Personafisica personafisica) {
		this.comboSexo.getSelectionModel().select(personafisica.getSexo());
		this.textCuitCuil.setText(String.valueOf(personafisica.getPersona()
				.getCuilcuit()));
		this.textApellido.setText(personafisica.getApellido());
		this.textNombrePila.setText(personafisica.getNombrePila());
		this.textNombre.setText(personafisica.getPersona().getNombre());
	}

	public void iniciarColumnasP() {
		this.colCuilCuitP
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Personafisica, String> p) {
						SimpleStringProperty cuil = new SimpleStringProperty(p
								.getValue().getPersona().getCuilcuit());
						return cuil;
					}
				});

		this.colApellidoP
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Personafisica, String> p) {
						return p.getValue().apellidoProperty();
					}
				});

		this.colNombreP
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Personafisica, String> p) {
						return p.getValue().getPersona().nombreProperty();
					}
				});

		this.colNombrePilaP
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Personafisica, String> p) {
						return p.getValue().nombrePilaProperty();
					}
				});

		this.colSexoP
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Personafisica, String> p) {
						return p.getValue().sexoProperty();
					}
				});

		this.colIdP
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, Number>, ObservableValue<Number>>() {
					public ObservableValue<Number> call(
							CellDataFeatures<Personafisica, Number> p) {
						return p.getValue().idPersonaFisicaProperty();
					}
				});
	}

	public void iniciarColumnasF() {
		this.colCuilCuitF
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Personafisica, String> p) {
						SimpleStringProperty cuil = new SimpleStringProperty(p
								.getValue().getPersona().getCuilcuit());
						return cuil;
					}
				});

		this.colApellidoF
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Personafisica, String> p) {
						return p.getValue().apellidoProperty();
					}
				});

		this.colNombreF
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Personafisica, String> p) {
						return p.getValue().getPersona().nombreProperty();
					}
				});

		this.colNombrePilaF
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Personafisica, String> p) {
						return p.getValue().nombrePilaProperty();
					}
				});

		this.colSexoF
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Personafisica, String> p) {
						return p.getValue().sexoProperty();
					}
				});

		this.colIdF
				.setCellValueFactory(new Callback<CellDataFeatures<Personafisica, Number>, ObservableValue<Number>>() {
					public ObservableValue<Number> call(
							CellDataFeatures<Personafisica, Number> p) {
						return p.getValue().idPersonaFisicaProperty();
					}
				});
	}

	@ActionMethod("deshabilitar abm")
	public void deshabilitarABM() {
		this.panelABM.setDisable(true);
	}

	@ActionMethod("habilitar abm editar")
	public void HabilitarABMEditar() {
		this.bandera = 1;
		panelABM.setDisable(false);
		this.textCuitCuil.setDisable(true);
	}

	@ActionMethod("habilitar abm nuevo")
	public void HabilitarABMNuevo() {
		this.bandera = 0;
		panelABM.setDisable(false);
		this.textCuitCuil.setDisable(false);
	}

	public boolean validar() {
		if (Validate.textFieldValidate(textApellido, "Apellido")
				&& Validate.ExpresionTextFieldValidate(textCuitCuil,
						"[0-9]{11}", "Cuil/Cuit", "11 numeros sin espacios")
				&& Validate.textFieldValidate(textNombre, "Nombre")
				&& Validate.textFieldValidate(textNombrePila, "Nombre de Pila")
				&& Validate.comboValidate(comboRelacion, "Relacion Familiar")
				&& Validate.comboValidate(comboSexo, "Sexo")) {
			return true;
		}
		DialogsUtil.errorDialog("ERROR", "ERROR",
				"todos los campos son obligatorios");
		return false;
	}

	public String preguntarRelacionFamiliar() {
		// List<String> lista = new ArrayList<String>();
		// lista.add("hijo");
		// lista.add("pareja");
		return DialogsUtil.choiseDialog("Relacion Familiar",
				"Seleccione la relacion", "", relaciones);
	}



}
