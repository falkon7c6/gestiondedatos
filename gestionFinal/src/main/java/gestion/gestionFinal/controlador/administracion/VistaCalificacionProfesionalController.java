package gestion.gestionFinal.controlador.administracion;

import gestion.gestionFinal.modelo.Calificacionprofesional;
import gestion.gestionFinal.persistencia.CalificacionProfesionalDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/administracion/VistaCalificacionProfesional.fxml")
public class VistaCalificacionProfesionalController {
	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView<Calificacionprofesional> tablaCategoriaEmpleado;
	@FXML
	private TableColumn colId;
	@FXML
	private TableColumn colDescripcion;
	@FXML
	private TableColumn colSalarioHora;
	@FXML
	private TableColumn colImporteFijo;
	@FXML
	private GridPane panelABM;
	@FXML
	private TextField textDescripcion;
	@FXML
	private TextField textSalarioHora;
	@FXML
	private TextField textImporteFijo;
	@FXML
	@ActionTrigger("nueva")
	private Button botonNueva;
	@FXML
	@ActionTrigger("eliminar")
	private Button botonEliminar;
	@FXML
	@ActionTrigger("editar")
	private Button botonEditar;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;

	private ObservableList<Calificacionprofesional> itemsTabla;
	private ObservableList<Calificacionprofesional> filtrados;
	private CalificacionProfesionalDAO CalificacionDao;
	int bandera = -1;

	@PostConstruct
	public void Initialize() {
		this.desHabilitarBotonesAceptarCancelar();
		this.panelABM.setDisable(true);
		CalificacionDao = new CalificacionProfesionalDAO();
		iniciarColumnas();
		itemsTabla = FXCollections.observableArrayList();
		itemsTabla = CalificacionDao.listar();
		this.tablaCategoriaEmpleado.setItems(itemsTabla);
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});
		this.tablaCategoriaEmpleado.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 1) {
							if (tablaCategoriaEmpleado.getSelectionModel()
									.getSelectedItem() != null) {
								llenarEspacios(tablaCategoriaEmpleado
										.getSelectionModel().getSelectedItem());
							}
						}

					}

				});
	}

	private void iniciarColumnas() {
		this.colId.setCellValueFactory(new PropertyValueFactory<>(
				"idCalificacionProfesional"));
		this.colSalarioHora.setCellValueFactory(new PropertyValueFactory<>(
				"salarioHora"));
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<>(
				"descripcion"));
		this.colImporteFijo.setCellValueFactory(new PropertyValueFactory<>(
				"importeFijo"));
	}

	public void llenarEspacios(Calificacionprofesional calificacionprofesional) {
		this.textSalarioHora.setText(""
				+ calificacionprofesional.getSalarioHora());
		this.textDescripcion.setText(calificacionprofesional.getDescripcion());
		this.textImporteFijo.setText(String.valueOf(calificacionprofesional
				.getImporteFijo()));
	}

	public void habilitarBotonesAceptarCancelar() {
		this.botonAceptar.setDisable(false);
		this.botonCancelar.setDisable(false);
	}

	public void desHabilitarBotonesAceptarCancelar() {
		this.botonAceptar.setDisable(true);
		this.botonCancelar.setDisable(true);
	}

	@ActionMethod("nueva")
	public void nueva() {
		this.bandera = 0;
		this.panelABM.setDisable(false);
		this.habilitarBotonesAceptarCancelar();
	}

	@ActionMethod("editar")
	public void editar() {
		if (Validate.tableNotSelectedItemValidate(tablaCategoriaEmpleado)) {
			bandera = 1;
			this.panelABM.setDisable(false);
			this.habilitarBotonesAceptarCancelar();
		}

	}

	@ActionMethod("eliminar")
	public void eliminar() {
		try {
			Calificacionprofesional temp = this.tablaCategoriaEmpleado
					.getSelectionModel().getSelectedItem();
			this.CalificacionDao.borrar(temp.getIdCalificacionProfesional());
			this.itemsTabla.remove(temp);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos",
					"La calificacion profesional no puede ser eliminada");
		} catch (NullPointerException e) {
			System.out.println("null pointer");
		}

	}

	public void filtrar() {
		String filtrado = textFiltrar.getText().toLowerCase();
		filtrados = FXCollections.observableArrayList();
		for (Calificacionprofesional cat : itemsTabla) {
			if (cat.getDescripcion().toLowerCase().startsWith(filtrado)) {
				filtrados.add(cat);
			}
			this.tablaCategoriaEmpleado.setItems(filtrados);
		}
	}

	@ActionMethod("aceptar")
	public void aceptar() {
		if (this.validar()) {
			try {
				Calificacionprofesional cal = null;
				if (bandera == 0) {
					cal = new Calificacionprofesional();

				}
				if (bandera == 1
						&& Validate
								.tableNotSelectedItemValidate(tablaCategoriaEmpleado)) {
					cal = tablaCategoriaEmpleado.getSelectionModel()
							.getSelectedItem();
				}
				cal.setSalarioHora(Float.valueOf(textSalarioHora.getText()));
				cal.setDescripcion(this.textDescripcion.getText());
				cal.setImporteFijo(Float.valueOf(this.textImporteFijo.getText()));
				if (bandera == 0) {
					int id;
					id = CalificacionDao.insertar(cal);
					cal.setIdCalificacionProfesional(id);
					itemsTabla.add(cal);

				}
				if (bandera == 1) {
					/*
					 * cal = tablaCategoriaEmpleado.getSelectionModel()
					 * .getSelectedItem();
					 */
					CalificacionDao.editar(cal);
				}
				bandera = -1;
				this.panelABM.setDisable(true);
				this.tablaCategoriaEmpleado.setItems(itemsTabla);
				this.desHabilitarBotonesAceptarCancelar();
			} catch (NumberFormatException e) {
				DialogsUtil
						.errorDialog(
								"ERROR",
								"el salario por hora y el importe fijo deben ser un numero con decimal",
								e.toString());
			} catch (SQLException e) {
				DialogsUtil.errorDialog("ERROR", "error  al Insertar en la DB",
						e.toString());
			}
		}
	}

	@ActionMethod("cancelar")
	public void cancelar() {
		this.panelABM.setDisable(true);
		this.desHabilitarBotonesAceptarCancelar();
	}

	public boolean validar() {
		if (Validate.textFieldValidate(textDescripcion, "descripcion")
				&& Validate.textFieldDecimalValidate(textSalarioHora,
						"salario por hora")
				&& Validate.textFieldDecimalValidate(textImporteFijo,
						"importe fijo")

		) {
			return true;
		}

		return false;
	}
}
