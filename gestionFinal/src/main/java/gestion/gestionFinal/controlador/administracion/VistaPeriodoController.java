package gestion.gestionFinal.controlador.administracion;

import gestion.gestionFinal.modelo.Periodo;
import gestion.gestionFinal.persistencia.PeriodoDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import java.sql.SQLException;
import java.time.LocalDate;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

import javax.annotation.PostConstruct;

@FXMLController(value="/gestion/gestionFinal/vista/administracion/VistaPeriodo.fxml")
public class VistaPeriodoController {
	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView<Periodo> tabla;
	@FXML
    private TableColumn colId;

    @FXML
    private TableColumn colAnio;

    @FXML
    private TableColumn colMes;

    @FXML
    private TableColumn colDescripcion;
	@FXML
	private GridPane panelABM;
	@FXML
	private TextField textDescription;
	@FXML
	private Spinner<Integer> spinAnio;
	@FXML
	private Spinner<Integer> spinMes;
	@FXML
	@ActionTrigger("nueva")
	private Button botonNueva;
	@FXML
	@ActionTrigger("eliminar")
	private Button botonEliminar;
	@FXML
	@ActionTrigger("editar")
	private Button botonEditar;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;

	private ObservableList<Periodo> itemsTabla;
	private ObservableList<Periodo> filtrados;
	private PeriodoDAO periodoDao;
	int bandera = -1;

	@PostConstruct
	public void Initialize() {
		this.panelABM.setDisable(true);
		periodoDao = new PeriodoDAO();
		iniciarColumnas();
		itemsTabla = FXCollections.observableArrayList();
		itemsTabla = periodoDao.listar();
		this.tabla.setItems(itemsTabla);
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});
		LocalDate fecha = LocalDate.now();
		
		this.spinAnio.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(fecha.getYear(), 3000, fecha.getYear()));
		//this.spinAnio.setEditable(true);
		this.spinMes.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 12, 1));
		this.tabla.getSelectionModel().getSelectedIndices()
		.addListener(new ListChangeListener<Integer>() {
			@Override
			public void onChanged(Change<? extends Integer> change) {
				if (change.getList().size() >= 1) {
					if (tabla.getSelectionModel()
							.getSelectedItem() != null) {
						llenarEspacios(tabla
								.getSelectionModel().getSelectedItem());
					}
				}

			}

		});
	}
	
	private void iniciarColumnas() {
		this.colId.setCellValueFactory(new PropertyValueFactory<>("idPeriodo"));
		this.colAnio.setCellValueFactory(new PropertyValueFactory<>("anio"));
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
		this.colMes.setCellValueFactory(new PropertyValueFactory<>("mes"));
		
	}

	public void llenarEspacios(Periodo p){
		this.textDescription.setText("" + p.getDescripcion());
		this.spinAnio.getValueFactory().setValue(Integer.valueOf(p.getAnio())); 
		this.spinMes.getValueFactory().setValue(Integer.valueOf(p.getMes()));
	}
	
	@ActionMethod("nueva")
	public void nueva(){
		this.bandera = 0;
		this.panelABM.setDisable(false);
	}
	@ActionMethod("editar")
	public void editar(){
		bandera = 1;
		this.panelABM.setDisable(false);
	}
	
	@ActionMethod("eliminar")
	public void eliminar(){
		try {
			Periodo temp = this.tabla.getSelectionModel().getSelectedItem();
			this.periodoDao.borrar(temp.getIdPeriodo());
			this.itemsTabla.remove(temp);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", "El periodo no puede ser eliminado");
		} catch (NullPointerException e) {
			System.out.println("null pointer");
		}
		
	}
	
	public void filtrar(){
		String filtrado = textFiltrar.getText().toLowerCase();
		filtrados = FXCollections.observableArrayList();
		for (Periodo periodo : itemsTabla) {
			if (periodo.getDescripcion().toLowerCase()
					.startsWith(filtrado)) {
				filtrados.add(periodo);
			}
			this.tabla.setItems(filtrados);
		}
	}
	@ActionMethod("aceptar")
	public void aceptar(){
		if (validar()) {
			try {
				Periodo periodo = null;
				if(bandera == 0){
					periodo = new Periodo();
					
				}
				if(bandera == 1){
					periodo = tabla.getSelectionModel().getSelectedItem();
				}
				periodo.setAnio(this.spinAnio.getValueFactory().getValue().toString());
				periodo.setDescripcion(this.textDescription.getText());
				periodo.setMes(this.spinMes.getValueFactory().getValue().toString());
				if(bandera == 0){
					int id;
					id = periodoDao.insertar(periodo);
					periodo.setIdPeriodo(id);
					itemsTabla.add(periodo);
					
				}
				if(bandera == 1){
					periodo = tabla.getSelectionModel().getSelectedItem();
					periodoDao.editar(periodo);
				}
				bandera = -1;
				this.panelABM.setDisable(true);
				this.tabla.setItems(itemsTabla);
			} catch (SQLException e) {
				DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos", e.toString());
			}
		}
	}
	@ActionMethod("cancelar")
	public void cancelar(){
		this.panelABM.setDisable(true);
	}
	
	public boolean validar() {
		if(Validate.textFieldValidate(textDescription, "Descripcion")) {
			return true;
		}
		return false;
	}
}
