package gestion.gestionFinal.controlador.administracion;

import gestion.gestionFinal.controlador.persona.VistaPrincipalEmpleadoController;
import gestion.gestionFinal.controlador.producto.vistaProductoController;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.container.DefaultFlowContainer;
import io.datafx.controller.util.VetoException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/administracion/VistaAdministracion.fxml")
public class VistaAdministracionController {
	@FXML
	private AnchorPane panelIzquierdo;
	@FXML
	private VBox VboxIzquierda;
	@FXML
	// @ActionTrigger("EmpleadosAdmin")
	private Button botonPersonal;
	@FXML
	@ActionTrigger("ProductosAdmin")
	private Button botonProductos;
	@FXML
	@ActionTrigger("ObraSocialAdmin")
	private Button botonObraSocial;
	@FXML
	@ActionTrigger("CategoriaEmpleadoAdmin")
	private Button botonCategoriaEmpleado;
	@FXML
	@ActionTrigger("menu principal")
	private Button botonAtras;
	@FXML
	@ActionTrigger("periodoAdmin")
	private Button botonPeriodo;
	@FXML
	@ActionTrigger("reportes")
	private Button botonReportes;
	@FXML
	@ActionTrigger("categoriaProducto")
	private Button botonCategoriaProducto;
	@FXML
	@ActionTrigger("calificacionPro")
	private Button botonCalificacionProfesional;
	@FXML
	@ActionTrigger("marcaProducto")
	private Button botonMarcaProducto;
	@FXML
	@ActionTrigger("conceptos")
	private Button botonConceptos;
	@FXML
	@ActionTrigger("usuario")
	private Button botonUsuario;

	@FXML
	private SplitPane splitpane;

	@FXML
	private StackPane panelDerecho;

	

	private FlowHandler flowHandler;

	@PostConstruct
	public void Initialize() {
		try {
			Flow innerFlow = new Flow(
					VistaCalificacionProfesionalController.class);
			flowHandler = innerFlow.createHandler();
			panelDerecho.getChildren().add(
					flowHandler.start(new DefaultFlowContainer()));
			// panelDerecho.getStylesheets().add("/styles/NewFile.css");
			
		} catch (FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

	// @ActionMethod("EmpleadosAdmin")
	public void toEmpleados() {
		try {
			this.flowHandler.navigateTo(VistaPrincipalEmpleadoController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	@ActionMethod("ProductosAdmin")
	public void toProductos() {
		try {
			this.flowHandler.navigateTo(vistaProductoController.class);

		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ActionMethod("ObraSocialAdmin")
	public void toObraSocial() {
		try {
			flowHandler.navigateTo(VistaObraSocialController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ActionMethod("CategoriaEmpleadoAdmin")
	public void toCategoriaEmpleado() {
		try {
			flowHandler.navigateTo(VistaCategoriaEmpleadoController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ActionMethod("reportes")
	public void toReportes() {
		try {
			flowHandler.navigateTo(VistaReportesController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ActionMethod("categoriaProducto")
	public void toCategoriaProducto() {
		try {
			flowHandler.navigateTo(VistaCategoriaProductoController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ActionMethod("marcaProducto")
	public void toMarca() {
		try {
			flowHandler.navigateTo(VistaMarcaProductoController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ActionMethod("calificacionPro")
	public void toCalificacionPro() {
		try {
			flowHandler
					.navigateTo(VistaCalificacionProfesionalController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ActionMethod("periodoAdmin")
	public void toPeriodo() {
		try {
			flowHandler.navigateTo(VistaPeriodoController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@ActionMethod("conceptos")
	public void toConcepto(){
		try {
			flowHandler.navigateTo(VistaConceptoController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ActionMethod("usuario")
	public void toUsuario() {
		try {
			flowHandler.navigateTo(VistaUsuarioController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
