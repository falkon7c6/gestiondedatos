package gestion.gestionFinal.controlador.administracion;

import gestion.gestionFinal.persistencia.CompraDAO;
import gestion.gestionFinal.persistencia.SueldoDAO;
import gestion.gestionFinal.persistencia.VentaDAO;
import gestion.gestionFinal.persistencia.databaseConnection;
import gestion.gestionFinal.util.ReportUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.util.Callback;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/administracion/VistaReportes.fxml")
public class VistaReportesController {

	@FXML
	private TableView tabla;

	@FXML
	@ActionTrigger("id")
	private Button botonAceptar;

	@FXML
	private ComboBox<String> comboTipo;

	private databaseConnection conexion;
	private ObservableList<ObservableList> data;
	private ObservableList<String> itemsCombo;

	@PostConstruct
	public void init() {
		conexion = new databaseConnection();

		iniciarCombo();
		SueldoDAO sueldoDAO = new SueldoDAO();
		CompraDAO compaDao = new CompraDAO();
		VentaDAO ventaDao = new VentaDAO();
		data = FXCollections.observableArrayList();
	}

	public void iniciarCombo() {
		itemsCombo = FXCollections.observableArrayList();
		itemsCombo.add("sueldo");
		itemsCombo.add("compra");
		itemsCombo.add("venta");
		comboTipo.setItems(itemsCombo);
		comboTipo.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<String>() {
					@Override
					public void changed(
							ObservableValue<? extends String> observable,
							String oldValue, String newValue) {
						System.out.println("cambio a "
								+ comboTipo.getSelectionModel()
										.getSelectedItem());
						seleccionar();
					}
				});
	}

	public void seleccionar() {
		// *validar TODO
		tabla.setTableMenuButtonVisible(true);
		this.selectRecordsFromTable((String) comboTipo.getSelectionModel()
				.getSelectedItem());
	}

	@ActionMethod("id")
	public void imprimir() {
		if (Validate.tableNotSelectedItemValidate(tabla)) {
			TableColumn col = (TableColumn) tabla.getColumns().get(0);
			int index = tabla.getSelectionModel().getSelectedIndex();
			String id = col.getCellData(index).toString();
			Map<String, Object> parametrosMap = new HashMap();
			ReportUtil reporterReportUtil = new ReportUtil();
			String reporteNombre;
			switch (comboTipo.getSelectionModel().getSelectedIndex()) {
			case 0:
				parametrosMap.put("idliquidacion", new Integer(id));
				reporterReportUtil = new ReportUtil();
				reporteNombre = "liquidacion" + ".pdf";
				reporterReportUtil.imprimirPDF(
						"/reportes/ReciboHaberesPrueba.jasper", parametrosMap,
						reporteNombre);
				break;
			case 1:
				parametrosMap.put("idcompra", new Integer(id));
				reporterReportUtil = new ReportUtil();
				reporteNombre = "compra" + ".pdf";
				reporterReportUtil.imprimirPDF("/reportes/CompraPrueba.jasper",
						parametrosMap, reporteNombre);
				break;
			case 2:
				parametrosMap.put("idventa", new Integer(id));
				reporterReportUtil = new ReportUtil();
				reporteNombre = "reporte.pdf";
				reporterReportUtil.imprimirPDF("/reportes/ventaprueba.jasper",
						parametrosMap, reporteNombre);
				parametrosMap.clear();
				break;
			default:
				break;
			}
		}

	}

	private void selectRecordsFromTable(String seleccion) {

		PreparedStatement preparedStatement = null;
		String selectSQL = "SELECT * from gestionfinal." + seleccion;
		data.clear();
		tabla.getColumns().clear();
		try {
			conexion.conectar();
			preparedStatement = conexion.connection.prepareStatement(selectSQL);
			// execute select SQL stetement
			ResultSet rs = preparedStatement.executeQuery();
			for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
				// We are using non property style for making dynamic table
				final int j = i;
				TableColumn col = new TableColumn(rs.getMetaData()
						.getColumnName(i + 1));
				System.out.println(rs.getMetaData().getColumnName(i + 1));
				col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<ObservableList, String> param) {
						return new SimpleStringProperty(param.getValue().get(j)
								.toString());
					}
				});
				tabla.getColumns().addAll(col);
				System.out.println("Column [" + i + "] ");
			}

			while (rs.next()) {

				ObservableList<String> row = FXCollections
						.observableArrayList();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					// Iterate Column
					row.add(rs.getString(i));
				}
				System.out.println("Row [1] added " + row);
				data.add(row);

			}

			// FINALLY ADDED TO TableView
			tabla.setItems(data);

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (conexion.connection != null) {
				try {
					conexion.connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}

}
