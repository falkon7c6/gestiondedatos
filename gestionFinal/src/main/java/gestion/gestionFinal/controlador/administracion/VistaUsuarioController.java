package gestion.gestionFinal.controlador.administracion;

import gestion.gestionFinal.modelo.Empleado;
import gestion.gestionFinal.persistencia.EmpleadoDAO;
import gestion.gestionFinal.persistencia.UsuarioDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import java.sql.SQLException;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/administracion/VistaUsuario.fxml")
public class VistaUsuarioController {
	@FXML
	private TableView<Empleado> tabla;
	@FXML
	private GridPane panelABM;
	@FXML
	private PasswordField textPass1;
	@FXML
	private PasswordField textPass2;
	@FXML
	private ComboBox<String> comboRol;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	@FXML
	private TableColumn<Empleado, String> colLegajo;
	@FXML
	private TableColumn<Empleado, String> colNombrePila;
	@FXML
	@ActionTrigger("editarPrivilegios")
	private Button botonCambiarPrivilegios;
	@FXML
	@ActionTrigger("editarPassword")
	private Button botonCambiarPassword;

	private ObservableList<String> roles;
	private ObservableList<Empleado> empleados;
	private UsuarioDAO usuarioDAO;
	private EmpleadoDAO empleadoDAO;
	private int bandera = -1;

	@PostConstruct
	public void init() {
		iniciarCampos();
		iniciarColumna();
		this.desHabilitarPassword();
		this.desHabilitarprivilegio();
	}

	private void iniciarCampos() {
		roles = FXCollections.observableArrayList();
		empleados = FXCollections.observableArrayList();
		usuarioDAO = new UsuarioDAO();
		empleadoDAO = new EmpleadoDAO();
		roles.addAll("administracion", "compraventa", "liquidacion", "ninguno");
		empleados = empleadoDAO.listarActivos();
		comboRol.setItems(roles);
		tabla.setItems(empleados);
	}

	private void iniciarColumna() {
		this.colLegajo
				.setCellValueFactory(new PropertyValueFactory<>("legajo"));

		this.colNombrePila
				.setCellValueFactory(new Callback<CellDataFeatures<Empleado, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Empleado, String> p) {
						return p.getValue().getPersonafisica()
								.nombrePilaProperty();
					}
				});

	}

	public boolean comprobarPassword() {
		if (textPass1.getText().equals(textPass2.getText())
				&& textPass1.getText().isEmpty() == false
				&& textPass2.getText().isEmpty() == false
				&& textPass1.getText().length() > 0
				&& textPass2.getText().length() > 0) {
			return true;
		}
		DialogsUtil.messageDialog("Usuario",
				"los passwords no coinciden o estan vacios");
		return false;
	}

	public boolean validarRol() {
		if (Validate.comboValidate(comboRol, "Roles")) {
			System.out.println("valido");
			return true;
		}
		return false;
	}

	@ActionMethod("editarPassword")
	public void habilitarPassword() {
		if (Validate.tableNotSelectedItemValidate(tabla)) {
			this.textPass1.setDisable(false);
			this.textPass2.setDisable(false);
			this.botonAceptar.setDisable(false);
			this.botonCancelar.setDisable(false);
			bandera = 0;
		}

	}

	@ActionMethod("editarPrivilegios")
	public void habilitarprivilegio() {
		if (Validate.tableNotSelectedItemValidate(tabla)) {
			this.comboRol.setDisable(false);
			this.botonAceptar.setDisable(false);
			this.botonCancelar.setDisable(false);
			bandera = 1;
		}

	}

	public void desHabilitarPassword() {
		this.textPass1.setDisable(true);
		this.textPass2.setDisable(true);
		this.botonAceptar.setDisable(true);
		this.botonCancelar.setDisable(true);
	}

	public void desHabilitarprivilegio() {
		this.comboRol.setDisable(true);
		this.botonAceptar.setDisable(true);
		this.botonCancelar.setDisable(true);
	}

	public void editarPrivilegios() {
		if (Validate.tableNotSelectedItemValidate(tabla) && validarRol()) {
			try {
				System.out.println("entro");
				Empleado emp = (Empleado) tabla.getSelectionModel()
						.getSelectedItem();
				String legajoString = String.valueOf(emp.getLegajo());
				usuarioDAO.otorgarPrivilegios(legajoString, comboRol
						.getSelectionModel().getSelectedItem());
				bandera = -1;
				this.desHabilitarprivilegio();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void editarPassword() {
		if (Validate.tableNotSelectedItemValidate(tabla) && comprobarPassword()) {
			try {
				Empleado emp = (Empleado) tabla.getSelectionModel()
						.getSelectedItem();
				String legajoString = String.valueOf(emp.getLegajo());
				String password = textPass1.getText();
				usuarioDAO.cambiarClave(legajoString, password);
				bandera = -1;
				this.desHabilitarPassword();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@ActionMethod("cancelar")
	public void cancelar() {
		this.desHabilitarPassword();
		this.desHabilitarprivilegio();
		bandera = -1;
	}

	@ActionMethod("aceptar")
	public void aceptar() {
		if (bandera == 0) {
			this.editarPassword();
		}
		if (bandera == 1) {
			this.editarPrivilegios();
		}
	}
}
