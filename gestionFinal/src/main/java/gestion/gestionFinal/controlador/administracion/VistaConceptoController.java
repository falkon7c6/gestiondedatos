package gestion.gestionFinal.controlador.administracion;

import gestion.gestionFinal.modelo.Conceptoliquidacion;
import gestion.gestionFinal.persistencia.ConceptoDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

import javax.annotation.PostConstruct;

@FXMLController(value="/gestion/gestionFinal/vista/administracion/VistaConcepto.fxml")
public class VistaConceptoController {
	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView tabla;
	@FXML
	private TableColumn colId;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TableColumn colDescripcion;
	@FXML
	private TableColumn colMonto;
	@FXML
	private TableColumn colTipo;
	@FXML
	private GridPane panelABM;
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textDescripcion;
	@FXML
	private TextField textMonto;
	@FXML
	private ComboBox comboTipo;
	@FXML
	@ActionTrigger("nuevo")
	private Button botonNuevo;
	@FXML
	@ActionTrigger("eliminar")
	private Button botonEliminar;
	@FXML
	@ActionTrigger("editar")
	private Button botonEditar;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;


	private ObservableList<Conceptoliquidacion> itemsTabla;
	private ObservableList<Conceptoliquidacion> filtrados;
	private ObservableList<String> itemsCombo;
	private ConceptoDAO conceptoDao;
	int bandera = -1;

	@PostConstruct
	public void Initialize() {
		this.desHabilitarBotonesAceptarCancelar();
		panelABM.setDisable(true);
		conceptoDao = new ConceptoDAO();
		iniciarColumnas();
		itemsTabla = FXCollections.observableArrayList();
		itemsTabla = conceptoDao.listar();
		this.tabla.setItems(itemsTabla);
		itemsCombo = FXCollections.observableArrayList("debito", "credito");
		this.comboTipo.setItems(itemsCombo);
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});
		this.tabla.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 1) {
							if (tabla.getSelectionModel()
									.getSelectedItem() != null) {
								llenarEspacios((Conceptoliquidacion) tabla
										.getSelectionModel().getSelectedItem());
							}
						}
					}
				});
	}

	private void iniciarColumnas() {
		this.colId.setCellValueFactory(new PropertyValueFactory<>(
				"idConceptoLiquidacion"));
		this.colNombre.setCellValueFactory(new PropertyValueFactory<>(
				"nombre"));
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<>(
				"descripcion"));
		this.colMonto.setCellValueFactory(new PropertyValueFactory<>(
				"monto"));
		this.colTipo.setCellValueFactory(new PropertyValueFactory<>(
				"tipo"));
	}

	public void llenarEspacios(Conceptoliquidacion concepto) {
		
		this.textDescripcion.setText(concepto.getDescripcion());
		this.textNombre.setText(concepto.getNombre());
		this.textMonto.setText("" + concepto.getMonto());
		this.comboTipo.getSelectionModel().select(concepto.getTipo());
	}

	@ActionMethod("nuevo")
	public void nueva() {
		this.bandera = 0;
		this.panelABM.setDisable(false);
		this.habilitarBotonesAceptarCancelar();
	}

	@ActionMethod("editar")
	public void editar() {
		if (Validate.tableNotSelectedItemValidate(tabla)) {
			bandera = 1;
		this.panelABM.setDisable(false);
			this.habilitarBotonesAceptarCancelar();
		}

	}

	public void habilitarBotonesAceptarCancelar() {
		this.botonAceptar.setDisable(false);
		this.botonCancelar.setDisable(false);
	}

	public void desHabilitarBotonesAceptarCancelar() {
		this.botonAceptar.setDisable(true);
		this.botonCancelar.setDisable(true);
	}

	@ActionMethod("eliminar")
	public void eliminar() {
		try {
			Conceptoliquidacion temp = (Conceptoliquidacion) this.tabla
					.getSelectionModel().getSelectedItem();
			this.conceptoDao.borrar(temp.getIdConceptoLiquidacion()); //TODO 
			this.itemsTabla.remove(temp);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos",
					"El concepto no puede ser eliminada");
		} catch (NullPointerException e) {
			System.out.println("null pointer");
		}

	}

	public void filtrar() {
		String filtrado = textFiltrar.getText().toLowerCase();
		filtrados = FXCollections.observableArrayList();
		for (Conceptoliquidacion con : itemsTabla) {
			if (con.getDescripcion().toLowerCase().startsWith(filtrado)) {
				filtrados.add(con);
			}
			this.tabla.setItems(filtrados);
		}
	}

	@ActionMethod("aceptar")
	public void aceptar() {
		if (this.validar()) {
			try {
				Conceptoliquidacion con = null;
				if (bandera == 0) {
					con = new Conceptoliquidacion();

				}
				if (bandera == 1
						&& Validate.tableNotSelectedItemValidate(tabla)) {
					con = (Conceptoliquidacion) tabla.getSelectionModel()
							.getSelectedItem();
				}
				con.setMonto(Float.valueOf(textMonto.getText()));
				con.setDescripcion(this.textDescripcion.getText());
				con.setTipo((String) comboTipo.getSelectionModel()
						.getSelectedItem());
				con.setNombre(textNombre.getText());
				if (bandera == 0) {
					int id;
					id = conceptoDao.insertar(con);
					con.setIdConceptoLiquidacion(id);
					itemsTabla.add(con);
				}
				if (bandera == 1) {
					/*
					 * con = (Conceptoliquidacion) tabla.getSelectionModel()
					 * .getSelectedItem();
					 */
					conceptoDao.editar(con);
				}
				bandera = -1;
				this.panelABM.setDisable(true);
				this.tabla.setItems(itemsTabla);
				this.desHabilitarBotonesAceptarCancelar();
			} catch (NumberFormatException e) {
				DialogsUtil
						.errorDialog(
								"ERROR",
								"el salario por hora y el importe fijo deben ser un numero con decimal",
								e.toString());
			} catch (SQLException e) {
				DialogsUtil.errorDialog("ERROR", "error  al Insertar en la DB",
						e.toString());
			}
		}
	}

	@ActionMethod("cancelar")
	public void cancelar() {
		this.panelABM.setDisable(true);
		this.desHabilitarBotonesAceptarCancelar();
	}
	
	public boolean validar() {
		if(
Validate.textFieldValidate(textNombre, "nombre")
				&& Validate.textFieldValidate(textDescripcion, "descripcion")
				&& Validate.textFieldDecimalValidate(textMonto, "monto")
				&& Validate.comboValidate(comboTipo, "Tipo")
				
				) {
			return true;
		}
		
		return false;
	}
	
	
}
