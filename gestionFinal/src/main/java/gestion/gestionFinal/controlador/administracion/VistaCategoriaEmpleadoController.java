package gestion.gestionFinal.controlador.administracion;

import gestion.gestionFinal.modelo.Categoriaempleado;
import gestion.gestionFinal.persistencia.CategoriaEmpleadoDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/administracion/VistaCategoriaEmpleado.fxml")
public class VistaCategoriaEmpleadoController {
	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView<Categoriaempleado> tablaCategoriaEmpleado;
	@FXML
	private TableColumn colId;
	@FXML
	private TableColumn colDescripcion;
	@FXML
	private TableColumn colSalarioHora;
	@FXML
	private TextField textDescripcion;
	@FXML
	private TextField textSalarioHora;
	@FXML
	@ActionTrigger("nueva")
	private Button botonNueva;
	@FXML
	@ActionTrigger("eliminar")
	private Button botonEliminar;
	@FXML
	@ActionTrigger("editar")
	private Button botonEditar;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	@FXML
	private GridPane panelABM;

	private ObservableList<Categoriaempleado> itemsTabla;
	private ObservableList<Categoriaempleado> filtrados;
	private CategoriaEmpleadoDAO categoriaDao;
	int bandera = -1;

	@PostConstruct
	public void Initialize() {
		this.desHabilitarBotonesAceptarCancelar();
		this.panelABM.setDisable(true);
		categoriaDao = new CategoriaEmpleadoDAO();
		iniciarColumnas();
		itemsTabla = FXCollections.observableArrayList();
		itemsTabla = categoriaDao.listar();
		this.tablaCategoriaEmpleado.setItems(itemsTabla);
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});
		this.tablaCategoriaEmpleado.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 1) {
							if (tablaCategoriaEmpleado.getSelectionModel()
									.getSelectedItem() != null) {
								llenarEspacios(tablaCategoriaEmpleado
										.getSelectionModel().getSelectedItem());
							}
						}

					}

				});
	}

	private void iniciarColumnas() {
		this.colId.setCellValueFactory(new PropertyValueFactory<>(
				"idCategoriaEmpleado"));
		this.colSalarioHora.setCellValueFactory(new PropertyValueFactory<>(
				"salarioHora"));
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<>(
				"descripcion"));

	}

	public void llenarEspacios(Categoriaempleado cat) {
		this.textSalarioHora.setText("" + cat.getSalarioHora());
		this.textDescripcion.setText(cat.getDescripcion());
	}

	@ActionMethod("nueva")
	public void nueva() {
		this.bandera = 0;
		this.panelABM.setDisable(false);
		this.habilitarBotonesAceptarCancelar();
	}

	@ActionMethod("editar")
	public void editar() {
		if (Validate.tableNotSelectedItemValidate(tablaCategoriaEmpleado)) {
			bandera = 1;
			this.panelABM.setDisable(false);
			this.habilitarBotonesAceptarCancelar();
		}

	}

	public void habilitarBotonesAceptarCancelar() {
		this.botonAceptar.setDisable(false);
		this.botonCancelar.setDisable(false);
	}

	public void desHabilitarBotonesAceptarCancelar() {
		this.botonAceptar.setDisable(true);
		this.botonCancelar.setDisable(true);
	}

	@ActionMethod("eliminar")
	public void eliminar() {
		try {
			Categoriaempleado temp = this.tablaCategoriaEmpleado
					.getSelectionModel().getSelectedItem();
			this.categoriaDao.borrar(temp.getIdCategoriaEmpleado());
			this.itemsTabla.remove(temp);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos",
					"La categoria no puede ser eliminada");
		} catch (NullPointerException e) {
			System.out.println("null pointer");
		}

	}

	public void filtrar() {
		String filtrado = textFiltrar.getText().toLowerCase();
		filtrados = FXCollections.observableArrayList();
		for (Categoriaempleado cat : itemsTabla) {
			if (cat.getDescripcion().toLowerCase().startsWith(filtrado)) {
				filtrados.add(cat);
			}
			this.tablaCategoriaEmpleado.setItems(filtrados);
		}
	}

	@ActionMethod("aceptar")
	public void aceptar() {
		if (this.validar()) {
			try {
				Categoriaempleado cat = null;
				if (bandera == 0) {
					cat = new Categoriaempleado();

				}
				if (bandera == 1
						&& Validate
								.tableNotSelectedItemValidate(tablaCategoriaEmpleado)) {
					cat = tablaCategoriaEmpleado.getSelectionModel()
							.getSelectedItem();
				}
				cat.setSalarioHora(Float.valueOf(textSalarioHora.getText()));
				cat.setDescripcion(this.textDescripcion.getText());
				if (bandera == 0) {
					int id;
					id = categoriaDao.insertar(cat);
					cat.setIdCategoriaEmpleado(id);
					itemsTabla.add(cat);

				}
				if (bandera == 1) {
					/*
					 * cat = tablaCategoriaEmpleado.getSelectionModel()
					 * .getSelectedItem();
					 */
					categoriaDao.editar(cat);
				}
				bandera = -1;
				this.panelABM.setDisable(false);
				this.tablaCategoriaEmpleado.setItems(itemsTabla);
				this.desHabilitarBotonesAceptarCancelar();
			} catch (NumberFormatException e) {
				DialogsUtil.errorDialog("ERROR",
						"el salario por hora debe ser un numero con decimal",
						e.toString());
			} catch (SQLException e) {
				DialogsUtil.errorDialog("ERROR", "error  al Insertar en la DB",
						e.toString());
			}
		}
	}

	@ActionMethod("cancelar")
	public void cancelar() {
		this.panelABM.setDisable(true);
		this.desHabilitarBotonesAceptarCancelar();
	}

	public boolean validar() {
		if (Validate.textFieldValidate(textDescripcion, "descripcion")
				&& Validate.textFieldDecimalValidate(textSalarioHora,
						"salario por hora")

		) {
			return true;
		}

		return false;
	}
}
