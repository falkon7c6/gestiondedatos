package gestion.gestionFinal.controlador.administracion;

import gestion.gestionFinal.modelo.Obrasocial;
import gestion.gestionFinal.persistencia.ObraSocialDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/administracion/VistaObraSocial.fxml")
public class VistaObraSocialController {
	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView<Obrasocial> tablaObraSocial;
	@FXML
	private TableColumn colId;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TableColumn colImporteFijo;
	@FXML
	private TableColumn colImportePorcentual;
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textImporteFijo;
	@FXML
	private TextField textImportePorcentual;
	@FXML
	@ActionTrigger("nueva")
	private Button botonNueva;
	@FXML
	@ActionTrigger("eliminar")
	private Button botonEliminar;
	@FXML
	@ActionTrigger("editar")
	private Button botonEditar;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	@FXML
	private GridPane panelABM;

	private ObservableList<Obrasocial> itemsTabla;
	private ObservableList<Obrasocial> filtrados;
	private ObraSocialDAO obraSocialDao;
	int bandera = -1;

	@PostConstruct
	public void Initialize() {
		this.desHabilitarBotonesAceptarCancelar();
		this.panelABM.setDisable(true);
		obraSocialDao = new ObraSocialDAO();
		iniciarColumnas();
		itemsTabla = FXCollections.observableArrayList();
		itemsTabla = obraSocialDao.listar();
		this.tablaObraSocial.setItems(itemsTabla);
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});
		this.tablaObraSocial.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 1) {
							if (tablaObraSocial.getSelectionModel()
									.getSelectedItem() != null) {
								llenarEspacios(tablaObraSocial
										.getSelectionModel().getSelectedItem());
							}
						}

					}

				});
	}

	private void iniciarColumnas() {
		this.colId.setCellValueFactory(new PropertyValueFactory<>(
				"idObraSocial"));
		this.colImporteFijo.setCellValueFactory(new PropertyValueFactory<>(
				"importeFijo"));
		this.colImportePorcentual
				.setCellValueFactory(new PropertyValueFactory<>(
						"importePorcentual"));
		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<>("nombre"));

	}

	public void llenarEspacios(Obrasocial o) {
		this.textImporteFijo.setText("" + o.getImporteFijo());
		this.textImportePorcentual.setText("" + o.getImportePorcentual());
		this.textNombre.setText(o.getNombre());
	}

	@ActionMethod("nueva")
	public void nueva() {
		this.bandera = 0;
		this.panelABM.setDisable(false);
		this.habilitarBotonesAceptarCancelar();
	}

	@ActionMethod("editar")
	public void editar() {
		if (Validate.tableNotSelectedItemValidate(tablaObraSocial)) {
			bandera = 1;
			this.panelABM.setDisable(false);
			this.habilitarBotonesAceptarCancelar();
		}
	}

	public void habilitarBotonesAceptarCancelar() {
		this.botonAceptar.setDisable(false);
		this.botonCancelar.setDisable(false);
	}

	public void desHabilitarBotonesAceptarCancelar() {
		this.botonAceptar.setDisable(true);
		this.botonCancelar.setDisable(true);
	}

	@ActionMethod("eliminar")
	public void eliminar() {
		try {
			Obrasocial temp = this.tablaObraSocial.getSelectionModel()
					.getSelectedItem();
			this.obraSocialDao.borrar(temp.getIdObraSocial());
			this.itemsTabla.remove(temp);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("ERROR SQL", "Error en la Base de Datos",
					"La obra social no puede ser eliminada");
		} catch (NullPointerException e) {
			System.out.println("null pointer");
		}
	}

	public void filtrar() {
		String filtrado = textFiltrar.getText().toLowerCase();
		filtrados = FXCollections.observableArrayList();
		for (Obrasocial obra : itemsTabla) {
			if (obra.getNombre().toLowerCase().startsWith(filtrado)) {
				filtrados.add(obra);
			}
			this.tablaObraSocial.setItems(filtrados);
		}
	}

	@ActionMethod("aceptar")
	public void aceptar() {
		if (validar()) {
			try {
				Obrasocial obra = null;
				if (bandera == 0) {
					obra = new Obrasocial();

				}
				if (bandera == 1
						&& Validate
								.tableNotSelectedItemValidate(tablaObraSocial)) {
					obra = tablaObraSocial.getSelectionModel()
							.getSelectedItem();
				}
				obra.setImporteFijo(Float.valueOf(textImporteFijo.getText()));
				obra.setImportePorcentual(Float.valueOf(textImportePorcentual
						.getText()));
				obra.setNombre(this.textNombre.getText());
				if (bandera == 0) {
					int id;
					id = obraSocialDao.insertar(obra);
					obra.setIdObraSocial(id);
					itemsTabla.add(obra);

				}
				if (bandera == 1) {
					// obra =
					// tablaObraSocial.getSelectionModel().getSelectedItem();
					obraSocialDao.editar(obra);
				}
				bandera = -1;
				this.panelABM.setDisable(true);
				this.tablaObraSocial.setItems(itemsTabla);
				this.desHabilitarBotonesAceptarCancelar();
			} catch (NumberFormatException e) {
				DialogsUtil.errorDialog("ERROR",
						"Los campos importe fijo e importe porcentual "
								+ "deben ser Numeros decimales", e.toString());

			} catch (SQLException e) {
				DialogsUtil.errorDialog("Error",
						"Error al insertar en la base de datos", e.toString());

			}
		}
	}

	@ActionMethod("cancelar")
	public void cancelar() {
		this.panelABM.setDisable(true);
		this.desHabilitarBotonesAceptarCancelar();
	}

	public boolean validar() {
		if(
Validate.textFieldValidate(textNombre, "nombre")
				&& Validate.textFieldDecimalValidate(textImporteFijo,
						"importe fijo")
				&& Validate.textFieldDecimalValidate(textImportePorcentual,
						"importe porcentual")
				
				) {
			return true;
		}
		
		return false;
	}
}
