package gestion.gestionFinal.controlador.administracion;

import gestion.gestionFinal.modelo.Categoriaproducto;
import gestion.gestionFinal.persistencia.CategoriaProductoDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/administracion/VistaCategoriaProducto.fxml")
public class VistaCategoriaProductoController {

	@FXML
	private TextField textFiltrar;
	@FXML
	private TableView<Categoriaproducto> tabla;
	@FXML
	private TableColumn colId;
	@FXML
	private TableColumn colNombre;
	@FXML
	@ActionTrigger("nueva")
	private Button botonNueva;
	@FXML
	@ActionTrigger("eliminar")
	private Button botonEliminar;
	@FXML
	@ActionTrigger("editar")
	private Button botonEditar;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	@FXML
	private TextField textNombre;

	private ObservableList<Categoriaproducto> itemsTabla;
	private ObservableList<Categoriaproducto> filtrados;
	private CategoriaProductoDAO categoriaProductoDao;
	int bandera = -1;

	@PostConstruct
	public void Initialize() {
		this.desHabilitarBotonesAceptarCancelar();
		this.textNombre.setDisable(true);
		categoriaProductoDao = new CategoriaProductoDAO();
		iniciarColumnas();
		itemsTabla = FXCollections.observableArrayList();
		itemsTabla = categoriaProductoDao.listar();
		this.tabla.setItems(itemsTabla);
		this.textFiltrar.setOnKeyReleased(event -> {
			filtrar();
		});
		this.tabla.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 1) {
							if (tabla.getSelectionModel().getSelectedItem() != null) {
								llenarEspacios(tabla.getSelectionModel()
										.getSelectedItem());
							}
						}

					}

				});
	}

	private void iniciarColumnas() {
		this.colId.setCellValueFactory(new PropertyValueFactory<>(
				"idCategoriaProducto"));
		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<>("nombre"));

	}

	public void llenarEspacios(Categoriaproducto o) {
		this.textNombre.setText(o.getNombre());
	}

	@ActionMethod("nueva")
	public void nueva() {
		this.bandera = 0;
		this.textNombre.setDisable(false);
		this.habilitarBotonesAceptarCancelar();
	}

	@ActionMethod("editar")
	public void editar() {
		if (Validate.tableNotSelectedItemValidate(tabla)) {
			bandera = 1;
			this.textNombre.setDisable(false);
			this.habilitarBotonesAceptarCancelar();
		}

	}

	public void habilitarBotonesAceptarCancelar() {
		this.botonAceptar.setDisable(false);
		this.botonCancelar.setDisable(false);
	}

	public void desHabilitarBotonesAceptarCancelar() {
		this.botonAceptar.setDisable(true);
		this.botonCancelar.setDisable(true);
	}

	@ActionMethod("eliminar")
	public void eliminar() {
		try {
			Categoriaproducto temp = this.tabla.getSelectionModel()
					.getSelectedItem();
			this.categoriaProductoDao.borrar(temp.getIdCategoriaProducto());
			this.itemsTabla.remove(temp);
		} catch (SQLException e) {
			DialogsUtil.errorDialog("Error", "Error en la base de datos",
					"la categoria del producto no puede ser eliminada");
		} catch (NullPointerException e) {
			System.out.println("null pointer");
		}

	}

	public void filtrar() {
		String filtrado = textFiltrar.getText().toLowerCase();
		filtrados = FXCollections.observableArrayList();
		for (Categoriaproducto cat : itemsTabla) {
			if (cat.getNombre().toLowerCase().startsWith(filtrado)) {
				filtrados.add(cat);
			}
			this.tabla.setItems(filtrados);
		}
	}

	@ActionMethod("aceptar")
	public void aceptar() {
		if (this.validar()) {
			try {
				Categoriaproducto cat = null;
				if (bandera == 0) {
					cat = new Categoriaproducto();

				}
				if (bandera == 1
						&& Validate.tableNotSelectedItemValidate(tabla)) {
					cat = tabla.getSelectionModel().getSelectedItem(); // TODO
																		// editar
				}
				cat.setNombre(this.textNombre.getText());
				if (bandera == 0) {
					int id;
					id = categoriaProductoDao.insertar(cat);
					cat.setIdCategoriaProducto(id);
					itemsTabla.add(cat);

				}
				if (bandera == 1) {
					// cat = tabla.getSelectionModel().getSelectedItem();
					categoriaProductoDao.editar(cat);
				}
				bandera = -1;
				this.textNombre.setDisable(true);
				this.tabla.setItems(itemsTabla);
				this.desHabilitarBotonesAceptarCancelar();
			} catch (NumberFormatException e) {
				DialogsUtil.errorDialog("ERROR",
						"Los campos importe fijo e importe porcentual "
								+ "deben ser Numeros decimales", e.toString());

			} catch (SQLException e) {
				DialogsUtil.errorDialog("Error",
						"Error al insertar en la base de datos", e.toString());

			}
		}
	}

	@ActionMethod("cancelar")
	public void cancelar() {
		this.textNombre.setDisable(true);
		this.desHabilitarBotonesAceptarCancelar();
	}

	public boolean validar() {
		if (Validate.textFieldValidate(textNombre, "nombre")) {
			return true;
		}
		return false;
	}
}
