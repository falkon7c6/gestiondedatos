package gestion.gestionFinal.controlador;

import gestion.gestionFinal.modelo.Empleado;
import gestion.gestionFinal.modelo.Periodo;
import gestion.gestionFinal.persistencia.EmpleadoDAO;
import gestion.gestionFinal.persistencia.PeriodoDAO;
import gestion.gestionFinal.persistencia.UsuarioDAO;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;

import java.time.LocalDate;
import java.time.LocalTime;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/VistaPrincipal.fxml")
public class VistaPrincipalController {

	@FXML
	private Label labelUsuario;

	@FXML
	@ActionTrigger("Productos")
	private Button btnProductos;

	@FXML
	@ActionTrigger("Empleados")
	private Button botonEmpleados;

	@FXML
	@ActionTrigger("Ventas")
	private Button botonVentas;

	@FXML
	@ActionTrigger("Compras")
	private Button botonCompras;

	@FXML
	@ActionTrigger("liquidacion")
	private Button botonLiquidacion;

	@FXML
	@ActionTrigger("administracion")
	private Button botonAdministracion;

	@FXML
	@ActionTrigger("cliente")
	private Button botonCliente;

	@FXML
	@ActionTrigger("proveedor")
	private Button botonProveedor;

	@FXML
	@ActionTrigger("cerrarSesion")
	private Button botonCerrarSesion;

	@FXML
	private ImageView imageView;

	@FXMLViewFlowContext
	private ViewFlowContext context;

	@FXML
	private ComboBox<String> combotest;

	private UsuarioDAO userDao;

	@PostConstruct
	public void Initialize() {

		try {
			userDao = new UsuarioDAO();

			// EMPLEADO LOG IN
			String usuario;
			usuario = (String) context.getApplicationContext()
					.getRegisteredObject("usuario");
			EmpleadoDAO empDAO = new EmpleadoDAO();
			Empleado empleadoActual = empDAO.obtenerEmpleado(
					Integer.parseInt(usuario)).get(0);
			context.getApplicationContext().register("empleadoActual",
					empleadoActual);

			String rol = userDao.obtenerRol(usuario);

			// PERIODO
			LocalDate date = LocalDate.now();
			LocalTime time = LocalTime.now();
			Periodo periodo = new Periodo();
			ObservableList<Periodo> periodos = FXCollections
					.observableArrayList();
			PeriodoDAO periodoDAO = new PeriodoDAO();
			periodos = periodoDAO.listar();
			boolean bandera = false;
			for (Periodo per : periodos) {
				if (Integer.parseInt(per.getMes()) == date.getMonthValue()
						&& Integer.parseInt(per.getAnio()) == date.getYear()) {
					periodo = per;
					System.out.println(periodo.getDescripcion());
					bandera = true;
				} else if (bandera == false) {
					periodo.setAnio("2015");
					periodo.setDescripcion("sin periodo");
					periodo.setMes("12");
					periodo.setIdPeriodo(3);
				}
			}
			context.getApplicationContext().register("periodo", periodo);
			this.permisos(rol);
		} catch (Exception e) {
			this.labelUsuario.setText("Usuario o contraseña no validos");
			this.botonAdministracion.setVisible(false);
			this.botonCliente.setVisible(false);
			this.botonCompras.setVisible(false);
			this.botonEmpleados.setVisible(false);
			this.botonLiquidacion.setVisible(false);
			this.botonProveedor.setVisible(false);
			this.botonVentas.setVisible(false);
			this.btnProductos.setVisible(false);
			// e.printStackTrace();
		}

	}

	public void permisos(String rol) {
		switch (rol) {
		case "compraventa":
			System.out.println("compra");
			this.botonAdministracion.setDisable(true);
			this.botonCliente.setDisable(true);
			this.botonCompras.setDisable(false);
			this.botonEmpleados.setDisable(true);
			this.botonLiquidacion.setDisable(true);
			this.botonProveedor.setDisable(true);
			this.botonVentas.setDisable(false);
			this.btnProductos.setDisable(true);
			break;
		case "liquidacion":
			System.out.println("liquidacion");
			this.botonAdministracion.setDisable(true);
			this.botonCliente.setDisable(true);
			this.botonCompras.setDisable(true);
			this.botonEmpleados.setDisable(true);
			this.botonLiquidacion.setDisable(false);
			this.botonProveedor.setDisable(true);
			this.botonVentas.setDisable(true);
			this.btnProductos.setDisable(true);
			break;
		case "administrador":
			System.out.println("admin");
			this.botonAdministracion.setDisable(false);
			this.botonCliente.setDisable(false);
			this.botonCompras.setDisable(false);
			this.botonEmpleados.setDisable(false);
			this.botonLiquidacion.setDisable(false);
			this.botonProveedor.setDisable(false);
			this.botonVentas.setDisable(false);
			this.btnProductos.setDisable(false);
			break;

		default:
			break;
		}
	}
}
