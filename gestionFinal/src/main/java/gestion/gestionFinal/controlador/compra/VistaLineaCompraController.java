package gestion.gestionFinal.controlador.compra;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import gestion.gestionFinal.controlador.venta.VistaVentaController;
import gestion.gestionFinal.modelo.Empleado;
import gestion.gestionFinal.modelo.Lineacompra;
import gestion.gestionFinal.modelo.Producto;
import gestion.gestionFinal.modelo.Proveedor;
import gestion.gestionFinal.persistencia.ProductoDAO;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.beans.property.ListProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

//esta anotacion relaciona el controlador con su archivo fxml de vista
@FXMLController(value = "/gestion/gestionFinal/vista/compra/VistaLineaCompra.fxml")
public class VistaLineaCompraController {
	@FXML
	private TableView tablaProductos;
	@FXML
	private TableColumn colDescripcion;
	@FXML
	private TableColumn colMarca;
	@FXML
	private TableColumn colPrecioUnitario;
	@FXML
	private TableColumn colNombre;
	@FXML
	private TextField textBuscar;
	@FXML
	private ComboBox comboBuscar;
	@FXML
	private TextField textCantidad;
	@FXML
	private TextField textDescuento;
	@FXML
	private Label labelSubtotal;
	
	//la anotacion actionTrigger asocia este boton con el metodo que tenga ese nombre en su anotacion
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	@FXMLViewFlowContext
    private ViewFlowContext context;

	// Variables
	private ProductoDAO productoDAO;
	private Stage DialogStage; //la ventana sobre la cual se abre este dialog
	private VistaCompraController controladorVentanaPrincipal; //el controlador de la ventana principal para poder acceder a sus datos (dataModel TODO)
	private ObservableList<Producto> itemsComboProductos; //lista de items para la tabla (el combo box sera borrado y esta variable renombrada)
	private ObservableList<Producto> filtrados; //lista que se muestra en la tabla cuando se filtran items
	private Lineacompra lineaActual; //linea de compras del dialog, sea para nuevo o para editar
	private String IVA;
	//este metodo se invoca despues del constructor
	@PostConstruct
	public void Initialize() {
		itemsComboProductos = FXCollections.observableArrayList(); //inicializa la lista de items para la tabla
		Proveedor provSeleccionado = (Proveedor) 
				context.getApplicationContext()
				.getRegisteredObject("proveedorSeleccionado");
				IVA = (String) context.getApplicationContext().getRegisteredObject("IVA");
		productoDAO = new ProductoDAO();
		itemsComboProductos.clear();
		try{
			itemsComboProductos = productoDAO.listar(provSeleccionado);
		}catch(NullPointerException ex){
			System.out.println("no selecciono ningun proveedor");
		}
		//inicializamos las columnas de la tabla para que se autoactualicen
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<>(
				"descripcion"));
		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<>("nombre"));
		this.colPrecioUnitario.setCellValueFactory(new PropertyValueFactory<>(
				"precioVenta"));
		this.colMarca.setCellValueFactory(new Callback<CellDataFeatures<Producto, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(
					CellDataFeatures<Producto, String> p) {
				return new SimpleStringProperty(p.getValue().getMarcaproducto().getNombre());
			}
		});
		
		//relaciono la tabla con su lista de items
		tablaProductos.setItems(itemsComboProductos);
		//diferencia si es un NUEVO o un MODIFICAR, esto sera eliminado
		if(this.lineaActual == null){
			lineaActual = new Lineacompra();
		} else {
			tablaProductos.getSelectionModel().select(lineaActual.getProducto());
			textCantidad.setText(Integer.toString(lineaActual.getCantidad()));
		}
		//Evento para autocalcular el subtotal al escribir en el text cantidad
		textCantidad.setOnKeyReleased(event -> {
			try{
			if(tablaProductos.getSelectionModel().getSelectedIndex()>=0){
				labelSubtotal.setText(null);
				Float valor = 0F; 
				Producto temp = (Producto) tablaProductos.getSelectionModel().getSelectedItem();
				valor = temp.getPrecioVenta() * 
						Integer.parseInt(textCantidad.getText());
				System.out.println("valor: " + valor);
				labelSubtotal.setText(valor.toString());
				
			}}catch(NumberFormatException ex){
				System.out.println("excepcion");
			}
		});
		//evento para filtrar productos de la tabla
		this.textBuscar.setOnKeyReleased(event -> {
			filtrar();
		});
		
	}

	@FXML //metodo para el boton aceptar
	@ActionMethod("aceptar")
	public void aceptar() {
		try {
			if (controladorVentanaPrincipal != null) {
				if (tablaProductos.getSelectionModel().getSelectedIndex() >= 0) {
					lineaActual.setProducto((Producto) tablaProductos
							.getSelectionModel().getSelectedItem());
				}
				lineaActual.setCantidad(Integer.parseInt(this.textCantidad.getText()));
				if(IVA == "A"){
					lineaActual.setPrecio(lineaActual.getProducto().getPrecioVenta());
					lineaActual.setIva(lineaActual.getPrecio() * 0.21F);
					lineaActual.setSubtotal((lineaActual.getPrecio() + lineaActual.getIva())* lineaActual.getCantidad());
				} else{
					lineaActual.setPrecio(lineaActual.getProducto().getPrecioVenta());
					lineaActual.setSubtotal(lineaActual.getPrecio() * lineaActual.getCantidad());
					lineaActual.setIva(0);
				}				
				controladorVentanaPrincipal.setLineaNueva(lineaActual);
				lineaActual = null;
				DialogStage.close();
				
				
			}
		} catch (NumberFormatException e) {
			System.out.println("error en el campo numerico");
		} catch(NullPointerException e){
			System.out.println("cuando no java, cuando no");
		}
	}

	@FXML
	@ActionMethod("cancelar")
	public void cancelar() {
		DialogStage.close();
	}

	public void editar(){
		tablaProductos.getSelectionModel().select(lineaActual.getProducto());
		textCantidad.setText(Integer.toString(lineaActual.getCantidad()));
	}
	
	public void filtrar(){
		String filtrado = textBuscar.getText().toLowerCase();
		filtrados = FXCollections.observableArrayList();
		for (Producto prod : itemsComboProductos) {
			if (prod.getNombre().toLowerCase()
					.startsWith(filtrado)) {
				filtrados.add(prod);
			}
			//cambia la lista por la filtrada (la original no se pierde)
			this.tablaProductos.setItems(filtrados);
		}
	}
	
	
	//GETS y SETS que a nadie le importan
	public Lineacompra getLineaActual() {
		return lineaActual;
	}

	public void setLineaActual(Lineacompra lineaActual) {
		this.lineaActual = lineaActual;
	}

	public Stage getDialogStage() {
		return DialogStage;
	}

	public void setDialogStage(Stage dialogStage) {
		DialogStage = dialogStage;
	}

	public VistaCompraController getControladorVentanaPrincipal() {
		return controladorVentanaPrincipal;
	}

	public void setControladorVentanaPrincipal(
			VistaCompraController vistaCompraController) {
		this.controladorVentanaPrincipal = vistaCompraController;
	}

}
