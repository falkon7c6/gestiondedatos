package gestion.gestionFinal.controlador.compra;

import gestion.gestionFinal.modelo.Compra;
import gestion.gestionFinal.modelo.Empleado;
import gestion.gestionFinal.modelo.Lineacompra;
import gestion.gestionFinal.modelo.Periodo;
import gestion.gestionFinal.modelo.Proveedor;
import gestion.gestionFinal.persistencia.CompraDAO;
import gestion.gestionFinal.persistencia.LineaCompraDAO;
import gestion.gestionFinal.persistencia.PeriodoDAO;
import gestion.gestionFinal.persistencia.ProveedorDAO;
import gestion.gestionFinal.util.DialogsUtil;
import gestion.gestionFinal.util.ReportUtil;
import gestion.gestionFinal.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/compra/VistaCompra.fxml")
public class VistaCompraController {
	@FXML
	private TableView tabla;
	@FXML
	private TableColumn colProducto;
	@FXML
	private TableColumn colCantidad;
	@FXML
	private TableColumn colPrecioUnitario;
	@FXML
	private TableColumn colIVA;
	@FXML
	private TableColumn colSubtotal;
	@FXML
	@ActionTrigger("agregar")
	private Button botonAgregar;
	@FXML
	@ActionTrigger("editar")
	private Button botonModificar;
	@FXML
	@ActionTrigger("borrar")
	private Button botonBorrar;
	@FXML
	private ComboBox<String> comboIVA;
	@FXML
	private ComboBox<String> comboFactura;
	@FXML
	private ComboBox<Proveedor> comboProveedor;
	@FXML
	private Button botonBuscarProveedor;
	@FXML
	private TextField textTotal;
	@FXML
	@ActionTrigger("guardar")
	private Button botonGuardar;
	@FXML
	@ActionTrigger("atras")
	private Button botonAtras;
	@FXML
	private Label labelFecha;
	@FXML
	private Label labelEmpleado;
	@FXML
	private TextArea textAreaComentarios;
	@FXML
	private Label labelPeriodo;
	@FXMLViewFlowContext
	private ViewFlowContext context;

	// VARIABLES
	Stage ventanaDialog;
	private Lineacompra lineaNueva;
	private ObservableList<Lineacompra> itemsTabla;
	private ObservableList<Proveedor> listaProveedores;
	private ObservableList<String> itemsIva;
	private String tipoFactura;
	private Float total = 0F;
	private ProveedorDAO provDAO;
	private Calendar fecha = Calendar.getInstance();
	private LocalDate date;
	private Empleado empleadoActual;
	private Periodo periodo;
	private PeriodoDAO periodoDAO;
	private CompraDAO compraDAO;
	private LineaCompraDAO lineaDAO;

	@PostConstruct
	public void Initialize() {

		// inicializamos las listas y las llenamos con datos test
		itemsTabla = FXCollections.observableArrayList();
		listaProveedores = FXCollections.observableArrayList();
		itemsIva = FXCollections.observableArrayList();
		provDAO = new ProveedorDAO();
		listaProveedores = provDAO.listar();
		comboProveedor.setItems(listaProveedores);
		itemsIva.add("Responsable Inscripto");
		itemsIva.add("monotributista");
		itemsIva.add("consumidor final");
		this.comboIVA.setItems(itemsIva);
		// evento para limpiar la tabla si cambiamos de proveedor (agregar
		// dialog de yes or no)
		iniciarColumnas();
		this.tabla.setItems(itemsTabla);

		date = LocalDate.now();
		this.labelFecha.setText("" + date.getDayOfMonth() + "-"
				+ date.getMonthValue() + "-" + date.getYear());
		empleadoActual = (Empleado) context.getApplicationContext()
				.getRegisteredObject("empleadoActual");
		periodo = (Periodo) context.getApplicationContext()
				.getRegisteredObject("periodo");
		System.out.println(periodo.getDescripcion());
		labelEmpleado.setText(empleadoActual.toString());
		compraDAO = new CompraDAO();
		lineaDAO = new LineaCompraDAO();
		textTotal.setEditable(false);
		comboIVA.setDisable(true);
		mostrarPeriodo();
		deshabilitarABM();

	}

	private void mostrarPeriodo() {
		ObservableList<Periodo> periodos = FXCollections.observableArrayList();
		periodoDAO = new PeriodoDAO();
		periodos = periodoDAO.listar();
		for (Periodo per : periodos) {
			if(Integer.parseInt(per.getMes())  == date.getMonthValue() && Integer.parseInt(per.getAnio())  == date.getYear()) {
				this.labelPeriodo.setText(per.getDescripcion());
			}
		}
		
	}

	private void habilitarABM() {
		botonAgregar.setDisable(false);
		botonBorrar.setDisable(false);
		botonModificar.setDisable(false);

	}

	private void deshabilitarABM() {
		botonAgregar.setDisable(true);
		botonBorrar.setDisable(true);
		botonModificar.setDisable(true);

	}

	@ActionMethod("agregar")
	public void agregar() {
		System.out.println("agregar");

		try {
			Proveedor provSeleccionado = listaProveedores.get(comboProveedor
					.getSelectionModel().getSelectedIndex());
			context.getApplicationContext().register("proveedorSeleccionado",
					provSeleccionado);
			String condicionIVA = this.tipoFactura();
			context.getApplicationContext().register("IVA", condicionIVA);
		} catch (Exception ex) {
			System.out.println("error con el combo proveedores");
			ex.printStackTrace();
		}
		ventanaDialog = crearDialog(null);
		ventanaDialog.showAndWait();
		if (lineaNueva != null) {
			itemsTabla.add(lineaNueva);
			lineaNueva = null;
			calcularTotal();
		} else {
			System.out.println("se cancelo");
		}
		ventanaDialog = null;

	}

	public Stage crearDialog(Lineacompra linea) { // esto seria mas simple si
													// DATAFX estuviera completo
													// y pudiera usar bien los
													// flows y los datamodels en
													// dialogs modales, pero
													// como esta en
													// implementacion me las
													// tuve que rebuscar
		Stage stage = new Stage();
		AnchorPane root = null;
		try {
			root = FXMLLoader
					.load(getClass()
							.getResource(
									"/gestion/gestionFinal/vista/compra/VistaLineaCompra.fxml"));
			Flow mainFlow = new Flow(VistaLineaCompraController.class);
			io.datafx.controller.flow.FlowHandler mainFlowHandler = mainFlow
					.createHandler();
			// agrega la animacion
			StackPane pane = mainFlowHandler.start(new AnimatedFlowContainer(
					Duration.millis(320), ContainerAnimations.ZOOM_IN));
			VistaLineaCompraController controladorDialog = (VistaLineaCompraController) mainFlowHandler
					.getCurrentViewContext().getController();
			controladorDialog.setControladorVentanaPrincipal(this);
			root.getChildren().add(pane);
			Scene escena = new Scene(root);
			escena.getStylesheets().add("/styles/NewFile.css");
			stage.setTitle("Categoria de Productos");
			stage.setScene(escena);
			stage.initModality(Modality.APPLICATION_MODAL);
			controladorDialog.setDialogStage(stage);
			// si la linea que le pasas no es nula, edita y no hace nueva
			if (linea != null) {
				controladorDialog.setLineaActual(linea);
				controladorDialog.editar();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stage;
	}

	@ActionMethod("borrar")
	public void borrar() {
		System.out.println("borrar");
		int index = tabla.getSelectionModel().getSelectedIndex();
		if (index >= 0) {
			itemsTabla.remove(index);
			calcularTotal();
		}
	}

	@ActionMethod("editar")
	public void editar() {
		System.out.println("editar");
		if (this.tabla.getSelectionModel().getSelectedIndex() >= 0) {
			lineaNueva = (Lineacompra) tabla.getSelectionModel()
					.getSelectedItem();
			ventanaDialog = crearDialog(lineaNueva);
			ventanaDialog.showAndWait();
			lineaNueva = null;
			ventanaDialog = null;
			calcularTotal();
		}
	}

	public void llenarEspacios() {

	}

	public void iniciarColumnas() {
		this.colCantidad
				.setCellValueFactory(new PropertyValueFactory<Lineacompra, Integer>(
						"cantidad"));
		this.colPrecioUnitario.setCellValueFactory(new PropertyValueFactory<>(
				"precio"));
		this.colSubtotal.setCellValueFactory(new PropertyValueFactory<>(
				"subtotal"));
		this.colProducto
				.setCellValueFactory(new Callback<CellDataFeatures<Lineacompra, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(
							CellDataFeatures<Lineacompra, String> p) {
						return new SimpleStringProperty(p.getValue()
								.getProducto().toString());
					}
				});
		this.colIVA.setCellValueFactory(new PropertyValueFactory<>("iva"));

		comboProveedor.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Proveedor>() {
					@Override
					public void changed(
							ObservableValue<? extends Proveedor> observable,
							Proveedor oldValue, Proveedor newValue) {
						comboIVA.setDisable(false);
						comboProveedor.setDisable(true);
						itemsTabla.clear();

					}
				});

comboIVA.getSelectionModel().selectedItemProperty()
.addListener(new ChangeListener<String>() {
	@Override
	public void changed(
			ObservableValue<? extends String> observable,
			String oldValue, String newValue) {
		System.out.println("cambio iva");
						comboIVA.setDisable(true);
		habilitarABM();
		itemsTabla.clear();

	}
});
	}

	public void calcularTotal() {
		if (itemsTabla.size() > 0) {
			total = 0F;
			for (Lineacompra lineacompra : itemsTabla) {
				total += lineacompra.getSubtotal();
			}
			textTotal.setText(Float.toString(total));
		}
	}

	public String tipoFactura() {
		int index = this.comboIVA.getSelectionModel().getSelectedIndex();
		if (index >= 0) {
			try {
				switch (index) {
				case 0:
					System.out.println("factura: A");
					this.comboIVA.setDisable(true);
					return "A";

				case 1:
					System.out.println("factura: C");
					this.comboIVA.setDisable(true);
					return "B";
				case 2:
					System.out.println("factura: Ticket");
					this.comboIVA.setDisable(true);
					return "Ticket";
				}
			} catch (Exception ex) {
				System.out.println("error del combobox");
				ex.printStackTrace();
			}
		}

		return "";

	}

	@ActionMethod("guardar")
	public void guardar() {
		try {
			if (validar()) {
				Compra compra = new Compra();
				compra.setTipoFactura(tipoFactura());
				compra.setCondicionIVA((String) this.comboIVA
						.getSelectionModel().getSelectedItem());
				compra.setFecha(date);
				System.out.println("fecha" + date.toString());
				compra.setNumeroComprobante("" + 10); // TODO
				compra.setObservaciones(this.textAreaComentarios.getText());
				tipoFactura = this.tipoFactura();
				compra.setTipoFactura(tipoFactura);
				compra.setTotal(total);
				compra.setEmpleado(empleadoActual);
				compra.setPeriodo(periodo);
				compra.setProveedor(listaProveedores.get(comboProveedor
						.getSelectionModel().getSelectedIndex()));
				compra.setIdCompra(compraDAO.insertar(compra));
				int idCompra = compra.getIdCompra();
				for (Lineacompra linea : this.itemsTabla) {
					linea.setCompra(compra);
					int idLinea = lineaDAO.insertar(linea);
					linea.setIdLineaCompra(idLinea);
				}

				// imprime el reporte
				if (DialogsUtil.confirmationDialog("Reporte",
						"Se va a imprimir el comprobante de compra",
						"Presione OK para continuar") == true) {
					Map<String, Object> parametrosMap = new HashMap();
					parametrosMap.put("idcompra", new Integer(idCompra));
					ReportUtil reporterReportUtil = new ReportUtil();
					String reporteNombre = "" + compra.getFecha() + " - "
							+ idCompra + " - " + compra.getNumeroComprobante()
							+ ".pdf";
					reporterReportUtil.imprimirPDF(
							"/reportes/CompraPrueba.jasper",
							parametrosMap, reporteNombre);
				}

				System.out.println("compra factura " + compra.getTipoFactura());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean validar() {

		if (Validate.textAreaValidate(textAreaComentarios, "Comentarios", 45)
				&& Validate.comboValidate(comboProveedor, "ComboBox Proveedor")
				&& Validate.comboValidate(comboIVA, "ComboBox IVA")
				&& Validate.tableValidate(tabla, "Tabla Lineas de Compra")) {
			return true;
		}

		return false;
	}

	// SETS Y GETS QUE A NADIE LE INTERESAN
	public Lineacompra getLineaNueva() {
		return lineaNueva;
	}

	public void setLineaNueva(Lineacompra lineaNueva) {
		this.lineaNueva = lineaNueva;
	}

	public ObservableList<Proveedor> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(ObservableList<Proveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

}
