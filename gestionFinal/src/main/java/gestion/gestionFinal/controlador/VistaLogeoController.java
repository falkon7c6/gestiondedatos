package gestion.gestionFinal.controlador;

import gestion.gestionFinal.MainApp;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import io.datafx.controller.util.VetoException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import javax.annotation.PostConstruct;

@FXMLController(value = "/gestion/gestionFinal/vista/VistaLogeo.fxml")
public class VistaLogeoController {
	@FXML
	private TextField textUsuario;
	@FXML
	private PasswordField textPass;
	@FXML
	@ActionTrigger("comprobar")
	private Button botonIngresar;
	@FXML
	@ActionTrigger("salir")
	private Button botonCerrar;
	@FXMLViewFlowContext
    private ViewFlowContext context;
	private String usuario;
	private String clave;
	
	@PostConstruct
	public void Init() {
	}
	
	@ActionMethod("test")
	public void test() {
		MainApp mainApp = context.getApplicationContext().getRegisteredObject(
				MainApp.class);
		try {
			mainApp.mainFlowHandler.navigateTo(VistaPrincipalController.class);
		} catch (VetoException | FlowException e) {
			e.printStackTrace();
		}
	}


	@ActionMethod("comprobar")
	public void comprobar() {
		context.getApplicationContext().register("usuario",
				this.textUsuario.getText());
		context.getApplicationContext().register("empleadoActual",
				this.textUsuario.getText());
		context.getApplicationContext().register("clave",
				this.textPass.getText());
		test();
	}
	
	@ActionMethod("salir")
	public void salir() {
		System.exit(0);
	}
}
