package gestion.gestionFinal;

import gestion.gestionFinal.controlador.VistaLogeoController;
import gestion.gestionFinal.controlador.VistaPrincipalController;
import gestion.gestionFinal.controlador.administracion.VistaAdministracionController;
import gestion.gestionFinal.controlador.cliente.VistaClienteController;
import gestion.gestionFinal.controlador.compra.VistaCompraController;
import gestion.gestionFinal.controlador.liquidacion.VistaPrincipalLiquidacionController;
import gestion.gestionFinal.controlador.persona.GrupoFamiliarController;
import gestion.gestionFinal.controlador.persona.VistaPrincipalEmpleadoController;
import gestion.gestionFinal.controlador.producto.vistaProductoController;
import gestion.gestionFinal.controlador.proveedor.VistaCatalogoController;
import gestion.gestionFinal.controlador.proveedor.VistaProveedorController;
import gestion.gestionFinal.controlador.venta.VistaVentaController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.container.DefaultFlowContainer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;



public class MainApp extends Application {
	public FlowHandler mainFlowHandler;
	
    @Override
    public void start(Stage stage) throws Exception {

    	
    	//creamos el Layout principal y le cargamos el FXML principal (Contiene toda la app)
        StackPane root = FXMLLoader.load(getClass().getResource("vista/rootLayout.fxml"));
        //creamos un flujo con todas las vistas de la app
    	
        Flow mainFlow = new Flow(VistaLogeoController.class)
        .withLink(VistaLogeoController.class, "logeo", VistaPrincipalController.class)
        .withLink(VistaPrincipalController.class, "Productos", vistaProductoController.class)
        .withLink(VistaPrincipalController.class,"Empleados" ,VistaPrincipalEmpleadoController.class)
        .withLink(VistaPrincipalController.class, "Ventas",VistaVentaController.class)
        .withLink(VistaPrincipalController.class, "Compras", VistaCompraController.class)
        .withLink(VistaPrincipalController.class, "liquidacion", VistaPrincipalLiquidacionController.class)
        .withLink(VistaPrincipalController.class, "administracion", VistaAdministracionController.class)
        .withLink(VistaPrincipalController.class, "cliente", VistaClienteController.class)
        .withLink(VistaPrincipalController.class, "proveedor", VistaProveedorController.class)
        .withLink(VistaProveedorController.class, "catalogo", VistaCatalogoController.class)
        .withLink(VistaPrincipalEmpleadoController.class, "agregar familiar", GrupoFamiliarController.class)
				.withLink(VistaPrincipalController.class, "cerrarSesion",
						VistaLogeoController.class)
        .withGlobalLink("menu principal", VistaPrincipalController.class)
        .withGlobalBackAction("atras");
        //creamos una variable para manipular el flujo de vistas y le pasamos una animacion
        mainFlowHandler = mainFlow.createHandler();
        /*root = mainFlowHandler.start(
        		new AnimatedFlowContainer(Duration.millis(320), ContainerAnimations.ZOOM_IN));*/
        root = mainFlowHandler.start(new DefaultFlowContainer());
        //insertamos en el centro del layout principal el flujo de vistas de la app
        //root.setCenter(pane);
        //creamos la escena y le pasamos el layout principal
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/NewFile.css");
        //le colocamos un titulo, lo insertamos en una ventana de windows y lo iniciamos
        mainFlowHandler.registerInApplicationContext(MainApp.class, this);
        stage.setTitle("Sistema de Gestion");
        stage.setScene(scene);
        stage.show();



    }

  
    public static void main(String[] args) {
        launch(args);
    }

}
