package gestion.gestionFinal.util;

import java.util.List;
import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;

public class DialogsUtil {

	public static void errorDialog(String title, String header, String context) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.getDialogPane().getStylesheets().add("/styles/NewFile.css");
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(context);
		alert.showAndWait();
	}
	
	public static void messageDialog(String titulo, String mensaje) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.getDialogPane().getStylesheets().add("/styles/NewFile.css");
		alert.setTitle(titulo);
		alert.setHeaderText(null);
		alert.setContentText(mensaje);

		alert.showAndWait();
	}
	
	public static boolean confirmationDialog(String titulo, String Header, String Content) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.getDialogPane().getStylesheets().add("/styles/NewFile.css");
		alert.setTitle(titulo);
		alert.setHeaderText(Header);
		alert.setContentText(Content);
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
		    return true;
		} else {
		    return false;
		}
	}
	
	public static String choiseDialog(String titulo, String Header, String Context ,List<String> choices) {
		
		ChoiceDialog<String> dialog = new ChoiceDialog<>(choices.get(0), choices);
		dialog.getDialogPane().getStylesheets().add("/styles/NewFile.css");
		dialog.setTitle(titulo);
		dialog.setHeaderText(Header);
		dialog.setContentText(Context);
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
		    return "" + result.get();
		}
		return "cancelo";
	}
}
