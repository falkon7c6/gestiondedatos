package gestion.gestionFinal.util;

import java.util.List;

import javafx.collections.ObservableList;

public class ProvinciasUtil {
	
	private static String[] provincias = {"Buenos Aires", "Catamarca", "Chaco", 
		"CABA", "Chubut", "Córdoba", "Corrientes", "Entre Ríos", "Formosa", 
		"Jujuy" ,"La Pampa", "La Rioja", "Mendoza", "Misiones", "Neuquén", "Río Negro", 
		"Salta", "San Juan", "San Luis", "Santa Cruz", "Santa Fe", "Santiago del Estero", 
		"Tierra del Fuego", "Tucumán"};
	
	public static String[] getProvincias(){
		return provincias;
		
	}
}
