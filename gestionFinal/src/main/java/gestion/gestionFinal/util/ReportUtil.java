package gestion.gestionFinal.util;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.fill.ReportFiller;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import gestion.gestionFinal.persistencia.databaseConnection;

public class ReportUtil {
	
	private databaseConnection conexion;
	private JasperReport reporte;
	private JasperPrint reportFilled;
	private JasperViewer visor;
	public ReportUtil() {
		conexion = new databaseConnection();
		
	}
	
	public void imprimirPDF(String path, Map parametros, String nombre){
		try {
			conexion.conectar();
			URL  in = this.getClass().getResource( path );
			reporte = (JasperReport) JRLoader.loadObject(in);
			reportFilled = JasperFillManager.fillReport(reporte, parametros, conexion.connection);
			visor = new JasperViewer(reportFilled, false);
			visor.setVisible(true);
			JasperExportManager.exportReportToPdfFile(reportFilled, "./Reportes/" + nombre);
		} catch (JRException e) {
			e.printStackTrace();
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
	}
}
